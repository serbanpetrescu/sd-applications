package sd.a3.bl;

/**
 * Exceptions thrown by the application when an expected error occurs. This
 * class extends the {@link RuntimeException} class, such that it's exceptions
 * may propagate through the Olingo methods up to the custom
 * {@link sd.a3.com.ODataServlet}.
 *
 * @author Serban Petrescu
 */
public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * A business exception is built with a business error and an optional array
	 * of parameters for the business error.
	 *
	 * @param e
	 * @param args
	 */
	public BusinessException(BusinessError e, String... args) {
		super(buildMessage(e, args));
	}

	/**
	 * Method used to build the JSON message from the {@link BusinessError} and
	 * the parameters.
	 *
	 * @param e
	 * @param args
	 * @return JSON serialized string.
	 */
	private static String buildMessage(BusinessError e, String[] args) {
		StringBuilder str = new StringBuilder();
		str.append("{\"error\": { \"code\": ");
		str.append(escapeJSON(Integer.toString(e.getCode())));
		if (e.hasParams()) {
			str.append(", \"params\": [");
			String separator = "";
			for (String s : args) {
				str.append(separator);
				separator = ",";
				str.append(escapeJSON(s));
			}
			str.append("]");
		}
		str.append("}}");
		return str.toString();
	}

	/**
	 * Escape a JSON-string for literals. Source: Jettison open source JSON
	 * library.
	 */
	public static String escapeJSON(String string) {
		if (string == null || string.length() == 0) {
			return "\"\"";
		}

		char c = 0;
		int i;
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		String t;

		sb.append('"');
		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
			case '"':
				sb.append('\\');
				sb.append(c);
				break;
			case '/':
				// if (b == '<') {
				sb.append('\\');
				// }
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else {
					sb.append(c);
				}
			}
		}
		sb.append('"');
		return sb.toString();
	}

}
