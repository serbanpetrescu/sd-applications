// define a root UI component that exposes the main view
jQuery.sap.declare("sd.a3.ui.login.Component");
jQuery.sap.require("sap.ui.core.UIComponent");
jQuery.sap.require("sap.ui.core.routing.History");
jQuery.sap.require("sap.m.routing.RouteMatchedHandler");

sap.ui.core.UIComponent.extend("sd.p.ui.login.Component", {
    metadata : {
        "name" : "login",
        "version" : "1.1.0-SNAPSHOT",
        "library" : "sd.p.ui.login",
        "includes" : [ ],
        "dependencies" : {
            "libs" : [ "sap.m", "sap.ui.layout" ],
            "components" : []
        },
		"config" : {
			resourceBundle : "i18n/messageBundle.properties"
		}
    },

    /**
     * Initialize the application
     *
     * @returns {sap.ui.core.Control} the content
     */
    createContent : function() {
        var oViewData = {
            component : this
        };

        return sap.ui.view({
            viewName : "sd.p.ui.login.view.Main",
            type : sap.ui.core.mvc.ViewType.XML,
            viewData : oViewData
        });
    },

    init : function() {
        // call super init (will call function "create content")
        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        // always use absolute paths relative to our own component
        // (relative paths will fail if running in the Fiori Launchpad)
        var sRootPath = jQuery.sap.getModulePath("sd.p.ui.login");

        // the metadata is read to get the location of the i18n language files later
        var mConfig = this.getMetadata().getConfig();


        // set i18n model
        var i18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl : [ sRootPath, mConfig.resourceBundle ].join("/")
        });
        this.setModel(i18nModel, "i18n");

    }
});