package sd.a3.com;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

import sd.a3.bl.UserBO;
import sd.a3.db.Cleanup;

/**
 * Helper class for propagating the HTTP user session to the {@link WSEndpoint}.
 * @author Serban Petrescu
 *
 */
public class WSHttpSessionConfigurator extends ServerEndpointConfig.Configurator
{
	public static final String PROP_USERNAME = "username";
	public static final String PROP_ISDOCTOR = "is_doctor";

    @Override
    public void modifyHandshake(ServerEndpointConfig config,
                                HandshakeRequest request,
                                HandshakeResponse response)
    {
        HttpSession httpSession = (HttpSession)request.getHttpSession();
        String username = (String)httpSession.getAttribute(SessionServlet.SESSION_USER);
        Boolean doctor = false;
        if (username != null) {
        	try {
				UserBO.setLoggedInUser(username);
				if (UserBO.getLoggedInUser().hasRole(UserBO.ROLE_DOCTOR)) {
					doctor = true;
				}
        	}
        	catch (Exception e) { }
        	finally {
        		Cleanup.INSTANCE.doCleanup();
        	}
		}
        config.getUserProperties().put(PROP_USERNAME, username);
        config.getUserProperties().put(PROP_ISDOCTOR, doctor);
    }
}