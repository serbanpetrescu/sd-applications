package sd.p.err;

public enum GeneralError {

	/* Technical: */
	NAMING_EXCEPTION		(1000, false),
	INCOMPLETE_REQUEST		(1101, false),
	MALFORMED_REQUEST		(1102, false),
	UNABLE_TO_OUTPUT		(1103, false),
	UNABLE_TO_CONNECT_OO	(1104, false),
	DOCUMENT_STORE_ERROR	(1105, false),
	TEMPORARY_FILE_ERROR	(1106, false),

	/* General BOs: */
	BO_ALREADY_EXISTS		(2000, true),
	BO_NOT_FOUND			(2001, true),
	INVALID_NAME		 	(2002, true),

	/* Users: */
	USER_ALREADY_AUTH		(4001, true),
	USER_NOT_AUTH			(4002, false),
	USER_NOT_OWNER			(4003, false),
	USER_PWD_FAIL			(4004, false),
	USER_ALREADY_HAS_ROLE	(4005, true),
	INVALID_USERNAME		(4006, true),
	USER_NO_ROLE_FOR_GROUP	(4007, true),
	USER_NO_ROLE_FOR_TEMPL	(4008, true),

	/* Logs: */
	LOGS_ARE_READONLY		(5000, false),

	/* Templates: */
	TEMPLATE_ALREADY_IN_GRP (6000, true);
	
	
	private GeneralError(int code, boolean params) {
		this.code = code;
		this.params = params;
	}

	private int code;
	private boolean params;

	public int getCode() {
		return code;
	}
	public boolean hasParams() {
		return params;
	}

}
