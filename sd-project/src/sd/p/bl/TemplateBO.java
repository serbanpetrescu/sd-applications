package sd.p.bl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.LinkedList;

import sd.p.db.DatabaseToolbox;
import sd.p.db.Template;
import sd.p.doc.DocumentManager;
import sd.p.doc.DocumentManager.DocumentNotFoundException;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public class TemplateBO extends BusinessObject<Template, String> {


	public static Collection<TemplateBO> search(String name) {
		if (UserBO.getLoggedInUser().getIsOwner()) {
			return createFromSet(DatabaseToolbox.searchTemplates(name));
		}
		else {
			Collection<TemplateBO> templates = UserBO.getLoggedInUser().getAccessibleTemplates(); 
			Collection<TemplateBO> result = new LinkedList<TemplateBO>();
			name = name.toLowerCase();
			for (TemplateBO t : templates) {
				if (t.getName().toLowerCase().contains(name)) {
					result.add(t);
				}
			}
			return result;
		}
	}
	
	public byte[] getDocumentFile() {
		try {
			return DocumentManager.INSTANCE.getDocumentBytes(this.dbObject.getDocument());
		} catch (DocumentNotFoundException | IOException e) {
			throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
		}
	}
	
	public void updateDocument(InputStream in, long size) {
		UserBO.checkLoggedInUserAuthorization();
		try {
			DocumentManager.INSTANCE.updateDocument(this.getDocument(), in, size);
		} catch (DocumentNotFoundException e) {
			throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
		}
	}
	
	protected TemplateBO(String dbKey) {
		super(Template.class, dbKey);
	}

	protected TemplateBO(Template dbObject, boolean check){
		super(Template.class, dbObject, check);
	}

	public static TemplateBO create(String name, String description) {
		UserBO.checkLoggedInUserAuthorization();
		validateName(name);
		TemplateBO t =  new TemplateBO(new Template(name, null,
				description), true);
		DatabaseToolbox.begin();
		t.dbObject.setDocument(DocumentManager.INSTANCE.createTemplateDocument(name));
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.TEMPL_CREATED, name);
		return t;
	}

	public static TemplateBO get(String name) {
		if (UserBO.getLoggedInUser().getIsOwner()) {
			return new TemplateBO(name);
		}
		else {
			Collection<TemplateBO> templs = UserBO.getLoggedInUser().getAccessibleTemplates();
			for (TemplateBO t : templs) {
				if (t.getName().equals(name)) {
					return t;
				}
			}
			throw new GeneralException(GeneralError.USER_NO_ROLE_FOR_TEMPL, name);
		}
	}

	public void delete() {
		UserBO.checkLoggedInUserAuthorization();
		String name = this.getName();
		String document = this.getDocument();
		this.deleteBase();
		DocumentManager.INSTANCE.deleteTemplateDocument(document);
		LogTM.getInstance().create(LogTM.Description.TEMPL_DELETED, name);
	}

	public void setDescription(String description) {
		UserBO.checkLoggedInUserAuthorization();
		DatabaseToolbox.begin();
		this.dbObject.setDescription(description);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.TEMPL_UPDATED, this.getName());
	}
	
	public String getName() {
		return this.dbObject.getName();
	}

	public String getDocument() {
		return this.dbObject.getDocument();
	}

	public String getDescription() {
		return this.dbObject.getDescription();
	}

	protected static Collection<TemplateBO> createFromSet(
			Collection<Template> resultSet) {
		Collection<TemplateBO> templates = new LinkedList<TemplateBO>();
		for (Template t : resultSet) {
			templates.add(new TemplateBO(t, false));
		}
		return templates;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TemplateBO && obj != null) {
			return this.getName().equals(((TemplateBO)obj).getName());
		}
		else {
			return super.equals(obj);
		}
	}
}
