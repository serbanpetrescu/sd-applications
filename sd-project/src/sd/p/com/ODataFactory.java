package sd.p.com;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.olingo.odata2.annotation.processor.core.edm.AnnotationEdmProvider;
import org.apache.olingo.odata2.api.ODataService;
import org.apache.olingo.odata2.api.ODataServiceFactory;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.processor.ODataContext;

import sd.p.db.Group;
import sd.p.db.Log;
import sd.p.db.Role;
import sd.p.db.SimpleField;
import sd.p.db.Template;
import sd.p.db.User;
import sd.p.doc.BuildDocument;

public class ODataFactory extends ODataServiceFactory {

	@Override
	public ODataService createService(ODataContext ctx) throws ODataException {
		Collection<Class<?>> classes = new LinkedList<Class<?>>();
		classes.add(User.class);
		classes.add(Role.class);
		classes.add(Group.class);
		classes.add(Template.class);
		classes.add(Log.class);
		classes.add(SimpleField.class);
		classes.add(BuildDocument.class);
		return createODataSingleProcessorService(
				new AnnotationEdmProvider(classes),
				new ODataSProcessor());
	}

}
