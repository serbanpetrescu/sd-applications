package sd.a3.bl;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.naming.NamingException;

import sd.a3.com.WSEndpoint;
import sd.a3.db.Cleanup;
import sd.a3.db.DatabaseToolbox;
import sd.a3.db.User;

/**
 * Business object for the User entity. Users can be Observers for Patients.
 *
 * @author Serban Petrescu
 *
 */
public class UserBO extends BusinessObject<String, User> implements Observer {

	private static final String DEFAULT_PASSWORD = "init";
	private static final String DEFAULT_USER = "admin";
	public static int ROLE_ANY = -1;
	public static int ROLE_ADMIN = 1;
	public static int ROLE_SECRETARY = 2;
	public static int ROLE_DOCTOR = 4;

	/**
	 * Reads a user form the DB can builds a BO.
	 *
	 * @param username
	 * @return The UserBO (if found).
	 */
	public static UserBO get(String username) {
		UserBO user = new UserBO(username);
		user.checkLoggedInUserAuthorizationSelf(ROLE_ADMIN | ROLE_SECRETARY);
		return user;
	}

	/**
	 * Searches all users by a substring of their username.
	 *
	 * @param username
	 * @return A list of users.
	 */
	public static Collection<UserBO> searchByUsername(String username) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_ADMIN);
		try {
			Collection<User> c = DatabaseToolbox
					.searchUsersByUsername(username);
			Collection<UserBO> result = new LinkedList<UserBO>();
			for (User u : c) {
				result.add(new UserBO(u));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Searches all doctors by a substring of their name.
	 *
	 * @param name
	 * @return A list of doctors.
	 */
	public static Collection<UserBO> searchDoctorsByName(String name) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_ADMIN
				| UserBO.ROLE_SECRETARY);
		try {
			Collection<User> c = DatabaseToolbox.searchUsersByNameAndRole(name,
					ROLE_DOCTOR);
			Collection<UserBO> result = new LinkedList<UserBO>();
			for (User u : c) {
				result.add(new UserBO(u));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Searches all users by a substring of their name.
	 *
	 * @param name
	 * @return A list of users.
	 */
	public static Collection<UserBO> searchByName(String name) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_ADMIN);
		try {
			Collection<User> c = DatabaseToolbox.searchUsersByName(name);
			Collection<UserBO> result = new LinkedList<UserBO>();
			for (User u : c) {
				result.add(new UserBO(u));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Deletes a user.
	 */
	public void delete() {
		this.checkLoggedInUserAuthorizationSelf(UserBO.ROLE_ADMIN);
		if (!this.getUsername().equals(UserBO.getLoggedInUser().getUsername())) {
			if (!DEFAULT_USER.equals(this.getUsername())) {
				this._delete();
			} else {
				throw new BusinessException(
						BusinessError.USER_AUTHORITY_CHK_FAILED);
			}
		} else {
			throw new BusinessException(BusinessError.USER_AUTHORITY_CHK_FAILED);
		}
	}

	/**
	 * Creates a new user.
	 *
	 * @param username
	 * @param name
	 * @param role
	 * @return The new UserBO (if validations were successful).
	 */
	public static UserBO create(String username, String name, Integer role) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_ADMIN);
		UserBO.validateUsername(username);
		UserBO.validateName(name);
		UserBO.validateRole(role);
		return new UserBO(username, DEFAULT_PASSWORD, name, role);
	}

	/**
	 * Updates the user's information.
	 *
	 * @param name
	 * @param role
	 */
	public void update(String name, Integer role) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_ADMIN);
		try {
			DatabaseToolbox.begin();
			if (name != null) {
				UserBO.validateName(name);
				this.reference.setName(name);
			}
			if (role != null) {
				UserBO.validateRole(role);
				this.reference.setRole(role);
			}
			DatabaseToolbox.commit();
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Checks if the logged in user has a given role combination or if the
	 * logged in user is actually the same as the current instance. Roles can be
	 * combined using the binary or operator |.
	 *
	 * @param role
	 *            The role combination.
	 */
	public void checkLoggedInUserAuthorizationSelf(int role) {
		if (!getLoggedInUser().hasRole(role)
				&& !getLoggedInUser().getName().equals(this.getName())) {
			throw new BusinessException(
					BusinessError.USER_AUTHORITY_CHK_FAILED,
					Integer.toString(role));
		}
	}

	/**
	 * Checks if the logged in user has a given role combination. Roles can be
	 * combined using the binary or operator |.
	 *
	 * @param role
	 *            The role combination.
	 */
	public static void checkLoggedInUserAuthorization(int role) {
		if (!getLoggedInUser().hasRole(role)) {
			throw new BusinessException(
					BusinessError.USER_AUTHORITY_CHK_FAILED,
					Integer.toString(role));
		}
	}

	/**
	 * Gets the currently logged in user.
	 *
	 * @return
	 */
	public static UserBO getLoggedInUser() {
		UserBO u = authUser.get();
		if (u != null) {
			return u;
		} else {
			throw new BusinessException(BusinessError.USER_NOT_AUTH);
		}
	}

	/**
	 * Performs authentication and sets the currently logged in user if
	 * successful.
	 *
	 * @param username
	 * @param password
	 */
	public static void setLoggedInUser(String username, String password) {
		if (authUser.get() == null) {
			UserBO u = new UserBO(username);
			if (u.reference.getPassword().equals(password)) {
				authUser.set(u);
			} else {
				throw new BusinessException(BusinessError.USER_PWD_FAIL);
			}
		} else {
			throw new BusinessException(BusinessError.USER_ALREADY_AUTH,
					authUser.get().getUsername());
		}
	}

	/**
	 * Sets the logged in user without authentication (for session restore).
	 *
	 * @param username
	 */
	public static void setLoggedInUser(String username) {
		if (authUser.get() == null) {
			authUser.set(new UserBO(username));
		} else if (!authUser.get().getUsername().equals(username)) {
			throw new BusinessException(BusinessError.USER_ALREADY_AUTH,
					authUser.get().getUsername());
		}
	}

	/**
	 * Checks if this instance has a role combination.
	 *
	 * @param role
	 *            The role combination.
	 * @return
	 */
	public boolean hasRole(int role) {
		if (role == -1 || (this.getRole() & role) != 0) {
			return true;
		} else {
			return false;
		}
	}

	public String getUsername() {
		return this.reference.getUsername();
	}

	public String getName() {
		return this.reference.getName();
	}

	public Integer getRole() {
		return this.reference.getRole();
	}

	protected static ThreadLocal<UserBO> authUser = new ThreadLocal<UserBO>();
	static {
		Cleanup.INSTANCE.registerCleaner(new Cleanup.Wrapper() {
			public void doCleanup() {
				authUser.remove();
			}
		});
	}

	protected UserBO(String username) {
		super(User.class, username);
	}

	protected UserBO(String username, String password, String name, Integer role) {
		super(User.class, new User(username, password, name, role));
	}

	protected UserBO(User reference) {
		super(User.class, reference, false);
	}

	private static void validateRole(int role) {
		if (role != ROLE_ADMIN && role != ROLE_DOCTOR && role != ROLE_SECRETARY) {
			throw new BusinessException(BusinessError.ROLE_VF,
					Integer.toString(role));
		}
	}

	private static void validateUsername(String username) {
		if (!username.matches("^[a-zA-Z][a-zA-Z_0-9]*$")) {
			throw new BusinessException(BusinessError.USERNAME_VF, username);
		}
	}

	private static void validateName(String name) {
		if (!name.matches("^[A-Z][a-zA-Z '-]+$")) {
			throw new BusinessException(BusinessError.NAME_VF, name);
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		ConsultationBO c = (ConsultationBO) arg1;
		if (this.getUsername().equals(c.getDoctor().getUsername())) {
			WSEndpoint ws = WSEndpoint.clients.get(this.getUsername());
			if (ws != null) {
				try {
					ws.sendNotification(c.getPatient().getName(),
							c.getStartTime());
				} catch (IOException e) {
					throw new BusinessException(
							BusinessError.DOCTOR_NOT_ONLINE, this.getName());
				}
			} else {
				throw new BusinessException(BusinessError.WS_ERROR);
			}
		}
	}
}
