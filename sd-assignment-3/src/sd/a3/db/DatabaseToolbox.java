package sd.a3.db;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import javax.naming.NamingException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 * Helper class with static convenience methods for DB access.
 *
 * @author Serban Petrescu
 *
 */
public class DatabaseToolbox {

	/**
	 * Start a DB transaction.
	 *
	 * @throws NamingException
	 */
	public static void begin() throws NamingException {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
	}

	/**
	 * Commit the DB transaction.
	 *
	 * @throws NamingException
	 */
	public static void commit() throws NamingException {
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	/**
	 * Create (persist) the given object in the DB.
	 *
	 * @param o
	 * @throws NamingException
	 */
	public static void persist(Object o) throws NamingException {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
		EmfFactory.INSTANCE.getEm().persist(o);
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	/**
	 * Remove the given object from the DB.
	 *
	 * @param o
	 * @throws NamingException
	 */
	public static void remove(Object o) throws NamingException {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
		EmfFactory.INSTANCE.getEm().remove(o);
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	/**
	 * Remove the given consultation from the DB. The consultations needed a
	 * special method for removal, because of the bidirectional relationship
	 * between them and their patients.
	 *
	 * @param c
	 * @throws NamingException
	 */
	public static void remove(Consultation c) throws NamingException {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
		c.getPatient().getConsultations().remove(c);
		EmfFactory.INSTANCE.getEm().remove(c);
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	/**
	 * @see #checkExistence
	 * @param username
	 * @return
	 * @throws NamingException
	 */
	public static boolean existsUser(String username) throws NamingException {
		return checkExistence(User.class, username);

	}

	/**
	 * @see #checkExistence
	 * @param personalNo
	 * @return
	 * @throws NamingException
	 */
	public static boolean existsPatient(String personalNo)
			throws NamingException {
		return checkExistence(Patient.class, personalNo);

	}

	/**
	 * @see #checkExistence
	 * @param uuid
	 * @return
	 * @throws NamingException
	 */
	public static boolean existsConsultation(String uuid)
			throws NamingException {
		return checkExistence(Consultation.class, uuid);

	}

	/**
	 * Checks if an object of a given class, with a given key, exists.
	 * @param clazz The class of the object.
	 * @param o The key of the entity.
	 * @return True if the entity exists.
	 * @throws NamingException
	 */
	public static boolean checkExistence(Class<?> clazz, Object o)
			throws NamingException {
		try {
			return EmfFactory.INSTANCE.getEm().getReference(clazz, o) != null;
		} catch (EntityNotFoundException e) {
			return false;
		}
	}

	/**
	 * @see #find
	 * @param username
	 * @return
	 * @throws NamingException
	 */
	public static User findUser(String username) throws NamingException {
		return find(User.class, username);
	}

	/**
	 * @see #find
	 * @param personalNo
	 * @return
	 * @throws NamingException
	 */
	public static Patient findPatient(String personalNo) throws NamingException {
		return find(Patient.class, personalNo);
	}

	/**
	 * @see #find
	 * @param uuid
	 * @return
	 * @throws NamingException
	 */
	public static Consultation findConsultation(String uuid)
			throws NamingException {
		return find(Consultation.class, uuid);
	}

	/**
	 * Find an entity of a given class, with a given key.
	 * @param clazz The class of the entity.
	 * @param key The key of the entity.
	 * @return
	 * @throws NamingException
	 */
	public static <K, T extends PersistentEntity<K>> T find(Class<T> clazz,
			K key) throws NamingException {
		return EmfFactory.INSTANCE.getEm().find(clazz, key);
	}

	/**
	 * Get the next consultation of a given patient.
	 * @param p The patient.
	 * @return A consultation or null if none was found.
	 * @throws NamingException
	 */
	public static Consultation getNextConsultation(Patient p)
			throws NamingException {
		TypedQuery<Consultation> q = EmfFactory.INSTANCE.getEm()
				.createNamedQuery("Consultation.findNextByPatient",
						Consultation.class);
		q.setMaxResults(1);
		q.setParameter("now", new Date(), TemporalType.TIMESTAMP);
		q.setParameter("patient", p);
		try {
			return q.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Get all the open consultations for a patient (consultations which have no result attached).
	 * @param p The patient.
	 * @return A collection of consultations.
	 * @throws NamingException
	 */
	public static Collection<Consultation> getOpenConsultations(Patient p)
			throws NamingException {
		TypedQuery<Consultation> q = EmfFactory.INSTANCE.getEm()
				.createNamedQuery("Consultation.findOpenByPatient",
						Consultation.class);
		q.setParameter("patient", p);
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<Consultation>();
		}
	}

	/**
	 * Get all the past consultations for a patient.
	 * @param p The patient.
	 * @return A collection of consultations.
	 * @throws NamingException
	 */
	public static Collection<Consultation> getPastConsultations(Patient p)
			throws NamingException {
		TypedQuery<Consultation> q = EmfFactory.INSTANCE.getEm()
				.createNamedQuery("Consultation.findPastByPatient",
						Consultation.class);
		q.setParameter("now", new Date(), TemporalType.TIMESTAMP);
		q.setParameter("patient", p);
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<Consultation>();
		}
	}

	/**
	 * Get all the past consultations for a doctor.
	 * @param u The doctor.
	 * @return A collection of consultations.
	 * @throws NamingException
	 */
	public static Collection<Consultation> getFutureConsultations(User u)
			throws NamingException {
		TypedQuery<Consultation> q = EmfFactory.INSTANCE.getEm()
				.createNamedQuery("Consultation.findFutureByDoctor",
						Consultation.class);
		// q.setParameter("now", new Date(), TemporalType.TIMESTAMP);
		q.setParameter("doctor", u);
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<Consultation>();
		}
	}

	/**
	 * Searches all users by name (using a substring) and role (using a precise value).
	 * @param value A substring of the user's name.
	 * @param role The role of the user.
	 * @return A collection of users.
	 * @throws NamingException
	 */
	public static Collection<User> searchUsersByNameAndRole(String value,
			Integer role) throws NamingException {
		TypedQuery<User> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"User.findByNameAndRole", User.class);
		q.setParameter("value", "%" + value.toLowerCase() + "%");
		q.setParameter("role", role);
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<User>();
		}
	}

	/**
	 * Searches all users by name (using a substring).
	 * @param value A substring of the user's name.
	 * @return A collection of users.
	 * @throws NamingException
	 */
	public static Collection<User> searchUsersByName(String value)
			throws NamingException {
		TypedQuery<User> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"User.findByName", User.class);
		q.setParameter("value", "%" + value.toLowerCase() + "%");
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<User>();
		}
	}

	/**
	 * Searches all users by username (using a substring).
	 * @param value A substring of the username.
	 * @return A collection of users.
	 * @throws NamingException
	 */
	public static Collection<User> searchUsersByUsername(String value)
			throws NamingException {
		TypedQuery<User> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"User.findByUsername", User.class);
		q.setParameter("value", "%" + value.toLowerCase() + "%");
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<User>();
		}
	}

	/**
	 * Searches all patients by name (using a substring).
	 * @param value A substring of the patients' name.
	 * @return A collection of patients.
	 * @throws NamingException
	 */
	public static Collection<Patient> searchPatientsByName(String value)
			throws NamingException {
		TypedQuery<Patient> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"Patient.findByName", Patient.class);
		q.setParameter("value", "%" + value.toLowerCase() + "%");
		try {
			return q.getResultList();
		} catch (Exception e) {
			return new LinkedList<Patient>();
		}
	}
}
