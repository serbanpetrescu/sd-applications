package sd.a3.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sd.a3.bl.BusinessError;
import sd.a3.bl.BusinessException;
import sd.a3.bl.UserBO;
import sd.a3.db.Cleanup;

/**
 * Servlet for managing user sessions (login, logout).
 *
 * @author Serban Petrescu
 *
 */
@WebServlet(name = "SessionManager", displayName = "SessionManager", urlPatterns = { "/session" })
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String SESSION_USER = "username";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_PASSWORD = "password";
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_LOGOUT = "logout";
	public static final String ACTION_CHECK = "check";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			String username = (String) req.getSession().getAttribute(
					SESSION_USER);
			if (username != null) {
				UserBO.setLoggedInUser(username);
			}
			String action = req.getParameter(PARAM_ACTION);
			if (ACTION_LOGIN.equalsIgnoreCase(action)) {
				username = req.getParameter(PARAM_USERNAME);
				String password = req.getParameter(PARAM_PASSWORD);
				if (username != null && password != null) {
					UserBO.setLoggedInUser(username, password);
					req.getSession().setAttribute(SESSION_USER, username);
					resp.setStatus(200);
					resp.getWriter().println(
							"{\"role\": " + UserBO.getLoggedInUser().getRole()
									+ "}");
					resp.getWriter().close();
				} else {
					throw new BusinessException(
							BusinessError.PARAM_NOT_SUPPLIED, PARAM_USERNAME,
							PARAM_PASSWORD);
				}
			} else if (ACTION_LOGOUT.equalsIgnoreCase(action)) {
				req.getSession().removeAttribute(SESSION_USER);
				resp.setStatus(204);
			} else if (ACTION_CHECK.equalsIgnoreCase(action)) {
				try {
					int role = UserBO.getLoggedInUser().getRole();
					resp.setStatus(200);
					resp.getWriter().println("{\"role\": " + role + "}");
					resp.getWriter().close();
				} catch (BusinessException e) {
					resp.setStatus(204);
				}
			} else {
				throw new BusinessException(BusinessError.PARAM_NOT_SUPPLIED,
						PARAM_ACTION);
			}
		} catch (BusinessException e) {
			resp.setStatus(403);
			resp.setContentType("application/json");
			resp.getWriter().print(e.getMessage());
			resp.getWriter().close();
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

}
