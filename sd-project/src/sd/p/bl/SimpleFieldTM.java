package sd.p.bl;

import java.util.Collection;

import sd.p.db.DatabaseToolbox;
import sd.p.db.SimpleField;

public class SimpleFieldTM extends TableModule<SimpleField, String> {

	private static volatile SimpleFieldTM instance = null;

	public static SimpleFieldTM getInstance() {
		if (instance == null) {
			synchronized (SimpleFieldTM.class) {
				if (instance == null) {
					instance = new SimpleFieldTM();
				}
			}
		}
		return instance;
	}

	private SimpleFieldTM() {
		super(SimpleField.class);
	}
	
	public Collection<SimpleField> search(String name) {
		return DatabaseToolbox.searchSimpleFields(name);
	}
	
	@Override
	public void delete(String key) {
		super.delete(key);
		LogTM.getInstance().create(LogTM.Description.FIELD_DELETED, key);
	}

	public void update(String name, String description,
			String validationFunction, String validationParameter){
		UserBO.checkLoggedInUserAuthorization();
		SimpleField field = this.get(name);
		DatabaseToolbox.begin();
		field.setDescription(description);
		field.setValidationFunction(validationFunction);
		field.setValidationParameter(validationParameter);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.FIELD_UPDATED, name);
	}

	public SimpleField create(String name, String description,
			String validationFunction, String validationParameter) {
		UserBO.checkLoggedInUserAuthorization();
		validateName(name);
		checkUnique(name);
		SimpleField f = persistBase(new SimpleField(name, validationFunction,
				validationParameter, description));
		LogTM.getInstance().create(LogTM.Description.FIELD_CREATED, name);
		return f;
	}

	
}
