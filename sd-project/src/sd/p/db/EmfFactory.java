package sd.p.db;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public enum EmfFactory {

	INSTANCE;

	public static final String DS_NAME = "java:comp/env/jdbc/DefaultDB";
	public static final String PU_NAME = "sd-project";
	
	private EmfFactory(){
		InitialContext ctx = null;
		try {
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(DS_NAME);
	        Map<String, Object> properties = new HashMap<String, Object>();
	        properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
	        this.emf = Persistence.createEntityManagerFactory(PU_NAME, properties); 
	        this.em = new ThreadLocal<EntityManager>();

			Cleanup.INSTANCE.registerCleaner(new Cleanup.Wrapper() {
				public void doCleanup() {
					em.remove();
				}
			});
			
		} catch (NamingException e) {
			this.emf = null;
			this.em = null;
		}
		finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) { }
			}
		}
	}
	
	private EntityManagerFactory emf;
	private ThreadLocal<EntityManager> em;
	
	
	public EntityManagerFactory getEmf() {
		if (emf == null) {
			throw new GeneralException(GeneralError.NAMING_EXCEPTION);
		}
		return emf;
	}
	
	public EntityManager getEm() {
		if (em == null) {
			throw new GeneralException(GeneralError.NAMING_EXCEPTION);
		}
		if (em.get() == null) {
			em.set(emf.createEntityManager());
		}
		return em.get();
	}
}
