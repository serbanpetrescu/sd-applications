package sd.a3.bl;

/**
 * Enum for all error types thrown by the application.
 *
 * @author Serban Petrescu
 *
 */
public enum BusinessError {

	/**
	 * Thrown when the user attempts to create an entity which has a duplicate
	 * PK.
	 */
	ALREADY_EXISTS(1001, true),
	/**
	 * Thrown when the specified PK is not found.
	 */
	ENTITY_DOESNT_EXIST(1002, true),
	/**
	 * Thrown when an entity was previously deleted (and a subsequent read /
	 * delete attempt is performed).
	 */
	ENTITY_ALREADY_DELETED(1003, true),
	/**
	 * Name validation failed.
	 */
	NAME_VF(1004, true),
	/**
	 * Username validation failed.
	 */
	USERNAME_VF(1005, true),
	/**
	 * Role validation failed.
	 */
	ROLE_VF(1006, true),
	/**
	 * Personal no (RO format) validation failed.
	 */
	PERSONAL_NO_VF(1007, true),
	/**
	 * Identity card number validation failed.
	 */
	ID_CARD_NO_VF(1008, true),
	/**
	 * Birthday validation failed (e.g. birthday in the future)
	 */
	BIRTHDAY_VF(1009, true),
	/**
	 * Time of day validation failed.
	 */
	TIME_VF(1010, false),
	/**
	 * The specified doctor is busy at the given time period.
	 */
	DOCTOR_BUSY_VF(1011, true),
	/**
	 * The specified user is not a doctor.
	 */
	NOT_A_DOCTOR(1012, true),
	/**
	 * The given patient has no future consultation(s).
	 */
	NO_CONSULTATION_FOUND(1013, false),
	/**
	 * The responsible doctor is not online.
	 */
	DOCTOR_NOT_ONLINE(1014, true),
	/**
	 * Unknown websocket error.
	 */
	WS_ERROR(1015, false),

	/**
	 * Unable to reach the persistence service (JNDI lookup failed).
	 */
	PERSIST_UNABLE(2001, false),

	/**
	 * The user is already authenticated on the system.
	 */
	USER_ALREADY_AUTH(4001, true),
	/**
	 * Authentication is needed to perform an action.
	 */
	USER_NOT_AUTH(4002, false),
	/**
	 * User has insufficient roles / privileges.
	 */
	USER_AUTHORITY_CHK_FAILED(4003, true),
	/**
	 * Wrong password.
	 */
	USER_PWD_FAIL(4004, false),

	/**
	 * A mandatory HTTP parameter was not given.
	 */
	PARAM_NOT_SUPPLIED(9001, true),
	/**
	 * The HTTP request is incomplete.
	 */
	INCOMPLETE_REQUEST(9002, false);

	private BusinessError(int code, boolean params) {
		this.code = code;
		this.params = params;
	}

	private int code;
	private boolean params;

	/**
	 * The code is used by the front-end to determine the text resource ID for a
	 * given error.
	 *
	 * @return the code number.
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Parameters are used to fill in placeholders in the error text.
	 *
	 * @return whether the error can have parameters.
	 */
	public boolean hasParams() {
		return params;
	}

}
