package sd.a3.db;

/**
 * Denotes an entity which is persisted in the database.
 *
 * @author Serban Petrescu
 *
 * @param <K>
 *            The type of the PK.
 */
public interface PersistentEntity<K> {
	/**
	 * @return The PK of the DB entity.
	 */
	K getKey();
}
