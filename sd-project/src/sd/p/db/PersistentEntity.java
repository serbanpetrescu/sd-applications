package sd.p.db;

public interface PersistentEntity<K> {
	K getKey();
}
