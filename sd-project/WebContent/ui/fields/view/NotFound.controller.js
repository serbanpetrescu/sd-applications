/* global logoff */
sap.ui.core.mvc.Controller.extend("sd.p.ui.fields.view.NotFound", {
    onLogoff: function() {
        logoff();
    }
});