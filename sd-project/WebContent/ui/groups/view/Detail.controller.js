/* global showError, logoff */
sap.ui.core.mvc.Controller.extend("sd.p.ui.groups.view.Detail", {

	onInit : function() {
		this.oInitialLoadFinishedDeferred = jQuery.Deferred();

		if(sap.ui.Device.system.phone) {
			//Do not wait for the master when in mobile phone resolution
			this.oInitialLoadFinishedDeferred.resolve();
		} else {
			this.getView().setBusy(true);
			var oEventBus = this.getEventBus(); 
			oEventBus.subscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
			oEventBus.subscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		}

		this.getRouter().attachRouteMatched(this.onRouteMatched, this);
	},

	onMasterLoaded :  function (sChannel, sEvent) {
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
	},
	
	onMetadataFailed : function(){
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
        this.showEmptyView();	    
	},

	onRouteMatched : function(oEvent) {
		var oParameters = oEvent.getParameters();

		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(function () {
			var oView = this.getView();

			// When navigating in the Detail page, update the binding context 
			if (oParameters.name !== "detail") { 
				return;
			}

			var sEntityPath = "/" + oParameters.arguments.entity;
			this.bindView(sEntityPath);

			var oIconTabBar = oView.byId("idIconTabBar");
			oIconTabBar.getItems().forEach(function(oItem) {
			    if(oItem.getKey() !== "selfInfo"){
    				oItem.bindElement(oItem.getKey());
			    }
			});

			// Specify the tab being focused
			var sTabKey = oParameters.arguments.tab;
			this.getEventBus().publish("Detail", "TabChanged", { sTabKey : sTabKey });

			if (oIconTabBar.getSelectedKey() !== sTabKey) {
				oIconTabBar.setSelectedKey(sTabKey);
			}
		}, this));

	},
	
	refreshRole: function() {
		var success = jQuery.proxy(function(oData){
			this.byId("roleName").setText(oData.Name);
			this.byId("roleDesc").setText(oData.Description);
			this.byId("unlockButton").setVisible(!!oData.Name);
		}, this);
		this.getView().getModel().read(this.getView().getBindingContext().getPath() + "/Role/", 
			{success: success, error: success});

	},

	bindView : function (sEntityPath) {
		var oView = this.getView();
		oView.bindElement(sEntityPath); 
		this.refreshRole();
		
		if(!oView.getModel().getData(sEntityPath)) {

			// Check that the entity specified was found.
			oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
				var oData = oView.getModel().getData(sEntityPath);
				if (!oData) {
					this.showEmptyView();
					this.fireDetailNotFound();
				} else {
					this.fireDetailChanged(sEntityPath);
				}
			}, this));

		} else {
			this.fireDetailChanged(sEntityPath);
		}

	},

	showEmptyView : function () {
		this.getRouter().myNavToWithoutHash({ 
			currentView : this.getView(),
			targetViewName : "sd.p.ui.groups.view.NotFound",
			targetViewType : "XML"
		});
	},

	fireDetailChanged : function (sEntityPath) {
		this.getEventBus().publish("Detail", "Changed", { sEntityPath : sEntityPath });
	},

	fireDetailNotFound : function () {
		this.getEventBus().publish("Detail", "NotFound");
	},

	onNavBack : function() {
		// This is only relevant when running on phone devices
		this.getRouter().myNavBack("main");
	},

	onDetailSelect : function(oEvent) {
		sap.ui.core.UIComponent.getRouterFor(this).navTo("detail",{
			entity : oEvent.getSource().getBindingContext().getPath().slice(1),
			tab: oEvent.getParameter("selectedKey")
		}, true);
	},

	openActionSheet: function() {

		if (!this._oActionSheet) {
			this._oActionSheet = new sap.m.ActionSheet({
				buttons: new sap.ushell.ui.footerbar.AddBookmarkButton()
			});
			this._oActionSheet.setShowCancelButton(true);
			this._oActionSheet.setPlacement(sap.m.PlacementType.Top);
		}
		
		this._oActionSheet.openBy(this.getView().byId("actionButton"));
	},

	getEventBus : function () {
		return sap.ui.getCore().getEventBus();
	},

	getRouter : function () {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},
	
	onExit : function(oEvent){
	    var oEventBus = this.getEventBus();
    	oEventBus.unsubscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		oEventBus.unsubscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
	},
	
    editEntity: function() {
        if (!this.dialog) {
            this.dialog = sap.ui.xmlfragment(this.getView().getId(), "sd.p.ui.groups.view.CreateDialog", this);
            this.getView().addDependent(this.dialog);
	        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            this.dialog.setTitle(i18n.getText("editDialogTitle"));
            this.byId("dialogInput1").setEnabled(false);
        }

        this.dialog.bindElement(this.getView().getBindingContext().getPath());
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.dialog);
        this.dialog.open(); 
    },

    deleteEntity: function() {
        jQuery.sap.require("sap.ca.ui.dialog.factory");
	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();  
	    var context = this.getView().getBindingContext();
        sap.ca.ui.dialog.confirmation.open({
            question : i18n.getText("confirmDeleteQuestion", context.getProperty("Name")),
            showNote : false,
            title : i18n.getText("confirmDeleteTitle"),
            confirmButtonLabel: i18n.getText("confirmDeleteLabel")
        }, jQuery.proxy(function(oResult) {
            if (oResult.isConfirmed) {
                this.getOwnerComponent().getModel().remove(context.getPath(), {
	            	success: jQuery.proxy(function() {
	            		this.getView().unbindElement();
	                	this.showEmptyView();
	            	}, this),
	            	error: showError
	            });
            }
        }, this));
    },

    onDialogAcceptButton: function() {
	    var context = this.getView().getBindingContext();
        this.getOwnerComponent().getModel().update(context.getPath(), {
            Description: this.byId("dialogInput2").getValue()
         }, {error: showError, success: jQuery.proxy(function(){this.dialog.close();}, this)});
    },

    onDialogCloseButton: function() {
        this.dialog.close();
    },
    
    onLogoff: function() {
        logoff();
    },
       
    onRoleAdd: function() {
        if (!this.roleDialog) {
            this.roleDialog = sap.ui.xmlfragment(this.getView().getId(), "sd.p.ui.groups.view.RoleDialog", this);
            this.getView().addDependent(this.roleDialog);
        }
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.roleDialog);
        this.roleDialog.open(); 
    },
    
    handleDialogSearch: function(oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new sap.ui.model.Filter("Name", sap.ui.model.FilterOperator.Contains, sValue);
        var oBinding = oEvent.getSource().getBinding("items");
        oBinding.filter([oFilter]);
    },
    
    handleAcceptRoleDialog: function(oEvent) {
        var item = oEvent.getParameter("selectedItem");
        if (item) {
            this.getOwnerComponent().getModel().update(
                this.getView().getBindingContext().getPath() + "/$links/Role/",
                { Name: item.getBindingContext().getProperty("Name") }, 
                { error: showError, success: jQuery.proxy(function() {
            		this.refreshRole();
                    this.roleDialog.close();
                }, this)}
            );
        }
    },
    
    handleCloseRoleDialog: function() {
        this.roleDialog.close();
    },
    
    onRoleRemove: function() {
        jQuery.sap.require("sap.ca.ui.dialog.factory");
	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
        sap.ca.ui.dialog.confirmation.open({
            question : i18n.getText("confirmRoleRemoveQuestion"),
            showNote : false,
            title : i18n.getText("confirmRoleRemoveTitle"),
            confirmButtonLabel: i18n.getText("confirmDeleteLabel")
        }, jQuery.proxy(function(oResult) {
            if (oResult.isConfirmed) {
                this.getOwnerComponent().getModel().update(
                    this.getView().getBindingContext().getPath() + "/$links/Role/",
                    { Name: ""},
                    { error: showError, success: jQuery.proxy(function() {
                		this.refreshRole();
                    }, this
                )});
            }
        }, this));
    },

    onTemplateAdd: function() {
        if (!this.templateDialog) {
            this.templateDialog = sap.ui.xmlfragment(this.getView().getId(), 
                "sd.p.ui.groups.view.TemplateDialog", this);
            this.getView().addDependent(this.templateDialog);
        }
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.templateDialog);
        this.templateDialog.open(); 
    },
    
    handleAcceptTemplateDialog: function(oEvent) {
        var item = oEvent.getParameter("selectedItem");
        if (item) {
            this.getOwnerComponent().getModel().create(
                this.getView().getBindingContext().getPath() + "/$links/Templates/",
                { Name: item.getBindingContext().getProperty("Name") }, 
                { error: showError}
            );
        }
    },
    
    handleCloseTemplateDialog: function() {
        this.templateDialog.close();
    },
    
    onTemplateRemove: function() {
        var item = this.byId("templateList").getSelectedItem();
        if (item) {
            this.getOwnerComponent().getModel().remove(
        		this.getView().getBindingContext().getPath() + "/$links/Templates('" +
        		item.getBindingContext().getProperty("Name") + "')", 
        		{success: jQuery.proxy(this.getOwnerComponent().getModel().refresh,
        				this.getOwnerComponent().getModel()), error: showError});
        }
        else {
            var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            jQuery.sap.require("sap.m.MessageToast");
            sap.m.MessageToast.show(i18n.getText("noTemplateSelected"));
        }
    }
});
