package sd.p.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sd.p.bl.BuildManager;
import sd.p.bl.UserBO;
import sd.p.db.Cleanup;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

/**
 * Servlet implementation class BuildServlet
 */
@WebServlet("/build"  )
public class BuildServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ZIP_NAME = "results.zip";
	private static final String BUILD_ID = "document";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			if (request.getParameter(BUILD_ID) != null) {
				response.setContentType("application/json");
				BuildManager.INSTANCE.restore(request.getParameter(BUILD_ID),
						response.getOutputStream());
			}
		} catch (GeneralException e) {
			response.reset();
			response.setStatus(403);
			response.setContentType("application/json");
			response.getWriter().print(e.getMessage());
			response.getWriter().close();
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename = "
					+ ZIP_NAME);
			BuildManager.INSTANCE.build(request.getInputStream(),
					response.getOutputStream(), true);
		} catch (GeneralException e) {
			response.reset();
			response.setStatus(403);
			response.setContentType("application/json");
			response.getWriter().print(e.getMessage());
			response.getWriter().close();
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	private void getAuthenticatedUser(HttpServletRequest request) {
		String username = (String) request.getSession().getAttribute(
				SessionServlet.SESSION_USER);
		if (username != null) {
			UserBO.setLoggedInUser(username);
		} else {
			throw new GeneralException(GeneralError.USER_NOT_AUTH);
		}
	}

}
