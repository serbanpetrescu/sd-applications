package sd.p.bl;

import sd.p.db.DatabaseToolbox;
import sd.p.db.PersistentEntity;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public abstract class TableModule<DB extends PersistentEntity<K>, K> {

	private Class<DB> dbClass;

	protected TableModule(Class<DB> dbClass) {
		this.dbClass = dbClass;
	}

	protected DB persistBase(DB value) {
		DatabaseToolbox.persist(value);
		return value;
	}

	public void delete(K key) {
		UserBO.checkLoggedInUserAuthorization();
		DB value = DatabaseToolbox.find(dbClass, key);
		if (value != null) {
			DatabaseToolbox.remove(value);
		} else {
			throw new GeneralException(GeneralError.BO_NOT_FOUND, dbClass
					.getSimpleName().toLowerCase());
		}
	}

	public DB get(K key) {
		DB value = DatabaseToolbox.find(dbClass, key);
		if (value != null) {
			return value;
		} else {
			throw new GeneralException(GeneralError.BO_NOT_FOUND, dbClass
					.getSimpleName().toLowerCase());
		}
	}

	protected static void validateName(String name) {
		if (!name.matches("^[a-zA-Z][a-zA-Z0-9]*$")) {
			throw new GeneralException(GeneralError.INVALID_NAME, name);
		}
	}

	protected void checkUnique(K value) {
		if (DatabaseToolbox.checkExistence(dbClass, value)) {
			throw new GeneralException(GeneralError.BO_ALREADY_EXISTS, dbClass
					.getSimpleName().toLowerCase());
		}

	}

}
