package sd.p.bl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import sd.p.db.SimpleField;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public class BuildConfiguration {
	private List<GroupBO> groups;
	private List<TemplateBO> templates;
	private Map<SimpleField, String> fields;
	
	public BuildConfiguration() {
		groups = new ArrayList<GroupBO>();
		templates = new ArrayList<TemplateBO>();
		fields = new HashMap<SimpleField, String>();
	}
	
	public BuildConfiguration(Flat flat) {
		this();
		this.fromFlat(flat);
	}
	
	public void fromFlat(Flat flat) {
		this.clear();
		for (String s : flat.getGroups()) {
			this.groups.add(GroupBO.get(s));
		}
		for (String s : flat.getTemplates()) {
			this.templates.add(TemplateBO.get(s));
		}
		for (Map.Entry<String, String> entry : flat.getFields().entrySet()) {
			this.fields.put(SimpleFieldTM.getInstance().get(entry.getKey()), entry.getValue());
		}
	}
	
	public void clear() {
		this.groups.clear();
		this.fields.clear();
		this.templates.clear();
	}
	
	public Flat toFlat() {
		Flat flat = new Flat();
		for (GroupBO s : this.getGroups()) {
			flat.groups.add(s.getName());
		}
		for (TemplateBO s : this.getTemplates()) {
			flat.templates.add(s.getName());
		}
		for (Map.Entry<SimpleField, String> entry : this.getFields().entrySet()) {
			flat.fields.put(entry.getKey().getName(), entry.getValue());
		}
		return flat;
	}
	
	public BuildConfiguration(InputStream in) {
		this();
		try {
			ObjectMapper mapper = new ObjectMapper();
			Flat flat = mapper.readValue(in, Flat.class);
			this.fromFlat(flat);
		} catch (IOException e) {
			throw new GeneralException(GeneralError.MALFORMED_REQUEST);
		}
	}
	
	public void toOutputStream(OutputStream out) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Flat flat = this.toFlat();
			mapper.writeValue(out, flat);
		} catch (IOException e) {
			throw new GeneralException(GeneralError.UNABLE_TO_OUTPUT);
		}
	}
	
	public byte[] toBytes() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Flat flat = this.toFlat();
			return mapper.writeValueAsBytes(flat);
		} catch (IOException e) {
			throw new GeneralException(GeneralError.UNABLE_TO_OUTPUT);
		}
	}
	
	public List<GroupBO> getGroups() {
		return groups;
	}

	public List<TemplateBO> getTemplates() {
		return templates;
	}

	public Map<SimpleField, String> getFields() {
		return fields;
	}
	
	public Collection<TemplateBO> getTemplatesDeep() {
		HashSet<TemplateBO> templates = new HashSet<TemplateBO>(this.templates);
		for (GroupBO g : this.groups) {
			for (TemplateBO t : g.getTemplates()) {
				templates.add(t);
			}
		}
		return templates;
	}

	public static class Flat {
		private List<String> groups;
		private List<String> templates;
		private Map<String, String> fields;
		
		public Flat() {
			groups = new ArrayList<String>();
			templates = new ArrayList<String>();
			fields = new HashMap<String, String>();
		}
		
		public List<String> getGroups() {
			return groups;
		}
		public void setGroups(List<String> groups) {
			this.groups = groups;
		}
		public List<String> getTemplates() {
			return templates;
		}
		public void setTemplates(List<String> templates) {
			this.templates = templates;
		}
		public Map<String, String> getFields() {
			return fields;
		}
		public void setFields(Map<String, String> fields) {
			this.fields = fields;
		}
		
	}
	
}
