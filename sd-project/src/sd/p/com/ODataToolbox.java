package sd.p.com;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmLiteral;
import org.apache.olingo.odata2.api.edm.EdmTyped;
import org.apache.olingo.odata2.api.uri.expression.BinaryExpression;
import org.apache.olingo.odata2.api.uri.expression.BinaryOperator;
import org.apache.olingo.odata2.api.uri.expression.ExpressionVisitor;
import org.apache.olingo.odata2.api.uri.expression.FilterExpression;
import org.apache.olingo.odata2.api.uri.expression.LiteralExpression;
import org.apache.olingo.odata2.api.uri.expression.MemberExpression;
import org.apache.olingo.odata2.api.uri.expression.MethodExpression;
import org.apache.olingo.odata2.api.uri.expression.MethodOperator;
import org.apache.olingo.odata2.api.uri.expression.OrderByExpression;
import org.apache.olingo.odata2.api.uri.expression.OrderExpression;
import org.apache.olingo.odata2.api.uri.expression.PropertyExpression;
import org.apache.olingo.odata2.api.uri.expression.SortOrder;
import org.apache.olingo.odata2.api.uri.expression.UnaryExpression;
import org.apache.olingo.odata2.api.uri.expression.UnaryOperator;

import sd.p.bl.GroupBO;
import sd.p.bl.LogTM;
import sd.p.bl.RoleTM;
import sd.p.bl.SimpleFieldTM;
import sd.p.bl.TemplateBO;
import sd.p.bl.UserBO;
import sd.p.bl.BuildManager;
import sd.p.db.Group;
import sd.p.db.Log;
import sd.p.db.Role;
import sd.p.db.SimpleField;
import sd.p.db.Template;
import sd.p.db.User;
import sd.p.doc.BuildDocument;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;


public class ODataToolbox {

	public static void createSimpleField(Map<String, Object> data) {
		String name = (String) data.get(SimpleField.EDM_PN_NAME);
		String description = (String) data.get(SimpleField.EDM_PN_DESC);
		String validFunc = (String) data.get(SimpleField.EDM_PN_VAL_FUNC);
		String validParam = (String) data.get(SimpleField.EDM_PN_VAL_PARAM);
		if (description != null && validFunc != null && validParam != null && name != null) {
			SimpleFieldTM.getInstance().create(name, description, validFunc, validParam);
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void createRole(Map<String, Object> data) {
		String name = (String) data.get(Role.EDM_PN_NAME);
		String description = (String) data.get(Role.EDM_PN_DESC);
		if (description != null && name != null) {
			RoleTM.getInstance().create(name, description);
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static Map<String, Object> createTemplate(Map<String, Object> data) {
		String name = (String) data.get(Template.EDM_PN_NAME);
		String description = (String) data.get(Template.EDM_PN_DESC);
		if (description != null && name != null) {
			return convertTemplate(TemplateBO.create(name, description));
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}

	public static Map<String, Object> createGroup(Map<String, Object> data) {
		String name = (String) data.get(Group.EDM_PN_NAME);
		String description = (String) data.get(Group.EDM_PN_DESC);
		if (description != null && name != null) {
			return convertGroup(GroupBO.create(name, description));
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static Map<String, Object> createUser(Map<String, Object> data) {
		String username = (String) data.get(User.EDM_PN_USERNAME);
		Boolean isOwner = (Boolean) data.get(User.EDM_PN_IS_OWNER);
		if (isOwner != null && username != null) {
			return convertUser(UserBO.create(username, isOwner));
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void updateSimpleField(String name, Map<String, Object> data) {
		String description = (String) data.get(SimpleField.EDM_PN_DESC);
		String validFunc = (String) data.get(SimpleField.EDM_PN_VAL_FUNC);
		String validParam = (String) data.get(SimpleField.EDM_PN_VAL_PARAM);
		if (description != null && validFunc != null && validParam != null) {
			SimpleFieldTM.getInstance().update(name, description, validFunc, validParam);
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void updateRole(String name, Map<String, Object> data) {
		String description = (String) data.get(Role.EDM_PN_DESC);
		if (description != null) {
			RoleTM.getInstance().update(name, description);
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void updateTemplate(String name, Map<String, Object> data) {
		String description = (String) data.get(Template.EDM_PN_DESC);
		if (description != null) {
			TemplateBO.get(name).setDescription(description);;
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void updateGroup(String name, Map<String, Object> data) {
		String description = (String) data.get(Group.EDM_PN_DESC);
		if (description != null) {
			GroupBO.get(name).setDescription(description);;
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static void updateUser(String username, Map<String, Object> data) {
		Boolean isOwner = (Boolean) data.get(User.EDM_PN_IS_OWNER);
		if (isOwner != null) {
			UserBO.get(username).setIsOwner(isOwner);
		}
		else {
			throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
		}
	}
	
	public static List<Map<String, Object>> getUserLogs(String username) {
		return convertLogs(UserBO.get(username).getLogs());
	}
	
	public static List<Map<String, Object>> getUserRoles(String username) {
		return convertRoles(UserBO.get(username).getRoles());
	}
	
	public static List<Map<String, Object>> getGroupTemplates(String name) {
		return convertTemplates(GroupBO.get(name).getTemplates());
	}
	
	public static List<Map<String, Object>> searchSimpleFields(FilterExpression e) {
		String name = extractSubstringFilter(e, SimpleField.EDM_PN_NAME);
		return convertSimpleFields(SimpleFieldTM.getInstance().search(name));
	}
	
	public static List<Map<String, Object>> searchRoles(FilterExpression e) {
		String name = extractSubstringFilter(e, Role.EDM_PN_NAME);
		return convertRoles(RoleTM.getInstance().search(name));
	}
	
	public static List<Map<String, Object>> searchGroups(FilterExpression e) {
		String name = extractSubstringFilter(e, Group.EDM_PN_NAME);
		return convertGroups(GroupBO.search(name));
	}

	public static List<Map<String, Object>> searchTemplates(FilterExpression e) {
		String name = extractSubstringFilter(e, Template.EDM_PN_NAME);
		return convertTemplates(TemplateBO.search(name));
	}
	
	public static List<Map<String, Object>> searchUsers(FilterExpression e) {
		String username = extractSubstringFilter(e, User.EDM_PN_USERNAME);
		return convertUsers(UserBO.search(username));
	}
	
	public static List<Map<String, Object>> getRoleGroups(String name) {
		Collection<GroupBO> groups = RoleTM.getInstance().getGroups(name);
		return convertGroups(groups);
	}
	
	public static Map<String, Object> getGroupRole(String name) {
		Role role = GroupBO.get(name).getRole();
		if (role != null) {
			return convertRole(role);
		}
		else {
			return null;
		}
	}
	
	public static Map<String, Object> getLogAuthor(String guid) {
		return convertUser(LogTM.getInstance().getAuthor(guid));
	}

	public static List<Map<String, Object>> getAllBuilds() {
		return convertBuilds(BuildManager.INSTANCE.readAll());
	}
	
	public static Map<String, Object> getBuild(String id) {
		return convertBuild(BuildManager.INSTANCE.get(id));
	}
	
	public static Map<String, Object> getUser(String username) {
		return convertUser(UserBO.get(username));
	}

	public static Map<String, Object> getRole(String name) {
		return convertRole(RoleTM.getInstance().get(name));
	}
	
	public static Map<String, Object> getTemplate(String name) {
		return convertTemplate(TemplateBO.get(name));
	}
	public static Map<String, Object> getGroup(String name) {
		return convertGroup(GroupBO.get(name));
	}
	public static Map<String, Object> getLog(String guid) {
		return convertLog(LogTM.getInstance().get(guid));
	}
	public static Map<String, Object> getSimpleField(String name) {
		return convertSimpleField(SimpleFieldTM.getInstance().get(name));
	}
	
	private static List<Map<String, Object>> convertSimpleFields(Collection<SimpleField> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (SimpleField e : c) {
			result.add(convertSimpleField(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertSimpleField(SimpleField field) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(SimpleField.EDM_PN_NAME, field.getName());
		result.put(SimpleField.EDM_PN_DESC, field.getDescription());
		result.put(SimpleField.EDM_PN_VAL_FUNC, field.getValidationFunction());
		result.put(SimpleField.EDM_PN_VAL_PARAM, field.getValidationParameter());
		return result;
	}
	
	private static List<Map<String, Object>> convertLogs(Collection<Log> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (Log e : c) {
			result.add(convertLog(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertLog(Log l) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Log.EDM_PN_GUID, l.getGuid());
		result.put(Log.EDM_PN_DESC, l.getDescription());
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(l.getDate().getTime());
		result.put(Log.EDM_PN_DATE, c);
		return result;
	}
	
	private static List<Map<String, Object>> convertGroups(Collection<GroupBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (GroupBO e : c) {
			result.add(convertGroup(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertGroup(GroupBO g) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Group.EDM_PN_NAME, g.getName());
		result.put(Group.EDM_PN_DESC, g.getDescription());
		return result;
	}
	
	private static List<Map<String, Object>> convertTemplates(Collection<TemplateBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (TemplateBO e : c) {
			result.add(convertTemplate(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertTemplate(TemplateBO t) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Template.EDM_PN_NAME, t.getName());
		result.put(Template.EDM_PN_DESC, t.getDescription());
		return result;
	}
	
	private static List<Map<String, Object>> convertRoles(Collection<Role> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (Role e : c) {
			result.add(convertRole(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertRole(Role role) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Role.EDM_PN_NAME, role.getName());
		result.put(Role.EDM_PN_DESC, role.getDescription());
		return result;
	}
	
	private static List<Map<String, Object>> convertUsers(Collection<UserBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (UserBO e : c) {
			result.add(convertUser(e));
		}
		return result;
	}

	private static List<Map<String, Object>> convertBuilds(Collection<BuildDocument> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(c.size());
		for (BuildDocument e : c) {
			result.add(convertBuild(e));
		}
		return result;
	}
	
	private static Map<String, Object> convertUser(UserBO user) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(User.EDM_PN_USERNAME, user.getUsername());
		result.put(User.EDM_PN_IS_OWNER, user.getIsOwner());
		return result;
	}
	
	private static Map<String, Object> convertBuild(BuildDocument build) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(BuildDocument.EDM_PN_ID, build.getId());
		result.put(BuildDocument.EDM_PN_DATE, build.getDate());
		result.put(BuildDocument.EDM_PN_AUTHOR, build.getAuthor());
		result.put(BuildDocument.EDM_PN_GROUPS, build.getGroups());
		return result;
	}

	private static String extractSubstringFilter(FilterExpression e, String property) {
		String value;
		try {
			value = (String) e.accept(new SingleMethodVisitor(property,
					MethodOperator.SUBSTRINGOF));
		} catch (Exception e1) {
			value = null;
		}
		if (value == null) {
			return "";
		} else {
			return value;
		}
	}
	
	private static class SingleMethodVisitor implements ExpressionVisitor {
		private String targetField;
		private MethodOperator targetMethod;

		public SingleMethodVisitor(String targetField,
				MethodOperator targetMethod) {
			super();
			this.targetField = targetField;
			this.targetMethod = targetMethod;
		}

		@Override
		public Object visitUnary(UnaryExpression unaryExpression,
				UnaryOperator operator, Object operand) {
			return null;
		}

		@Override
		public Object visitProperty(PropertyExpression propertyExpression,
				String uriLiteral, EdmTyped edmProperty) {
			try {
				if (edmProperty == null
						|| !targetField.equals(edmProperty.getName())) {
					return null;
				} else {
					return targetField;
				}
			} catch (EdmException e) {
				return null;
			}
		}

		@Override
		public Object visitOrderByExpression(
				OrderByExpression orderByExpression, String expressionString,
				List<Object> orders) {
			return null;
		}

		@Override
		public Object visitOrder(OrderExpression orderExpression,
				Object filterResult, SortOrder sortOrder) {
			return null;
		}

		@Override
		public Object visitMethod(MethodExpression methodExpression,
				MethodOperator method, List<Object> parameters) {
			if (method.equals(targetMethod) && parameters.size() > 1
					&& parameters.get(0) != null && parameters.get(1) != null) {
				if (targetField.equals(parameters.get(0))) {
					return parameters.get(1);
				}
				else {
					return parameters.get(0);
				}
			} else {
				return null;
			}
		}

		@Override
		public Object visitMember(MemberExpression memberExpression,
				Object path, Object property) {
			return null;
		}

		@Override
		public Object visitLiteral(LiteralExpression literal,
				EdmLiteral edmLiteral) {
			return edmLiteral.getLiteral();
		}

		@Override
		public Object visitFilterExpression(FilterExpression filterExpression,
				String expressionString, Object expression) {
			return expression;
		}

		@Override
		public Object visitBinary(BinaryExpression binaryExpression,
				BinaryOperator operator, Object leftSide, Object rightSide) {
			return null;
		}


	}

}
