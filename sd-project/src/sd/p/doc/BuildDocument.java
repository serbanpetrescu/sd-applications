package sd.p.doc;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

import sd.p.db.EdmConstants;

@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=BuildDocument.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=BuildDocument.EDM_ESN)
public class BuildDocument {

	public static final String EDM_EN = "BuildDocument";
	public static final String EDM_ESN = "BuildDocuments";
	public static final String EDM_PN_ID = "Id";
	public static final String EDM_PN_DATE = "Date";
	public static final String EDM_PN_AUTHOR = "Author";
	public static final String EDM_PN_GROUPS = "Groups";

	@EdmKey
	@EdmProperty(name=EDM_PN_ID, type=EdmType.STRING)
	private String id;

	@EdmProperty(name=EDM_PN_AUTHOR, type=EdmType.STRING)
	private String author;

	@EdmProperty(name=EDM_PN_DATE, type=EdmType.DATE_TIME)
	private Date date;
	
	@EdmProperty(name=EDM_PN_GROUPS, type=EdmType.STRING)
	private String groups;
	
	public String getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public String getGroups() {
		return groups;
	}
	
	public String getAuthor() {
		return author;
	}


	public BuildDocument() { }
	
	public BuildDocument(String id, String author, GregorianCalendar c, String[] tags) {
		this.id = id;
		this.author = author;
		this.date = new Date(c.getTimeInMillis());
		
		/* no more time to create navigation --> show as list */
		this.groups = "";
		String sep = "";
		for (String tag : tags) {
			this.groups += sep + tag;
			sep = ", ";
		}
	}
	
}
