package sd.a3.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.jaxrs.servlet.CXFNonSpringJaxrsServlet;

import sd.a3.bl.BusinessError;
import sd.a3.bl.BusinessException;
import sd.a3.bl.UserBO;
import sd.a3.db.Cleanup;

/**
 * Wrapper servlet for the OData REST API calls. Authentication checks and
 * sessiom management were added to the default functionality.
 *
 * @author Serban Petrescu
 *
 */
@WebServlet(name = "ODataService", displayName = "ODataService", urlPatterns = { "/data.svc/*" }, loadOnStartup = 1, initParams = {
		@WebInitParam(name = "javax.ws.rs.Application", value = "org.apache.olingo.odata2.core.rest.app.ODataApplication"),
		@WebInitParam(name = "org.apache.olingo.odata2.service.factory", value = "sd.a3.com.ODataFactory") })
public class ODataServlet extends CXFNonSpringJaxrsServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			getAuthenticatedUser(request);
			super.doGet(request, response);
		} catch (BusinessException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			getAuthenticatedUser(request);
			super.doPost(request, response);
		} catch (BusinessException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			super.doDelete(request, response);
		} catch (BusinessException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			super.doPut(request, response);
		} catch (BusinessException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	/**
	 * Writes an {@link BusinessException} as the response of a request.
	 *
	 * @param e
	 * @param response
	 */
	private void sendErrorResponse(BusinessException e,
			HttpServletResponse response) {
		try {
			response.setStatus(403);
			response.setContentType("application/json");
			response.getWriter().print(e.getMessage());
		} catch (IOException ex) {
			// nothing left to do...
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	/**
	 * PErforms authentication or session restore.
	 *
	 * @param request
	 */
	private void getAuthenticatedUser(HttpServletRequest request) {
		String username = (String) request.getSession().getAttribute(
				SessionServlet.SESSION_USER);
		if (username != null) {
			UserBO.setLoggedInUser(username);
		} else {
			throw new BusinessException(BusinessError.USER_NOT_AUTH);
		}
	}

}
