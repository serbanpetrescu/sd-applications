package sd.a3.db;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The DB entity class for patients.
 * @author Serban Petrescu
 *
 */
@Entity
@Table(name="T_PATIENTS")
@NamedQueries({
    @NamedQuery(name="Patient.findByName",
    			query="SELECT p FROM Patient p WHERE LOWER( p.name ) LIKE :value"),
})
public class Patient implements Serializable, PersistentEntity<String> {
	private static final long serialVersionUID = 1L;

	@Id
	private String personalNo;
	private String name;
	private String idCardNo;
	private String address;
	private Date birthday;

	@OneToMany(mappedBy="patient", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<Consultation> consultations;

	public String getPersonalNo() {
		return personalNo;
	}

	public void setPersonalNo(String personalNo) {
		this.personalNo = personalNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Collection<Consultation> getConsultations() {
		return consultations;
	}

	public void setConsultations(Collection<Consultation> consultations) {
		this.consultations = consultations;
	}

	@Override
	public String getKey() {
		return this.personalNo;
	}

	public Patient() {

	}

	public Patient(String personalNo, String name, String idCardNo,
			String address, Date birthday) {
		super();
		this.personalNo = personalNo;
		this.name = name;
		this.idCardNo = idCardNo;
		this.address = address;
		this.birthday = birthday;
		this.consultations = new LinkedList<Consultation>();
	}



}