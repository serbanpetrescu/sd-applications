package sd.p.db;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: SimpleField
 *
 */
@Entity
@Table(name="T_SIMPLE_FIELD")
@NamedQueries({
    @NamedQuery(name="SimpleField.readAll",
    			query="SELECT f FROM SimpleField f"),
    @NamedQuery(name="SimpleField.searchByName",
    			query="SELECT f FROM SimpleField f WHERE LOWER( f.name ) LIKE :value"),
}) 
@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=SimpleField.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=SimpleField.EDM_ESN)
public class SimpleField implements Serializable, PersistentEntity<String> {

	public static final String EDM_EN = "SimpleField";
	public static final String EDM_ESN = "SimpleFields";
	public static final String EDM_PN_NAME = "Name";
	public static final String EDM_PN_DESC = "Description";
	public static final String EDM_PN_VAL_FUNC = "ValidationFunction";
	public static final String EDM_PN_VAL_PARAM = "ValidationParameter";
	
	@Id
	@EdmKey
	@EdmProperty(name=EDM_PN_NAME, type=EdmType.STRING)
	private String name;

	@EdmProperty(name=EDM_PN_VAL_FUNC, type=EdmType.STRING)
	private String validationFunction;

	@EdmProperty(name=EDM_PN_VAL_PARAM, type=EdmType.STRING)
	private String validationParameter;

	@EdmProperty(name=EDM_PN_DESC, type=EdmType.STRING)
	private String description;
	
	private static final long serialVersionUID = 1L;

	public SimpleField() {
		super();
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getValidationFunction() {
		return this.validationFunction;
	}

	public void setValidationFunction(String validationFunction) {
		this.validationFunction = validationFunction;
	}   
	public String getValidationParameter() {
		return this.validationParameter;
	}

	public void setValidationParameter(String validationParameter) {
		this.validationParameter = validationParameter;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String getKey() {
		return this.name;
	}
	
	public SimpleField(String name, String validationFunction,
			String validationParameter, String description) {
		this.name = name;
		this.validationFunction = validationFunction;
		this.validationParameter = validationParameter;
		this.description = description;
	}
	
	
   
}
