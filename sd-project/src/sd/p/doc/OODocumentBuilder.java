package sd.p.doc;

import java.util.Map;

import sd.p.db.SimpleField;

public interface OODocumentBuilder {
	void build(byte[] file, String destFolder, String destFile);
	void setFields(Map<SimpleField, String> simpleFields);
}
