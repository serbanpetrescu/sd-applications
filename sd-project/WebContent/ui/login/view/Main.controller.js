sap.ui.controller("sd.p.ui.login.view.Main", {

    onInit : function() {
        if (sap.ui.Device.support.touch === false) {
            this.getView().addStyleClass("sapUiSizeCompact");
        }
        this.getView().addStyleClass("loginBackground");
    },

    onLogin: function() {
    	//sap.ui.getCore().getConfiguration().setLanguage(this.byId("language").getSelectedKey());
    	jQuery.ajax({
			type : "POST",
			url : "session",
			data : { action : "login", username: this.byId("username").getValue(), password: this.byId("password").getValue() },
			success: this.onSuccess,
			error: showError,
			dataType : "json"
		});
    },

    onSuccess: function(oData) {
    	if (oData) {
    		session.loaded = true;
    		session.role = oData.role;
    		navigate();
    	}
    	else {
    		showError();
    	}
    }

});
