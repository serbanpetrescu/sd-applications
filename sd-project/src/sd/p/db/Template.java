package sd.p.db;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: Template
 *
 */
@Entity
@Table(name="T_TEMPLATE")
@NamedQueries({
    @NamedQuery(name="Template.readAll",
    			query="SELECT f FROM Template f"),
    @NamedQuery(name="Template.searchByName",
    			query="SELECT f FROM Template f WHERE LOWER( f.name ) LIKE :value"),
}) 
@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=Template.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=Template.EDM_ESN)
public class Template implements Serializable, PersistentEntity<String> {

	public static final String EDM_EN = "Template";
	public static final String EDM_ESN = "Templates";
	public static final String EDM_PN_NAME = "Name";
	public static final String EDM_PN_DESC = "Description";
	
	@Id
	@EdmKey
	@EdmProperty(name=EDM_PN_NAME, type=EdmType.STRING)
	private String name;
	
	@Column(nullable = true)
	private String document;
	
	@EdmProperty(name=EDM_PN_DESC, type=EdmType.STRING)
	private String description;
	
	private static final long serialVersionUID = 1L;

	public Template() {
		super();
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDocument() {
		return this.document;
	}

	public void setDocument(String document) {
		this.document = document;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String getKey() {
		return this.name;
	}
	
	public Template(String name, String document, String description) {
		super();
		this.name = name;
		this.document = document;
		this.description = description;
	}
   
	
}
