package sd.p.com;

import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.olingo.odata2.api.commons.HttpStatusCodes;
import org.apache.olingo.odata2.api.commons.InlineCount;
import org.apache.olingo.odata2.api.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties.ODataEntityProviderPropertiesBuilder;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.exception.ODataNotFoundException;
import org.apache.olingo.odata2.api.exception.ODataNotImplementedException;
import org.apache.olingo.odata2.api.processor.ODataResponse;
import org.apache.olingo.odata2.api.processor.ODataSingleProcessor;
import org.apache.olingo.odata2.api.uri.info.DeleteUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetEntitySetUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetEntityUriInfo;
import org.apache.olingo.odata2.api.uri.info.PostUriInfo;
import org.apache.olingo.odata2.api.uri.info.PutMergePatchUriInfo;

import sd.p.bl.GroupBO;
import sd.p.bl.RoleTM;
import sd.p.bl.SimpleFieldTM;
import sd.p.bl.TemplateBO;
import sd.p.bl.UserBO;
import sd.p.db.Group;
import sd.p.db.Log;
import sd.p.db.Role;
import sd.p.db.SimpleField;
import sd.p.db.Template;
import sd.p.db.User;
import sd.p.doc.BuildDocument;
import sd.p.err.GeneralException;

public class ODataSProcessor extends ODataSingleProcessor {

	public ODataResponse readEntitySet(GetEntitySetUriInfo uriInfo,
			String contentType) throws ODataException {
		try {
			EdmEntitySet entitySet;
			ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
					.serviceRoot(getContext().getPathInfo().getServiceRoot());
			List<Map<String, Object>> result = null;

			if (uriInfo.getNavigationSegments().size() == 0) {
				entitySet = uriInfo.getStartEntitySet();

				switch (entitySet.getName()) {
				case User.EDM_ESN:
					result = ODataToolbox.searchUsers(uriInfo.getFilter());
					break;
				case Role.EDM_ESN:
					result = ODataToolbox.searchRoles(uriInfo.getFilter());
					break;
				case Template.EDM_ESN:
					result = ODataToolbox.searchTemplates(uriInfo.getFilter());
					break;
				case Group.EDM_ESN:
					result = ODataToolbox.searchGroups(uriInfo.getFilter());
					break;
				case SimpleField.EDM_ESN:
					result = ODataToolbox.searchSimpleFields(uriInfo
							.getFilter());
					break;
				case BuildDocument.EDM_ESN:
					result = ODataToolbox.getAllBuilds();
					break;
				}

				if (result != null) {
					result = applySetParameters(result, propertiesBuilder,
							uriInfo);
					return EntityProvider.writeFeed(contentType, entitySet,
							result, propertiesBuilder.build());
				} else {
					throw new ODataNotFoundException(
							ODataNotFoundException.ENTITY);
				}

			} else if (uriInfo.getNavigationSegments().size() == 1) {
				entitySet = uriInfo.getTargetEntitySet();
				String startSetName = uriInfo.getStartEntitySet().getName();
				String key = uriInfo.getKeyPredicates().get(0).getLiteral();

				switch (entitySet.getName()) {
				case Template.EDM_ESN:
					if (Group.EDM_ESN.equals(startSetName)) {
						result = ODataToolbox.getGroupTemplates(key);
					}
					break;
				case Group.EDM_ESN:
					if (Role.EDM_ESN.equals(startSetName)) {
						result = ODataToolbox.getRoleGroups(key);
					}
					break;
				case Role.EDM_ESN:
					if (User.EDM_ESN.equals(startSetName)) {
						result = ODataToolbox.getUserRoles(key);
					}
					break;
				case Log.EDM_ESN:
					if (User.EDM_ESN.equals(startSetName)) {
						result = ODataToolbox.getUserLogs(key);
					}
					break;
				}

				if (result != null) {
					result = applySetParameters(result, propertiesBuilder,
							uriInfo);
					return EntityProvider.writeFeed(contentType, entitySet,
							result, propertiesBuilder.build());
				} else {
					throw new ODataNotFoundException(
							ODataNotFoundException.ENTITY);
				}
			}

			throw new ODataNotImplementedException();
		} catch (GeneralException e) {
			throw new ODataException(e.getMessage());
		}
	}

	@Override
	public ODataResponse updateEntityLink(PutMergePatchUriInfo uriInfo,
			InputStream content, String requestContentType, String contentType)
			throws ODataException {

		EntityProviderReadProperties properties = EntityProviderReadProperties
				.init().mergeSemantic(false).build();
		ODataEntry entry = EntityProvider.readEntry(requestContentType,
				uriInfo.getTargetEntitySet(), content, properties);
		String key = uriInfo.getKeyPredicates().get(0).getLiteral();
		Map<String, Object> data = entry.getProperties();
		if (Role.EDM_ESN.equals(uriInfo.getTargetEntitySet().getName())
				&& Group.EDM_ESN.equals(uriInfo.getStartEntitySet().getName())) {
			String role = (String) data.get(Role.EDM_PN_NAME);
			if (role != null && role.length() == 0) {
				role = null;
			}
			if (role == null) {
				GroupBO.get(key).setRole(null);
			} else {
				GroupBO.get(key).setRole(RoleTM.getInstance().get(role));
			}
			return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
		} else {
			throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
		}
	}

	@Override
	public ODataResponse createEntityLink(PostUriInfo uriInfo,
			InputStream content, String requestContentType, String contentType)
			throws ODataException {

		EntityProviderReadProperties properties = EntityProviderReadProperties
				.init().mergeSemantic(false).build();
		ODataEntry entry = EntityProvider.readEntry(requestContentType,
				uriInfo.getTargetEntitySet(), content, properties);
		String key = uriInfo.getKeyPredicates().get(0).getLiteral();

		Map<String, Object> data = entry.getProperties();
		switch (uriInfo.getTargetEntitySet().getName()) {
		case Template.EDM_ESN:
			if (Group.EDM_ESN.equals(uriInfo.getStartEntitySet().getName())) {
				GroupBO.get(key)
						.addTemplate(
								TemplateBO.get((String) data
										.get(Template.EDM_PN_NAME)));
				return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
			}
			break;
		case Role.EDM_ESN:
			if (User.EDM_ESN.equals(uriInfo.getStartEntitySet().getName())) {
				UserBO.get(key).addRole(
						RoleTM.getInstance().get(
								(String) data.get(Role.EDM_PN_NAME)));
				return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
			}
			break;
		}
		throw new ODataNotFoundException(ODataNotFoundException.ENTITY);

	}

	public ODataResponse readEntity(GetEntityUriInfo uriInfo, String contentType)
			throws ODataException {
		try {
			int depth = uriInfo.getNavigationSegments().size();
			URI serviceRoot = getContext().getPathInfo().getServiceRoot();
			ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
					.serviceRoot(serviceRoot);

			String key = uriInfo.getKeyPredicates().get(0).getLiteral();

			Map<String, Object> data = null;

			if (depth == 0) {
				EdmEntitySet entitySet = uriInfo.getStartEntitySet();

				switch (entitySet.getName()) {
				case User.EDM_ESN:
					data = ODataToolbox.getUser(key);
					break;
				case Role.EDM_ESN:
					data = ODataToolbox.getRole(key);
					break;
				case Template.EDM_ESN:
					data = ODataToolbox.getTemplate(key);
					break;
				case Log.EDM_ESN:
					data = ODataToolbox.getLog(key);
					break;
				case Group.EDM_ESN:
					data = ODataToolbox.getGroup(key);
					break;
				case SimpleField.EDM_ESN:
					data = ODataToolbox.getSimpleField(key);
					break;
				case BuildDocument.EDM_ESN:
					data = ODataToolbox.getBuild(key);
					break;
				}

				if (data != null) {
					return EntityProvider.writeEntry(contentType, entitySet,
							data, propertiesBuilder.build());
				} else {
					throw new ODataNotFoundException(
							ODataNotFoundException.ENTITY);
				}

			} else if (depth == 1) {
				EdmEntitySet targetSet = uriInfo.getTargetEntitySet();
				String startSetName = uriInfo.getStartEntitySet().getName();

				switch (targetSet.getName()) {
				case User.EDM_ESN:
					if (Log.EDM_ESN.equals(startSetName)) {
						data = ODataToolbox.getLogAuthor(key);
					}
					break;
				case Role.EDM_ESN:
					if (Group.EDM_ESN.equals(startSetName)) {
						data = ODataToolbox.getGroupRole(key);
					}
				}

				if (data != null) {
					return EntityProvider.writeEntry(contentType, targetSet,
							data, propertiesBuilder.build());
				} else {
					throw new ODataNotFoundException(
							ODataNotFoundException.ENTITY);
				}
			}

			throw new ODataNotImplementedException();
		} catch (GeneralException e) {
			throw new ODataException(e.getMessage());
		}
	}

	public ODataResponse updateEntity(PutMergePatchUriInfo uriInfo,
			InputStream content, String requestContentType, boolean merge,
			String contentType) throws ODataException {
		try {
			EntityProviderReadProperties properties = EntityProviderReadProperties
					.init().mergeSemantic(false).build();

			ODataEntry entry = EntityProvider.readEntry(requestContentType,
					uriInfo.getTargetEntitySet(), content, properties);
			Map<String, Object> data = entry.getProperties();
			String key = uriInfo.getKeyPredicates().get(0).getLiteral();

			switch (uriInfo.getTargetEntitySet().getName()) {
			case User.EDM_ESN:
				ODataToolbox.updateUser(key, data);
				break;
			case Template.EDM_ESN:
				ODataToolbox.updateTemplate(key, data);
				break;
			case Role.EDM_ESN:
				ODataToolbox.updateRole(key, data);
				break;
			case Group.EDM_ESN:
				ODataToolbox.updateGroup(key, data);
				break;
			case SimpleField.EDM_ESN:
				ODataToolbox.updateSimpleField(key, data);
				break;
			}

			return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
		} catch (GeneralException e) {
			throw new ODataException(e.getMessage());
		}
	}

	@Override
	public ODataResponse createEntity(PostUriInfo uriInfo, InputStream content,
			String requestContentType, String contentType)
			throws ODataException {
		try {
			if (uriInfo.getStartEntitySet().getEntityType().hasStream()) {
				throw new ODataNotImplementedException();
			}

			EntityProviderReadProperties properties = EntityProviderReadProperties
					.init().mergeSemantic(false).build();

			if (uriInfo.getNavigationSegments().size() > 0) {
				throw new ODataNotImplementedException();
			} else {

				ODataEntry entry = EntityProvider.readEntry(requestContentType,
						uriInfo.getStartEntitySet(), content, properties);

				Map<String, Object> data = entry.getProperties();

				switch (uriInfo.getTargetEntitySet().getName()) {
				case User.EDM_ESN:
					data = ODataToolbox.createUser(data);
					break;
				case Template.EDM_ESN:
					ODataToolbox.createTemplate(data);
					break;
				case Role.EDM_ESN:
					ODataToolbox.createRole(data);
					break;
				case Group.EDM_ESN:
					ODataToolbox.createGroup(data);
					break;
				case SimpleField.EDM_ESN:
					ODataToolbox.createSimpleField(data);
					break;
				}
				return EntityProvider.writeEntry(
						contentType,
						uriInfo.getStartEntitySet(),
						data,
						EntityProviderWriteProperties.serviceRoot(
								getContext().getPathInfo().getServiceRoot())
								.build());
			}
		} catch (GeneralException e) {
			throw new ODataException(e.getMessage());
		}
	}

	@Override
	public ODataResponse deleteEntityLink(DeleteUriInfo uriInfo,
			String contentType) throws ODataException {
		String key = uriInfo.getKeyPredicates().get(0).getLiteral();
		String child = null;
		if (uriInfo.getNavigationSegments().size() > 0) {
			if (uriInfo.getNavigationSegments().get(0).getKeyPredicates()
					.size() > 0) {
				child = uriInfo.getNavigationSegments().get(0)
						.getKeyPredicates().get(0).getLiteral();
			}
		}

		switch (uriInfo.getTargetEntitySet().getName()) {
		case Template.EDM_ESN:
			if (Group.EDM_ESN.equals(uriInfo.getStartEntitySet().getName())) {
				GroupBO.get(key).removeTemplate(TemplateBO.get(child));
				return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
			}
			break;
		case Role.EDM_ESN:
			if (User.EDM_ESN.equals(uriInfo.getStartEntitySet()
					.getName())) {
				UserBO.get(key).removeRole(RoleTM.getInstance().get(child));
				return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
			}
			break;
		}
		throw new ODataNotImplementedException();
	}

	public ODataResponse deleteEntity(DeleteUriInfo uriInfo, String contentType)
			throws ODataException {
		try {
			String key = uriInfo.getKeyPredicates().get(0).getLiteral();
			if (uriInfo.getNavigationSegments().size() > 0) {
				throw new ODataNotImplementedException();
			} else {
				switch (uriInfo.getStartEntitySet().getName()) {
				case User.EDM_ESN:
					UserBO.get(key).delete();
					break;
				case Template.EDM_ESN:
					TemplateBO.get(key).delete();
					break;
				case Role.EDM_ESN:
					RoleTM.getInstance().delete(key);
					break;
				case Group.EDM_ESN:
					GroupBO.get(key).delete();
					break;
				case SimpleField.EDM_ESN:
					SimpleFieldTM.getInstance().delete(key);
					break;
				}
				return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
			}
		} catch (GeneralException e) {
			throw new ODataException(e.getMessage());
		}
	}

	private List<Map<String, Object>> applySetParameters(
			List<Map<String, Object>> result,
			ODataEntityProviderPropertiesBuilder properties,
			GetEntitySetUriInfo uriInfo) {
		if (InlineCount.ALLPAGES.equals(uriInfo.getInlineCount())) {
			properties.inlineCountType(InlineCount.ALLPAGES).inlineCount(
					result.size());
		}
		if (uriInfo.getSkip() == null && uriInfo.getTop() == null) {
			return result;
		} else {
			int top = (uriInfo.getTop() == null) ? result.size() : uriInfo
					.getTop();
			int skip = (uriInfo.getSkip() == null) ? 0 : uriInfo.getSkip();
			if (skip > result.size()) {
				skip = result.size();
			}
			if (skip + top > result.size()) {
				top = result.size() - skip;
			}
			return result.subList(skip, skip + top);
		}
	}
}
