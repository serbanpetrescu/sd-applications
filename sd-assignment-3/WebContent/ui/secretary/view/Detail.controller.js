sap.ui.core.mvc.Controller.extend("sd.a3.ui.secretary.view.Detail", {

	onInit : function() {
		this.oInitialLoadFinishedDeferred = jQuery.Deferred();

		if(sap.ui.Device.system.phone) {
			//Do not wait for the master when in mobile phone resolution
			this.oInitialLoadFinishedDeferred.resolve();
		} else {
			this.getView().setBusy(true);
			var oEventBus = this.getEventBus();
			oEventBus.subscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
			oEventBus.subscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		}

		this.getRouter().attachRouteMatched(this.onRouteMatched, this);
	},

	logoff: function() {
		session.logoff();
	},
 
    onSelect: function (oEvent) {
        this.byId("consultEditBtn").setEnabled(oEvent.getParameter("selected"));
        this.byId("consultDeleteBtn").setEnabled(oEvent.getParameter("selected"));
    },

    formatDate: function(d) {
        if (d) {
    	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            var date = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: i18n.getText("dateFormat")});
            return date.format(d);
        }
    },

    formatPeriod: function(start, end) {
	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
        var time = sap.ui.core.format.DateFormat.getTimeInstance({pattern: i18n.getText("timeFormat")});
        var date = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: i18n.getText("dateFormat")});
        return  i18n.getText("periodFormat", [date.format(start), time.format(start), time.format(end)]);
    },

	onMasterLoaded :  function () {
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
	},

	onMetadataFailed : function(){
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
        this.showEmptyView();
	},

	onRouteMatched : function(oEvent) {
		var oParameters = oEvent.getParameters();

		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(function () {
			var oView = this.getView();

			// When navigating in the Detail page, update the binding context
			if (oParameters.name !== "detail") {
				return;
			}

			var sEntityPath = "/" + oParameters.arguments.entity;
			this.bindView(sEntityPath);

			var oIconTabBar = oView.byId("idIconTabBar");
			oIconTabBar.getItems().forEach(function(oItem) {
			    if(oItem.getKey() !== "selfInfo"){
    				oItem.bindElement(oItem.getKey());
			    }
			});

			// Specify the tab being focused
			var sTabKey = oParameters.arguments.tab;
			this.getEventBus().publish("Detail", "TabChanged", { sTabKey : sTabKey });

			if (oIconTabBar.getSelectedKey() !== sTabKey) {
				oIconTabBar.setSelectedKey(sTabKey);
			}
		}, this));

	},

	bindView : function (sEntityPath) {
		var oView = this.getView();
		oView.bindElement(sEntityPath);

		//Check if the data is already on the client
		if(!oView.getModel().getData(sEntityPath)) {

			// Check that the entity specified was found.
			oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
				var oData = oView.getModel().getData(sEntityPath);
				if (!oData) {
					this.showEmptyView();
					this.fireDetailNotFound();
				} else {
					this.fireDetailChanged(sEntityPath);
				}
			}, this));

		} else {
			this.fireDetailChanged(sEntityPath);
		}

	},

	showEmptyView : function () {
		this.getRouter().myNavToWithoutHash({
			currentView : this.getView(),
			targetViewName : "sd.a3.ui.secretary.view.NotFound",
			targetViewType : "XML"
		});
	},

	fireDetailChanged : function (sEntityPath) {
		this.getEventBus().publish("Detail", "Changed", { sEntityPath : sEntityPath });
	},

	fireDetailNotFound : function () {
		this.getEventBus().publish("Detail", "NotFound");
	},

	onNavBack : function() {
		// This is only relevant when running on phone devices
		this.getRouter().myNavBack("main");
	},

	onDetailSelect : function(oEvent) {
		sap.ui.core.UIComponent.getRouterFor(this).navTo("detail",{
			entity : oEvent.getSource().getBindingContext().getPath().slice(1),
			tab: oEvent.getParameter("selectedKey")
		}, true);
	},

	getEventBus : function () {
		return sap.ui.getCore().getEventBus();
	},

	getRouter : function () {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},

	onExit : function(){
	    var oEventBus = this.getEventBus();
    	oEventBus.unsubscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		oEventBus.unsubscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
	},

    editPatient: function() {
        if (!this.patientDialog) {
            this.patientDialog = sap.ui.xmlfragment(this.getView().getId(), "sd.a3.ui.secretary.view.PatientDialog", this);
            this.getView().addDependent(this.patientDialog);
	        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            this.patientDialog.setTitle(i18n.getText("editPatientTitle"));
        }

        this.patientDialog.bindElement(this.getView().getBindingContext().getPath());
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.patientDialog);
        this.patientDialog.open();
    },

    deletePatient: function() {
        jQuery.sap.require("sap.ca.ui.dialog.factory");
	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
	    var context = this.getView().getBindingContext();
        sap.ca.ui.dialog.confirmation.open({
            question : i18n.getText("confirmDeletePatientQuestion", context.getProperty("Name")),
            showNote : false,
            title : i18n.getText("confirmDeletePatientTitle"),
            confirmButtonLabel: i18n.getText("confirmDeleteLabel")
        }, jQuery.proxy(function(confirmed) {
        	if (confirmed && confirmed.isConfirmed){
	            this.getOwnerComponent().getModel().remove(context.getPath(), {
	            	success: jQuery.proxy(function() {
	        		this.getView().unbindElement();
	            	this.showEmptyView();
	        	}, this),});
        	}
        }, this));
    },

    notifyDoctor: function() {
    	this.getOwnerComponent().getModel().callFunction("NotifyDoctor", {
    		urlParameters: {personalNo: this.getView().getBindingContext().getProperty("PersonalNo")},
    		method: "GET",
    		error: showError,
    		success: jQuery.proxy(function(){this.byId("notifyBtn").setEnabled(true);}, this)
    	});

    },

    onPatientAcceptButton: function() {
	    var context = this.getView().getBindingContext();
        this.getOwnerComponent().getModel().update(context.getPath(), {
            Name: this.byId("patientName").getValue(),
            Address: this.byId("patientAddress").getValue(),
            IdCardNo: this.byId("patientIdCardNo").getValue(),
            Birthday: this.byId("patientBirthday").getDateValue()
        }, {error: showError, success: jQuery.proxy(function(){this.patientDialog.close();}, this)});
    },

    onPatientCloseButton: function() {
        this.patientDialog.close();
    },

    createConsDialog: function() {
        this.consDialog = sap.ui.xmlfragment(this.getView().getId(), "sd.a3.ui.secretary.view.ConsultationDialog", this);
        this.getView().addDependent(this.consDialog);
        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
        this.consDialog.setTitle(i18n.getText("addConsultationTitle"));
        //this.byId("consDoctor").bindItems("/Doctors");
    },

    onConsultationAdd: function() {
        if (!this.consDialog) {
            this.createConsDialog();
        }

        this.byId("consDoctor").setEnabled(true);
        this.consType = 0;
        this.consDialog.unbindElement();
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.dialog);
        this.consDialog.open();
    },

    onConsultationEdit: function() {
        if (!this.consDialog) {
            this.createConsDialog();
        }

        this.byId("consDoctor").setEnabled(false);
        this.consType = 1;
        this.consDialog.bindElement(this.getSelectedConsultPath());
        jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.consDialog);
        this.consDialog.open();
    },

    getSelectedConsultPath: function() {
        var consultation = this.byId("consultationList").getSelectedItem().getBindingContext().getProperty("UUID");
	    return "/Consultations('" + consultation + "')";
    },

    onConsultationDelete: function() {
        jQuery.sap.require("sap.ca.ui.dialog.factory");
	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
        sap.ca.ui.dialog.confirmation.open({
            question : i18n.getText("confirmDeleteConsQuestion"),
            showNote : false,
            title : i18n.getText("confirmDeleteConstTitle"),
            confirmButtonLabel: i18n.getText("confirmDeleteLabel")
        }, jQuery.proxy(function(confirmed) {
            if (confirmed && confirmed.isConfirmed){this.getOwnerComponent().getModel().remove(this.getSelectedConsultPath(), {error: showError});}
        }, this));
    },

    
    extractDateTime: function(date, time) {
    	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes());
    },
    
    onConsultationAcceptButton: function() {
	    if (this.consType === 0) {
            this.getOwnerComponent().getModel().create("Consultations/", {
                StartTime: this.extractDateTime(this.byId("consDate").getDateValue(),this.byId("consStart").getDateValue()),
                EndTime: this.extractDateTime(this.byId("consDate").getDateValue(),this.byId("consEnd").getDateValue()),
                Doctor: this.byId("consDoctor").getSelectedKey(),
                Patient: this.getView().getBindingContext().getProperty("PersonalNo")
            }, {error: showError, success: jQuery.proxy(function(){this.consDialog.close();}, this)});
	    }
	    else {
	        this.getOwnerComponent().getModel().update(this.getSelectedConsultPath(), {
                StartTime: this.extractDateTime(this.byId("consDate").getDateValue(),this.byId("consStart").getDateValue()),
                EndTime: this.extractDateTime(this.byId("consDate").getDateValue(),this.byId("consEnd").getDateValue()),
            }, {error: showError, success: jQuery.proxy(function(){this.consDialog.close();}, this)});
	    }

    },

    onConsultationCloseButton: function() {
        this.consDialog.close();
    }

});
