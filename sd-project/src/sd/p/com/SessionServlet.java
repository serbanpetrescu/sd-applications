package sd.p.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sd.p.bl.UserBO;
import sd.p.db.Cleanup;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

@WebServlet(name="SessionManager", displayName="SessionManager", urlPatterns={"/session"})
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String SESSION_USER = "username";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_PASSWORD = "password";
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_LOGOUT = "logout";
	public static final String ACTION_CHECK = "check";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			String username = (String) req.getSession().getAttribute(
					SESSION_USER);
			if (username != null) {
				UserBO.setLoggedInUser(username);
			}
			String action = req.getParameter(PARAM_ACTION);
			if (ACTION_LOGIN.equalsIgnoreCase(action)) {
				username = req.getParameter(PARAM_USERNAME);
				String password = req.getParameter(PARAM_PASSWORD);
				if (username != null && password != null) {
					UserBO.setLoggedInUser(username, password);
					req.getSession().setAttribute(SESSION_USER, username);
					resp.setStatus(200);
					resp.getWriter().println("{\"role\": " + UserBO.getLoggedInUser().getIsOwner() + "}");
					resp.getWriter().close();
				} else {
					throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
				}
			} else if (ACTION_LOGOUT.equalsIgnoreCase(action)) {
				req.getSession().removeAttribute(SESSION_USER);
				resp.setStatus(204);
			} else if (ACTION_CHECK.equalsIgnoreCase(action)) {
				try {
					boolean owner = UserBO.getLoggedInUser().getIsOwner();
					resp.setStatus(200);
					resp.getWriter().println("{\"role\": " + owner + "}");
					resp.getWriter().close();
				}
				catch (GeneralException e) {
					resp.setStatus(204);
				}
			} else {
				throw new GeneralException(GeneralError.INCOMPLETE_REQUEST);
			}
		} catch (GeneralException e) {
			resp.reset();
			resp.setStatus(403);
			resp.setContentType("application/json");
			resp.getWriter().print(e.getMessage());
			resp.getWriter().close();
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

}
