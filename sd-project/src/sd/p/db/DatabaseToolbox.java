package sd.p.db;

import java.util.Collection;

import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;

public class DatabaseToolbox {

	public static void fixUserDirectory(String uname, String folder) {
		User u = find(User.class, uname);
		if (u != null) {
			begin();
			u.setDirectory(folder);
			commit();
		}
	}
	
	public static void begin(){
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
	}

	public static void commit() {
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	public static void persist(Object o) {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
		EmfFactory.INSTANCE.getEm().persist(o);
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	public static void remove(Object o/* , Class<?> clazz */) {
		EmfFactory.INSTANCE.getEm().getTransaction().begin();
		EmfFactory.INSTANCE.getEm().remove(o);
		EmfFactory.INSTANCE.getEm().getTransaction().commit();
	}

	public static boolean checkExistence(Class<?> clazz, Object o){
		try {
			return EmfFactory.INSTANCE.getEm().getReference(clazz, o) != null;
		} catch (EntityNotFoundException e) {
			return false;
		}
	}

	public static <K, T extends PersistentEntity<K>> T find(Class<T> clazz,
			K key) {
		return EmfFactory.INSTANCE.getEm().find(clazz, key);
	}
	
	public static Collection<Group> getGroupsByUser(User u) {
		TypedQuery<Group> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"User.getGroups", Group.class);
		q.setParameter(":username", u.getUsername());
		Collection<Group> g = q.getResultList();
		q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"Group.readPublic", Group.class);
		g.addAll(q.getResultList());
		return g;
	}
	
	
	public static Collection<Template> getTemplatesByUser(User u) {
		TypedQuery<Template> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
				"User.getTemplates", Template.class);
		q.setParameter(":username", u.getUsername());
		return q.getResultList();
	}
	
	public static Collection<Template> searchTemplates(String name) {
		return searchByString(Template.class, "Template.readAll", "Template.searchByName", name);
	}
	
	public static Collection<Role> searchRoles(String name) {
		return searchByString(Role.class, "Role.readAll", "Role.searchByName", name);
	}
	
	public static Collection<Group> searchGroups(String name) {
		return searchByString(Group.class, "Group.readAll", "Group.searchByName", name);
	}
	
	public static Collection<SimpleField> searchSimpleFields(String name) {
		return searchByString(SimpleField.class, "SimpleField.readAll", "SimpleField.searchByName", name);
	}

	public static Collection<User> searchUsers(String username) {
		return searchByString(User.class, "User.readAll", "User.searchByUsername", username);
	}

	private static <T> Collection<T> searchByString(Class<T> clazz,
			String readAll, String search, String param) {
		if (param == null || param.length() == 0) {
			TypedQuery<T> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
					readAll, clazz);
			return q.getResultList();
		} else {
			TypedQuery<T> q = EmfFactory.INSTANCE.getEm().createNamedQuery(
					search, clazz);
			q.setParameter("value", "%"+param.toLowerCase() + "%");
			return q.getResultList();
		}
	}
}
