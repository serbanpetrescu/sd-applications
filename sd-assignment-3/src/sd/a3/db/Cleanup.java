package sd.a3.db;

import java.util.LinkedList;

/**
 * Singleton class used to cleanup resources. It has a list of cleaners which
 * should implement the cleanup logic for their parent module.
 *
 * @author Serban Petrescu
 *
 */
public enum Cleanup {
	INSTANCE;

	private Cleanup() {
		cleaners = new LinkedList<Wrapper>();
	}

	public void registerCleaner(Wrapper cleaner) {
		cleaners.add(cleaner);
	}

	public void doCleanup() {
		for (Wrapper c : cleaners) {
			c.doCleanup();
		}
	}

	private LinkedList<Wrapper> cleaners;

	/**
	 * Functional interface for the cleaners.
	 *
	 * @author Serban Petrescu
	 *
	 */
	public static interface Wrapper {
		void doCleanup();
	}

}
