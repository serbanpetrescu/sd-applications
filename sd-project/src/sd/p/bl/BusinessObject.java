package sd.p.bl;

import sd.p.db.DatabaseToolbox;
import sd.p.db.PersistentEntity;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public abstract class BusinessObject<DB extends PersistentEntity<K>, K> {

	protected DB dbObject;
	protected K dbKey;
	private Class<DB> dbClass;

	protected BusinessObject(Class<DB> dbClass, DB dbObject, boolean createDB){
		this.dbClass = dbClass;
		this.dbObject = dbObject;
		this.dbKey = dbObject.getKey();
		if (createDB){
			if (DatabaseToolbox.checkExistence(dbClass, dbObject.getKey())) {
				throw new GeneralException(GeneralError.BO_ALREADY_EXISTS, dbClass.getSimpleName().toLowerCase());
			}
			DatabaseToolbox.persist(dbObject);
		}
	}

	protected BusinessObject(Class<DB> dbClass, K dbKey){
		this.dbClass = dbClass;
		this.dbKey = dbKey;
			this.dbObject = DatabaseToolbox.find(this.dbClass, dbKey);
			if (this.dbObject == null) {
				throw new GeneralException(GeneralError.BO_NOT_FOUND, dbClass.getSimpleName().toLowerCase());
			}
	}

	protected void deleteBase() {
		DatabaseToolbox.remove(dbObject);
	}

	public DB getDbObject() {
		return dbObject;
	}

	public K getDbKey() {
		return dbKey;
	}
	
	protected static void validateName(String name) {
		if (!name.matches("^[a-zA-Z][a-zA-Z0-9_.-]*$")) {
			throw new GeneralException(GeneralError.INVALID_NAME, name);
		}
	}

}
