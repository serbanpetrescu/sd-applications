package sd.a3.bl;

import java.sql.Date;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Observable;

import javax.naming.NamingException;

import sd.a3.db.Consultation;
import sd.a3.db.DatabaseToolbox;
import sd.a3.db.Patient;

/**
 * Class which models a patient. This is an observable class (multiple
 * inheritance was avoided using delegation). When a patient arrives at the
 * clinic, all the observers (doctors) are notified.
 *
 * @author Serban Petrescu
 *
 */
public class PatientBO extends BusinessObject<String, Patient> {

	private Delegate delegate;

	public ConsultationBO getNextConsultation() {
		Consultation result = null;
		try {
			result = DatabaseToolbox.getNextConsultation(this.reference);
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
		if (result != null) {
			ConsultationBO consult = new ConsultationBO(result);
			this.addObserver(consult.getDoctor());
			return consult;
		} else {
			return null;
		}
	}

	/**
	 * Reads a patient form the DB and constructs a BO.
	 *
	 * @param personalNo
	 * @return The PatientBO (if found).
	 */
	public static PatientBO get(String personalNo) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY
				| UserBO.ROLE_DOCTOR);
		return new PatientBO(personalNo);
	}

	/**
	 * Searches patients by name.
	 *
	 * @param name
	 *            A substring of the patient's name.
	 * @return A list of patients.
	 */
	public static Collection<PatientBO> searchByName(String name) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY
				| UserBO.ROLE_DOCTOR);
		try {
			Collection<Patient> c = DatabaseToolbox.searchPatientsByName(name);
			Collection<PatientBO> result = new LinkedList<PatientBO>();
			for (Patient p : c) {
				result.add(new PatientBO(p));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Deletes the patient.
	 */
	public void delete() {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		this._delete();
	}

	/**
	 * Creates a patient.
	 *
	 * @param personalNo
	 * @param name
	 * @param idCardNo
	 * @param address
	 * @param birthday
	 * @return The new PatientBO (if validations were successful).
	 */
	public static PatientBO create(String personalNo, String name,
			String idCardNo, String address, Date birthday) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		PatientBO.validatePersonalNo(personalNo);
		PatientBO.validateName(name);
		PatientBO.validateIdCardNo(idCardNo);
		PatientBO.validateBirthday(birthday);
		return new PatientBO(personalNo, name, idCardNo, address, birthday);
	}

	/**
	 * Updates the information pertaining to this patient.
	 *
	 * @param name
	 * @param idCardNo
	 * @param address
	 * @param birthday
	 */
	public void update(String name, String idCardNo, String address,
			Date birthday) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		try {
			DatabaseToolbox.begin();
			if (name != null) {
				PatientBO.validateName(name);
				this.reference.setName(name);
			}
			if (idCardNo != null) {
				PatientBO.validateIdCardNo(idCardNo);
				this.reference.setIdCardNo(idCardNo);
			}
			if (address != null) {
				this.reference.setAddress(address);
				;
			}
			if (birthday != null) {
				PatientBO.validateBirthday(birthday);
				this.reference.setBirthday(birthday);
			}
			DatabaseToolbox.commit();
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Adds a new observer.
	 *
	 * @param user
	 */
	public void addObserver(UserBO user) {
		delegate.addObserver(user);
	}

	public int countObservers() {
		return delegate.countObservers();
	}

	public void deleteObserver(UserBO user) {
		delegate.deleteObserver(user);
	}

	public void deleteObservers() {
		delegate.deleteObservers();
	}

	public String getPersonalNo() {
		return this.reference.getPersonalNo();
	}

	public String getName() {
		return this.reference.getName();
	}

	public String getIdCardNo() {
		return this.reference.getIdCardNo();
	}

	public String getAddress() {
		return this.reference.getAddress();
	}

	public Date getBirthday() {
		return this.reference.getBirthday();
	}

	/**
	 * Notifies all the observers. Called when a patient arrives at the clinic.
	 */
	public void notifyObservers() {
		ConsultationBO c = this.getNextConsultation();
		if (c != null) {
			delegate.setChanged();
			delegate.notifyObservers(c);
		} else {
			throw new BusinessException(BusinessError.NO_CONSULTATION_FOUND);
		}
	}

	protected PatientBO(String personalNo) {
		super(Patient.class, personalNo);
		this.delegate = new Delegate();
	}

	protected PatientBO(Patient reference) {
		super(Patient.class, reference, false);
		this.delegate = new Delegate();
	}

	protected PatientBO(String personalNo, String name, String idCardNo,
			String address, Date birthday) {
		super(Patient.class, new Patient(personalNo, name, idCardNo, address,
				birthday));
		this.delegate = new Delegate();
	}

	private static void validateBirthday(Date birthday) {
		if (birthday.after(new Date(System.currentTimeMillis()))) {
			throw new BusinessException(BusinessError.BIRTHDAY_VF);
		}
	}

	private static void validateName(String name) {
		if (!name.matches("^[A-Z][a-zA-Z '-]+$")) {
			throw new BusinessException(BusinessError.NAME_VF, name);
		}
	}

	private static void validatePersonalNo(String personalNo) {
		if (!personalNo
				.matches("^[1-2][0-9]{2}(1[0-2]|0[1-9])([0-2][0-9]|3[0-1])[0-9]{6}$")) {
			throw new BusinessException(BusinessError.PERSONAL_NO_VF,
					personalNo);
		}
	}

	private static void validateIdCardNo(String idCardNo) {
		if (!idCardNo.matches("^[A-Z]{2} ?[0-9]{6}$")) {
			throw new BusinessException(BusinessError.ID_CARD_NO_VF, idCardNo);
		}
	}

	/**
	 * Wrapper class for the observable object. Only promotes the visibility of
	 * some methods.
	 *
	 * @author Serban Petrescu
	 *
	 */
	private class Delegate extends Observable {
		@Override
		public synchronized void setChanged() {
			super.setChanged();
		}

		@Override
		public synchronized void clearChanged() {
			super.clearChanged();
		}
	}
}
