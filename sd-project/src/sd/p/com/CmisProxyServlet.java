package sd.p.com;

import javax.servlet.annotation.WebServlet;

import com.sap.ecm.api.AbstractCmisProxyServlet;

import sd.p.doc.DocumentManager;

/**
 * @author Serban
 * Proxy servlet for enabling the usage of the CMIS workbench.
 */
@WebServlet(urlPatterns={"/cmis/*"})
public class CmisProxyServlet extends AbstractCmisProxyServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected String getRepositoryKey() {
		return DocumentManager.REP_KEY;
	}

	@Override
	protected String getRepositoryUniqueName() {
		return DocumentManager.REP_NAME;
	}

	@Override
	protected boolean readOnlyMode() {
		return true;
	}

	@Override
	protected boolean requireAuthentication() {
		return false;
	}
}
