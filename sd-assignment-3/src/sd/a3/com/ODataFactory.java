package sd.a3.com;

import org.apache.olingo.odata2.api.ODataService;
import org.apache.olingo.odata2.api.ODataServiceFactory;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.processor.ODataContext;

/**
 * The ODataServiceFactory which builds the OData service from the {@link ODataEdmProvider}
 * and the {@link ODataSProcessor}.
 *
 * @author Serban Petrescu
 *
 */
public class ODataFactory extends ODataServiceFactory {

	@Override
	public ODataService createService(ODataContext ctx) throws ODataException {
		return createODataSingleProcessorService(new ODataEdmProvider(),
				new ODataSProcessor());
	}

}
