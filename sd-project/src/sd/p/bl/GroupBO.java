package sd.p.bl;

import java.util.Collection;
import java.util.LinkedList;

import sd.p.db.DatabaseToolbox;
import sd.p.db.Group;
import sd.p.db.Role;
import sd.p.db.Template;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public class GroupBO extends BusinessObject<Group, String> {

	public static Collection<GroupBO> search(String name) {
		if (UserBO.getLoggedInUser().getIsOwner()) {
			return createFromSet(DatabaseToolbox.searchGroups(name));
		} else {
			Collection<GroupBO> groups = UserBO.getLoggedInUser()
					.getAccessibleGroups();
			Collection<GroupBO> result = new LinkedList<GroupBO>();
			name = name.toLowerCase();
			for (GroupBO g : groups) {
				if (g.getName().toLowerCase().contains(name)) {
					result.add(g);
				}
			}
			return result;
		}
	}

	public GroupBO(Group dbObject, boolean check) {
		super(Group.class, dbObject, check);
	}

	public GroupBO(String dbKey) {
		super(Group.class, dbKey);
	}

	public static GroupBO create(String name, String description) {
		UserBO.checkLoggedInUserAuthorization();
		validateName(name);
		GroupBO g = new GroupBO(new Group(name, description), true);
		LogTM.getInstance().create(LogTM.Description.GROUP_CREATED, name);
		return g;
	}

	public static GroupBO get(String name) {
		if (UserBO.getLoggedInUser().getIsOwner()) {
			return new GroupBO(name);
		}
		else {
			Collection<GroupBO> groups = UserBO.getLoggedInUser().getAccessibleGroups();
			for (GroupBO g : groups) {
				if (g.getName().equals(name)) {
					return g;
				}
			}
			throw new GeneralException(GeneralError.USER_NO_ROLE_FOR_GROUP, name);
		}
	}

	public void delete() {
		UserBO.checkLoggedInUserAuthorization();
		String name = this.getName();
		this.deleteBase();
		LogTM.getInstance().create(LogTM.Description.GROUP_DELETED, name);
	}

	public void setRole(Role role) {
		UserBO.checkLoggedInUserAuthorization();
		DatabaseToolbox.begin();
		this.dbObject.setRole(role);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.GROUP_CNG_ROLE,
				this.getName(), role == null ? null : role.getName());
	}

	public void setDescription(String description) {
		UserBO.checkLoggedInUserAuthorization();
		DatabaseToolbox.begin();
		this.dbObject.setDescription(description);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.GROUP_UPDATED, this.getName());
	}

	public String getName() {
		return this.dbObject.getName();
	}

	public Role getRole() {
		return this.dbObject.getRole();
	}

	public String getDescription() {
		return this.dbObject.getDescription();
	}

	public void addTemplate(TemplateBO template) {
		UserBO.checkLoggedInUserAuthorization();
		Template t = template.getDbObject();
		if (!this.dbObject.getTemplates().contains(t)) {
			DatabaseToolbox.begin();
			this.dbObject.getTemplates().add(t);
			DatabaseToolbox.commit();
			LogTM.getInstance().create(LogTM.Description.GROUP_ADD_TEMPL, this.getName(), template.getName());
		} else {
			throw new GeneralException(GeneralError.TEMPLATE_ALREADY_IN_GRP,
					this.getName(), template.getName());
		}
	}

	public void removeTemplate(TemplateBO template) {
		UserBO.checkLoggedInUserAuthorization();
		Template t = template.getDbObject();
		DatabaseToolbox.begin();
		this.dbObject.getTemplates().remove(t);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.GROUP_ADD_TEMPL, this.getName(), template.getName());
	}

	public Collection<TemplateBO> getTemplates() {
		return TemplateBO.createFromSet(this.dbObject.getTemplates());
	}

	protected static Collection<GroupBO> createFromSet(
			Collection<Group> resultSet) {
		Collection<GroupBO> Groups = new LinkedList<GroupBO>();
		for (Group t : resultSet) {
			Groups.add(new GroupBO(t, false));
		}
		return Groups;
	}
}
