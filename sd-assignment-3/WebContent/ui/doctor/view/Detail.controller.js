sap.ui.core.mvc.Controller.extend("sd.a3.ui.doctor.view.Detail", {

	onInit : function() {
		this.oInitialLoadFinishedDeferred = jQuery.Deferred();

		if(sap.ui.Device.system.phone) {
			//Do not wait for the master when in mobile phone resolution
			this.oInitialLoadFinishedDeferred.resolve();
		} else {
			this.getView().setBusy(true);
			var oEventBus = this.getEventBus();
			oEventBus.subscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
			oEventBus.subscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		}

		this.getRouter().attachRouteMatched(this.onRouteMatched, this);
	},

	logoff: function() {
		session.logoff();
	},

    formatDate: function(d) {
        if (d) {
    	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            var date = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: i18n.getText("dateFormat")});
            return date.format(d);
        }
    },

    formatPeriod: function(start, end) {
        if (start && end) {
    	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            var time = sap.ui.core.format.DateFormat.getTimeInstance({pattern: i18n.getText("timeFormat")});
            var date = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: i18n.getText("dateFormat")});
            return  i18n.getText("periodFormat", [date.format(start), time.format(start), time.format(end)]);
        }
    },

	onMasterLoaded :  function () {
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
	},

	onMetadataFailed : function(){
		this.getView().setBusy(false);
		this.oInitialLoadFinishedDeferred.resolve();
        this.showEmptyView();
	},

	onRouteMatched : function(oEvent) {
		var oParameters = oEvent.getParameters();

		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(function () {
			var oView = this.getView();

			// When navigating in the Detail page, update the binding context
			if (oParameters.name !== "detail") {
				return;
			}

			var sEntityPath = "/" + oParameters.arguments.entity;
			this.bindView(sEntityPath);

			var oIconTabBar = oView.byId("idIconTabBar");
			oIconTabBar.getItems().forEach(function(oItem) {
			    if(oItem.getKey() !== "selfInfo"){
    				oItem.bindElement(oItem.getKey());
			    }
			});

			// Specify the tab being focused
			var sTabKey = oParameters.arguments.tab;
			this.getEventBus().publish("Detail", "TabChanged", { sTabKey : sTabKey });

			if (oIconTabBar.getSelectedKey() !== sTabKey) {
				oIconTabBar.setSelectedKey(sTabKey);
			}
		}, this));

	},

	bindView : function (sEntityPath) {
		var oView = this.getView();
		oView.bindElement(sEntityPath);

		//Check if the data is already on the client
		if(!oView.getModel().getData(sEntityPath)) {

			// Check that the entity specified was found.
			oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
				var oData = oView.getModel().getData(sEntityPath);
				if (!oData) {
					this.showEmptyView();
					this.fireDetailNotFound();
				} else {
				    this.bindIconTab();
					this.fireDetailChanged(sEntityPath);
				}
			}, this));

		} else {
			this.bindIconTab();
			this.fireDetailChanged(sEntityPath);
		}

	},

	getButtonEnabled: function() {
	    var context = this.getView().getBindingContext();
	    var startDate = context.getProperty("StartTime");
	    var description = context.getProperty("Description");
	    if (startDate) {
    	    var now = new Date().getTime();
    	    var start = startDate.getTime();
    	    if (start < now - 24 * 3600 * 1000) {
    	        if (description) {
    	            return false;
    	        }
    	        else {
    	            return true;
    	        }
    	    }
    	    else if (start > now + 24 * 3600 * 1000) {
    	        return false;
    	    }
    	    else {
    	        return true;
    	    }
	    }
	},

	bindIconTab: function() {
	    var patient = this.getView().getBindingContext().getProperty("Patient");
	    if (patient) {
	        var path = "/Patients('" + patient + "')";
	        this.byId("idIconTabBar").bindElement(path);
	        var item = new sap.m.StandardListItem({press:this.onSelect, type:"Navigation", description:"{Doctor}" });
	        item.bindProperty("title", {
	        	parts: [{path: "StartTime"}, {path: "EndTime"}],
	        	formatter: jQuery.proxy(this.formatPeriod, this)
	        });
	        this.byId("consultationList").bindItems(path + "/Consultations", item);
    	    if(!this.getView().getModel().getData(path)) {

    			this.byId("idIconTabBar").getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
    				this.byId("completeBtn").setEnabled(this.getButtonEnabled());
    			}, this));

    		} else {
    		    this.byId("completeBtn").setEnabled(this.getButtonEnabled());
    		}
	    }
	},

	onSelect: function(oEvent) {
	    var context = oEvent.getSource().getBindingContext();
	    jQuery.sap.require("sap.ca.ui.message.message");
	    sap.ca.ui.message.showMessageBox({
            type: sap.ca.ui.message.Type.INFO,
            message: context.getProperty("Description")
        });
	},

	completeConsult: function() {
	    jQuery.sap.require("sap.ca.ui.dialog.factory");
    	var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
	    sap.ca.ui.dialog.confirmation.open({
            question : i18n.getText("setDescriptionQuestion"),
            noteMandatory : true,
            title : i18n.getText("setDescriptionTitle"),
            confirmButtonLabel : i18n.getText("setDescriptionLabel")
        }, jQuery.proxy(this.saveConsultNote, this));
	},

    saveConsultNote: function(oEvent) {
        var path = this.getView().getBindingContext().getPath();
        this.getOwnerComponent().getModel().update(path, {
            Description: oEvent.sNote
        }, {error: showError});
    },

	showEmptyView : function () {
		this.getRouter().myNavToWithoutHash({
			currentView : this.getView(),
			targetViewName : "sd.a3.ui.doctor.view.NotFound",
			targetViewType : "XML"
		});
	},

	fireDetailChanged : function (sEntityPath) {
		this.getEventBus().publish("Detail", "Changed", { sEntityPath : sEntityPath });
	},

	fireDetailNotFound : function () {
		this.getEventBus().publish("Detail", "NotFound");
	},

	onNavBack : function() {
		// This is only relevant when running on phone devices
		this.getRouter().myNavBack("main");
	},

	onDetailSelect : function(oEvent) {
		sap.ui.core.UIComponent.getRouterFor(this).navTo("detail",{
			entity : oEvent.getSource().getBindingContext().getPath().slice(1),
			tab: oEvent.getParameter("selectedKey")
		}, true);
	},

	getEventBus : function () {
		return sap.ui.getCore().getEventBus();
	},

	getRouter : function () {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},

	onExit : function(){
	    var oEventBus = this.getEventBus();
    	oEventBus.unsubscribe("Master", "InitialLoadFinished", this.onMasterLoaded, this);
		oEventBus.unsubscribe("Component", "MetadataFailed", this.onMetadataFailed, this);
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
	}
});
