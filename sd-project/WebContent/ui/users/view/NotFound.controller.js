/* global logoff */
sap.ui.core.mvc.Controller.extend("sd.p.ui.users.view.NotFound", {
    onLogoff: function() {
        logoff();
    }
});