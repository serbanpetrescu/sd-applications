package sd.a3.com;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * The websocket endpoint used to communicate with the doctors.
 *
 * @author Serban Petrescu
 *
 */
@ServerEndpoint(value = "/websocket", configurator = WSHttpSessionConfigurator.class)
public class WSEndpoint {

	public static ConcurrentHashMap<String, WSEndpoint> clients = new ConcurrentHashMap<String, WSEndpoint>();

	private Session session;
	private String username;

	public WSEndpoint() {
		session = null;
		username = "";
	}

	@OnOpen
	public void open(Session session, EndpointConfig config) {
		Boolean isDoctor = (Boolean) config.getUserProperties().get(
				WSHttpSessionConfigurator.PROP_ISDOCTOR);
		if (isDoctor) {
			this.session = session;
			this.username = (String) config.getUserProperties().get(
					WSHttpSessionConfigurator.PROP_USERNAME);
			if (clients.containsKey(this.username)) {
				WSEndpoint that = clients.get(this.username);
				try {
					that.session.close();
				} catch (IOException e) {
				}
			}
			clients.put(this.username, this);
		} else {
			try {
				session.close();
			} catch (IOException e) { /* soooo, what to do now? */
			}
		}
	}

	@OnError
	public void error(Throwable e) {
		// do nothing...
	}

	/**
	 * Sends a refresh signal to the doctor with the given username (if he is
	 * online). The refresh signal makes the UI refresh the data model.
	 *
	 * @param username
	 */
	public static void sendDoctorRefresh(String username) {
		if (clients.get(username) != null) {
			try {
				clients.get(username).session.getBasicRemote().sendText(
						"{\"refresh\": 1}");
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Send a notification to the doctor paired with this session. The
	 * notification specifies that the given patient has arrived and has a
	 * consultation starting a the given time.
	 *
	 * @param patient
	 * @param start The start time of the consultation.
	 * @throws IOException
	 */
	public void sendNotification(String patient, Timestamp start)
			throws IOException {
		session.getBasicRemote().sendText(
				"{\"patient\": " + escapeJSON(patient) + ", \"start\": "
						+ start.getTime() + "}");
	}

	@OnClose
	public void close() {
		if (username != null && username.length() > 0) {
			clients.remove(username);
		}
	}

	/**
	 * Escape a JSON-string for literals. Source: Jettison open source JSON
	 * library.
	 */
	public static String escapeJSON(String string) {
		if (string == null || string.length() == 0) {
			return "\"\"";
		}

		char c = 0;
		int i;
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		String t;

		sb.append('"');
		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
			case '"':
				sb.append('\\');
				sb.append(c);
				break;
			case '/':
				// if (b == '<') {
				sb.append('\\');
				// }
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else {
					sb.append(c);
				}
			}
		}
		sb.append('"');
		return sb.toString();
	}

}
