package sd.p.bl;

import java.sql.Timestamp;
import java.util.UUID;

import sd.p.db.Log;
import sd.p.db.User;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;
import sd.p.util.JSONToolbox;

public class LogTM extends TableModule<Log, String> {

	private static volatile LogTM instance = null;

	public static LogTM getInstance() {
		if (instance == null) {
			synchronized (LogTM.class) {
				if (instance == null) {
					instance = new LogTM();
				}
			}
		}
		return instance;
	}

	private LogTM() {
		super(Log.class);
	}

	public UserBO getAuthor(String guid) {
		UserBO.checkLoggedInUserAuthorization();
		return new UserBO(this.get(guid).getAuthor(), false);
	}

	@Override
	public void delete(String key) {
		throw new GeneralException(GeneralError.LOGS_ARE_READONLY);
	}
	
	public Log create(Description description, String... args) {
		return create(UserBO.getLoggedInUser().getDbObject(), description.toString(args));
	}

	protected Log create(User author, String description) {
		Log log = new Log(UUID.randomUUID().toString(), new Timestamp(
				System.currentTimeMillis()), description, author);
		this.persistBase(log);
		return log;
	}

	public static enum Description {
		USER_CREATED	(1001),
		USER_DELETED	(1002),
		USER_UPDATED	(1003),
		USER_ADD_ROLE	(1004),
		USER_DEL_ROLE	(1005),
		USER_ACTIVITY	(1006),
		
		GROUP_CREATED	(2001),
		GROUP_DELETED	(2002),
		GROUP_UPDATED	(2003),
		GROUP_ADD_TEMPL (2004),
		GROUP_DEL_TEMPL (2005),
		GROUP_CNG_ROLE	(2006),
		
		TEMPL_CREATED	(3001),
		TEMPL_DELETED	(3002),
		TEMPL_UPDATED	(3003),
		TEMPL_UPLOADED	(3004),
		
		FIELD_CREATED	(4001),
		FIELD_DELETED	(4002),
		FIELD_UPDATED	(4003),
		
		ROLE_CREATED	(5001),
		ROLE_DELETED	(5002),
		ROLE_UPDATED	(5003);

		public static final String JSON_CODE = "c";
		public static final String JSON_ARGS = "a";

		private int code;

		private Description(int code) {
			this.code = code;
		}

		public int getCode() {
			return this.code;
		}

		public String toString(String... args) {
			return JSONToolbox.serializeObject(
					JSONToolbox.serializePair(JSON_CODE,
							Integer.toString(this.code), false),
					JSONToolbox.serializePair(JSON_ARGS,
							JSONToolbox.serializeArray(args, true), false));
		}
	}
}
