package sd.p.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class Zip {

	public static final String LOG_NAME = "log.txt";
	
	private StringBuilder log;
	private ZipOutputStream zos;

	public Zip(OutputStream out) {
		log = new StringBuilder();
		zos = new ZipOutputStream(out);
	}
	
	public void addToZip(File file) {
		try {
			FileInputStream fin = new FileInputStream(file);
			addToZip(file.getName(), fin);
			fin.close();
		} catch (IOException e) {
			writeToLog(e.getMessage());
		}
	}
	
	public void addToZip(String filename, InputStream in) {

		try {
			ZipEntry zipEntry = new ZipEntry(filename);
			zos.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			try {

				while ((length = in.read(bytes)) >= 0) {
					zos.write(bytes, 0, length);
				}
			} catch (Exception e) {
				log.append("Could not write into zip entry: ");
				log.append(filename);
				log.append(".\n");
			}
			
			zos.closeEntry();
		} catch (Exception e) {
			log.append("Could not create zip entry: ");
			log.append(filename);
			log.append(".\n");
		}
	}
	
	public void writeToLog(String str)
	{
		log.append(str);
		log.append("\n");
	}

	public void close() {
		try {
			if (log.length() > 0)
			{
				ZipEntry zipEntry = new ZipEntry(LOG_NAME);
				zos.putNextEntry(zipEntry);
				zos.write(log.toString().getBytes());
				zos.closeEntry();
			}
			zos.close();
		} catch (Exception e) { }
	}

}
