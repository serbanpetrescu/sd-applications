package sd.p.doc;

import java.util.HashMap;
import java.util.Map;

import sd.p.db.SimpleField;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.uno.XInterface;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextRange;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.XBridge;
import com.sun.star.bridge.XBridgeFactory;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.connection.XConnection;
import com.sun.star.connection.XConnector;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XStorable;
import com.sun.star.io.IOException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.util.XReplaceable;
import com.sun.star.util.XReplaceDescriptor;
import com.sun.star.util.XSearchDescriptor;

public enum OOProxy {
	INSTANCE;

	XDesktop xDesktop = null;

	public static final String FIELD_REGEX = "<[a-zA-Z_0-9]*>";
	public static final String OO_HOST = "localhost";
	public static final String OO_PORT = "8100";

	public OODocumentBuilder createDocumentBuilder() {
		return new DocumentBuilder();
	}
	
	private OOProxy() {
		this.setDesktop();
	}
	
	private void setDesktop() {
		XDesktop xDesktop = null;

		try {

			Object x;
			Object defaultContext;
			Object desktop;
			XComponentContext componentContext = Bootstrap
					.createInitialComponentContext(null);
			x = componentContext.getServiceManager().createInstanceWithContext(
					"com.sun.star.connection.Connector", componentContext);
			XConnector connector = (XConnector) UnoRuntime.queryInterface(
					XConnector.class, x);
			XConnection connection = connector.connect("socket,host=" + OO_HOST
					+ ",port=" + OO_PORT);
			x = componentContext.getServiceManager().createInstanceWithContext(
					"com.sun.star.bridge.BridgeFactory", componentContext);
			XBridgeFactory bridgeFactory = (XBridgeFactory) UnoRuntime
					.queryInterface(XBridgeFactory.class, x);
			XBridge bridge = bridgeFactory.createBridge("", "urp", connection,
					null);
			x = bridge.getInstance("StarOffice.ServiceManager");
			XMultiComponentFactory multiComponentFactory = (XMultiComponentFactory) UnoRuntime
					.queryInterface(XMultiComponentFactory.class, x);
			XPropertySet properySet = (XPropertySet) UnoRuntime.queryInterface(
					XPropertySet.class, multiComponentFactory);
			defaultContext = properySet.getPropertyValue("DefaultContext");
			componentContext = (XComponentContext) UnoRuntime.queryInterface(
					XComponentContext.class, defaultContext);
			multiComponentFactory = componentContext.getServiceManager();
			desktop = multiComponentFactory.createInstanceWithContext(
					"com.sun.star.frame.Desktop", componentContext);
			xDesktop = (com.sun.star.frame.XDesktop) UnoRuntime.queryInterface(
					XDesktop.class, desktop);
			this.xDesktop = xDesktop;
		} catch (Exception e) {
			throw new GeneralException(GeneralError.UNABLE_TO_CONNECT_OO);
		}
	}

	private static String convertToUrl(String path) {
		StringBuffer sUrl = new StringBuffer("file:///");
		sUrl.append(path.replace('\\', '/'));
		return sUrl.toString();
	}

	private static String createUrlFromDestination(String destFolder,
			String destName) {
		StringBuffer sPath = new StringBuffer(destFolder);
		if (sPath.lastIndexOf("\\") != sPath.length() - 1) {
			sPath.append('\\');
		}
		sPath.append(destName);
		return convertToUrl(sPath.toString());
	}

	private class DocumentBuilder implements OODocumentBuilder{

		XTextDocument xTextDocument = null;
		XReplaceable xReplaceable = null;
		XReplaceDescriptor xReplaceDescriptor = null;
		XSearchDescriptor xSearchDescriptor = null;
		XInterface lastResult = null;
		Map<String, String> fields;

		public DocumentBuilder() {
			fields = new HashMap<String, String>();
		}

		private void openDocument(byte[] in) {

			XComponent xComponent = null;
			XComponentLoader xComponentLoader = null;
			PropertyValue xLoadArgs[] = new PropertyValue[2];
			com.sun.star.text.XTextDocument aTextDocument = null;
			xLoadArgs[0] = new PropertyValue();
			xLoadArgs[0].Name = "Hidden";
			xLoadArgs[0].Value = new Boolean(true);

			xLoadArgs[1] = new PropertyValue();
			xLoadArgs[1].Name = "InputStream";
			xLoadArgs[1].Value = new OOInputStream(in);

			try {
				xComponentLoader = (XComponentLoader) UnoRuntime
						.queryInterface(XComponentLoader.class, xDesktop);
				xComponent = xComponentLoader.loadComponentFromURL(
						"private:stream", "_blank", 0, xLoadArgs);
				aTextDocument = (XTextDocument) UnoRuntime.queryInterface(
						XTextDocument.class, xComponent);
			} catch (Exception e) {
				e.printStackTrace(System.err);
			}

			this.xTextDocument = aTextDocument;
		}

		private void close() {
			if (this.xTextDocument != null) {
				this.xTextDocument.dispose();
			}

			this.xTextDocument = null;
			this.xReplaceable = null;
			this.xReplaceDescriptor = null;
			this.xSearchDescriptor = null;
			this.lastResult = null;
		}

		private String findNextField() {
			if (this.xReplaceable != null && this.xSearchDescriptor != null) {
				if (this.lastResult == null) {
					this.lastResult = (XInterface) this.xReplaceable
							.findFirst(this.xSearchDescriptor);
				} else {
					this.lastResult = (XInterface) this.xReplaceable.findNext(
							this.lastResult, this.xSearchDescriptor);
				}

				if (this.lastResult == null) {
					return null;
				}

				XTextRange textRange = (XTextRange) UnoRuntime.queryInterface(
						XTextRange.class, this.lastResult);
				return textRange.getString();
			} else {
				return null;
			}
		}

		private int replaceAll(String oldValue, String newValue) {
			if (this.xReplaceable != null && this.xReplaceDescriptor != null) {
				this.xReplaceDescriptor.setSearchString(oldValue);
				this.xReplaceDescriptor.setReplaceString(newValue);
				return this.xReplaceable.replaceAll(this.xReplaceDescriptor);
			} else {
				return -1;
			}
		}

		private void setReplaceable() {
			if (this.xTextDocument != null) {
				this.xReplaceable = (XReplaceable) UnoRuntime.queryInterface(
						XReplaceable.class, this.xTextDocument);
			} else {
				this.xReplaceable = null;
			}
		}

		private void setReplaceDescriptor() {
			if (this.xReplaceable != null) {
				this.xReplaceDescriptor = this.xReplaceable
						.createReplaceDescriptor();
				try {
					this.xReplaceDescriptor.setPropertyValue(
							"SearchCaseSensitive", true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				this.xReplaceDescriptor = null;
			}
		}

		private void setSearchDescriptor() {
			if (this.xReplaceable != null) {
				this.xSearchDescriptor = this.xReplaceable
						.createSearchDescriptor();
				this.xSearchDescriptor.setSearchString(FIELD_REGEX);
				try {
					this.xSearchDescriptor.setPropertyValue(
							"SearchRegularExpression", true);
					this.xSearchDescriptor.setPropertyValue(
							"SearchCaseSensitive", true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				this.xSearchDescriptor = null;
			}
		}

		private boolean open(byte[] file) {
			this.openDocument(file);
			this.setReplaceable();
			this.setReplaceDescriptor();
			this.setSearchDescriptor();
			return (this.xTextDocument != null);
		}

		private boolean save(String destFolder, String destName) {
			if (this.xTextDocument != null) {
				XStorable doc = (XStorable) UnoRuntime.queryInterface(
						XStorable.class, this.xTextDocument);
				try {
					doc.storeToURL(
							createUrlFromDestination(destFolder, destName),
							new PropertyValue[0]);
				} catch (IOException e) {
					return false;
				}
				return true;
			}
			return false;
		}

		public void setFields(Map<SimpleField, String> simpleFields) {
			fields.clear();
			for (Map.Entry<SimpleField, String> e : simpleFields.entrySet()) {
				fields.put("<" + e.getKey().getName() + ">", e.getValue());
			}
		}

		public void build(byte[] file, String destFolder, String destFile) {

			String field;
			if (this.open(file)) {
				while ((field = this.findNextField()) != null) {
					if (fields.containsKey(field)) {
						this.replaceAll(field, fields.get(field));
					}
				}

				this.save(destFolder, destFile);
				this.close();
			} else {
				throw new GeneralException(GeneralError.UNABLE_TO_CONNECT_OO);
			}
		}

	}

}
