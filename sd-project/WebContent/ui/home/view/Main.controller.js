sap.ui.controller("sd.p.ui.home.view.Main", {

    onInit : function() {
    	var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
        this.getView().setModel(new sap.ui.model.json.JSONModel([
            {title: i18n.getText("buildTitle"), component: "build", icon: "sap-icon://technical-object", admin: false},
            {title: i18n.getText("usersTitle"), component: "users", icon: "sap-icon://group", admin: true},
            {title: i18n.getText("fieldsTitle"), component: "fields", icon: "sap-icon://tags", admin: true},
            {title: i18n.getText("groupsTitle"), component: "groups", icon: "sap-icon://group-2", admin: true},
            {title: i18n.getText("templatesTitle"), component: "templates", icon: "sap-icon://enter-more", admin: true},
            {title: i18n.getText("rolesTitle"), component: "roles", icon: "sap-icon://role", admin: true}
        ]));
    },
    
    checkNav: function() {
    	if (window.location.href.indexOf('#') > 0) {
        	var hash = window.location.href.substr(window.location.href.indexOf("#")).toLowerCase();
        	var data = this.getView().getModel().getData();
        	for (var i = 0 ; i < data.length; ++i) {
        		if (hash.indexOf(data[i].component) !== -1) {
        			if (!data[i].admin || (session && session.role)) {
        				navigate(data[i].component);
        				return;
        			}
        		}
        		
        	}
    	}
    },

    onLogoff: function() {
        logoff();
    },
       
    onAfterRendering: function() {
        this.checkNav();
    },
    
    onBeforeRendering: function() {
        this.getView().getModel().refresh(true);
    },
    
    runApp: function(oEvent) {
        var data = oEvent.getSource().getCustomData();
        if (data && data.length) {
            navigate(data[0].getValue());
        }
    },
    
    setTileVisibility: function(fAdmin) {
        if (fAdmin) {
            if (session && session.role) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }
});
