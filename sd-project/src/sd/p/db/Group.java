package sd.p.db;

import java.io.Serializable;
import java.lang.String;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty.Multiplicity;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: Group
 *
 */
@Entity
@Table(name="T_GROUP")
@NamedQueries({
    @NamedQuery(name="Group.readAll",
    			query="SELECT f FROM Group f"),
    @NamedQuery(name="Group.readPublic",
    			query="SELECT f FROM Group f WHERE f.role IS NULL"),
    @NamedQuery(name="Group.searchByName",
    			query="SELECT f FROM Group f WHERE LOWER( f.name ) LIKE :value"),
}) 
@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=Group.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=Group.EDM_ESN)
public class Group implements Serializable, PersistentEntity<String> {

	public static final String EDM_EN = "Group";
	public static final String EDM_ESN = "Groups";
	public static final String EDM_PN_NAME = "Name";
	public static final String EDM_PN_DESC = "Description";
	public static final String EDM_NP_ROLE = "Role";
	public static final String EDM_NP_TEMPLATES = "Templates";
	   
	@Id
	@EdmKey
	@EdmProperty(name=EDM_PN_NAME, type=EdmType.STRING)
	private String name;
	
	@EdmProperty(name=EDM_PN_DESC, type=EdmType.STRING)
	private String description;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="roleName")
	@EdmNavigationProperty(toType=Role.class, name=EDM_NP_ROLE, toMultiplicity=Multiplicity.ZERO_OR_ONE)
	private Role role;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="T_TEMPLATE_GROUP",
	joinColumns = @JoinColumn(name="groupname", referencedColumnName="name"),
	inverseJoinColumns = @JoinColumn(name="templatename", referencedColumnName="name"))
	@EdmNavigationProperty(toType=Template.class, name=EDM_NP_TEMPLATES, toMultiplicity=Multiplicity.MANY)
	private Collection<Template> templates;
	
	private static final long serialVersionUID = 1L;

	public Group() {
		super();
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		if (this.role != null) {
			this.role.getGroups().remove(this);
		}
		this.role = role;
		if (this.role != null) {
			this.role.getGroups().add(this);
		}
	}
	@Override
	public String getKey() {
		return this.name;
	}
	
	public Group(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.role = null;
		this.templates = new LinkedList<Template>();
	}
	
	public Collection<Template> getTemplates() {
		return templates;
	}
	public void setTemplates(Collection<Template> templates) {
		this.templates = templates;
	}
	
}
