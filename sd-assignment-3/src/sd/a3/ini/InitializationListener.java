package sd.a3.ini;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import sd.a3.bl.UserBO;
import sd.a3.db.Consultation;
import sd.a3.db.DatabaseToolbox;
import sd.a3.db.Patient;
import sd.a3.db.User;

/**
 * Listener class which fills in the DB with initial mock data. This class
 * communicates directly with the DB layer, but it ensures DB consistency (BO
 * level was not used because of performance reasons).
 *
 * @author Serban Petrescu
 *
 */
@WebListener
public class InitializationListener implements ServletContextListener {
	private static final String ADMIN_UNAME = "admin";

	private ArrayList<User> doctors;
	private ArrayList<Patient> patients;

	/**
	 * Loads all the mock users into the DB.
	 *
	 * @throws NamingException
	 */
	private void loadUsers() throws NamingException {
		Scanner in = new Scanner(
				InitializationListener.class.getResourceAsStream("users.txt"));
		while (in.hasNextLine()) {
			String[] s = in.nextLine().split("\t");
			if (s.length > 3) {
				int role = Integer.parseInt(s[3]);
				User u = new User(s[0], s[1], s[2], role);
				if (role == UserBO.ROLE_DOCTOR) {
					doctors.add(u);
				}
				DatabaseToolbox.persist(u);
			}
		}
		in.close();
	}

	/**
	 * Loads all the mock patients into the DB.
	 *
	 * @throws NamingException
	 */
	private void loadPatients() throws NamingException {
		Scanner in = new Scanner(
				InitializationListener.class
						.getResourceAsStream("patients.txt"));
		while (in.hasNextLine()) {
			String[] s = in.nextLine().split("\t");
			if (s.length > 4) {
				Patient p = new Patient(s[1], s[0], s[2], s[4],
						Date.valueOf(s[3]));
				patients.add(p);
				DatabaseToolbox.persist(p);
			}
		}
		in.close();
	}

	/**
	 * Loads all the mock consultations into the DB.
	 *
	 * @throws NamingException
	 */
	private void loadConsultations() throws NamingException {
		Scanner in = new Scanner(
				InitializationListener.class
						.getResourceAsStream("consultations.txt"));
		while (in.hasNextLine()) {
			String[] s = in.nextLine().split("\t");
			if (s.length > 5) {
				int doctor = Integer.parseInt(s[4]) % doctors.size();
				int patient = Integer.parseInt(s[5]) % patients.size();
				long start, end;
				start = end = Date.valueOf(s[1]).getTime();
				start += Time.valueOf(s[2] + ":00").getTime();
				end += Time.valueOf(s[3] + ":00").getTime();

				if (start > end) {
					long aux = start;
					start = end;
					end = aux;
				}

				if (s[0].length() > 200) {
					s[0] = s[0].substring(0, 200);
				}

				DatabaseToolbox.persist(new Consultation(UUID.randomUUID()
						.toString(), new Timestamp(start), new Timestamp(end),
						s[0], doctors.get(doctor), patients.get(patient)));
			}
		}
		in.close();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			if (!DatabaseToolbox.existsUser(ADMIN_UNAME)) {
				doctors = new ArrayList<User>(100);
				patients = new ArrayList<Patient>(100);
				loadUsers();
				loadPatients();
				loadConsultations();
				doctors.clear();
				patients.clear();
				doctors = null;
				patients = null;
			}
		} catch (NamingException e) {
			// nothing to do really;
		}
	}
}
