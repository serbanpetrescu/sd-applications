package sd.a3.bl;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

import javax.naming.NamingException;

import sd.a3.db.Consultation;
import sd.a3.db.DatabaseToolbox;

/**
 * BO which models a consultation.
 *
 * @author Serban Petrescu
 *
 */
public class ConsultationBO extends BusinessObject<String, Consultation> {

	/**
	 * Reads and builds a consultation.
	 *
	 * @param uuid
	 * @return the ConsultationBO (if found)
	 */
	public static ConsultationBO get(String uuid) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY
				| UserBO.ROLE_DOCTOR);
		return new ConsultationBO(uuid);
	}

	/**
	 * Gets all future consultations for a given doctor.
	 *
	 * @param doctor
	 * @return A list of consultations.
	 */
	public static Collection<ConsultationBO> getNextByDoctor(UserBO doctor) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_DOCTOR
				| UserBO.ROLE_SECRETARY);
		try {
			Collection<Consultation> c = DatabaseToolbox
					.getFutureConsultations(doctor.reference);
			Collection<ConsultationBO> result = new LinkedList<ConsultationBO>();
			for (Consultation p : c) {
				result.add(new ConsultationBO(p));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Gets all the consultations for a patient.
	 *
	 * @param patient
	 * @return A list of consultations.
	 */
	public static Collection<ConsultationBO> getAllByPatient(PatientBO patient) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		Collection<Consultation> c = patient.reference.getConsultations();
		Collection<ConsultationBO> result = new LinkedList<ConsultationBO>();
		for (Consultation p : c) {
			result.add(new ConsultationBO(p));
		}
		return result;
	}

	/**
	 * Returns all "open" (with no result attached) consultations for a patient.
	 *
	 * @param patient
	 * @return A list of consultations.
	 */
	public static Collection<ConsultationBO> getOpenByPatient(PatientBO patient) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		try {
			Collection<Consultation> c = DatabaseToolbox
					.getOpenConsultations(patient.reference);
			Collection<ConsultationBO> result = new LinkedList<ConsultationBO>();
			for (Consultation p : c) {
				result.add(new ConsultationBO(p));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Returns all past consultations for a patient.
	 *
	 * @param patient
	 * @return A list of consultations.
	 */
	public static Collection<ConsultationBO> getPastByPatient(PatientBO patient) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_DOCTOR);
		try {
			Collection<Consultation> c = DatabaseToolbox
					.getPastConsultations(patient.reference);
			Collection<ConsultationBO> result = new LinkedList<ConsultationBO>();
			for (Consultation p : c) {
				result.add(new ConsultationBO(p));
			}
			return result;
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Deletes the consultation.
	 */
	public void delete() {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		this._delete();
	}

	/**
	 * Creates a new consultation.
	 *
	 * @param startTime
	 * @param endTime
	 * @param description
	 * @param doctor
	 * @param patient
	 * @return The ConsultationBO (if validations were successful)
	 */
	public static ConsultationBO create(Timestamp startTime, Timestamp endTime,
			String description, UserBO doctor, PatientBO patient) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		if (doctor.hasRole(UserBO.ROLE_DOCTOR)) {
			ConsultationBO.validateTimes(startTime, endTime, doctor);
			return new ConsultationBO(UUID.randomUUID().toString(), startTime,
					endTime, description, doctor, patient);
		} else {
			throw new BusinessException(BusinessError.NOT_A_DOCTOR,
					doctor.getName());
		}

	}

	/**
	 * Attempts to reschedule the consultation.
	 *
	 * @param start
	 * @param end
	 */
	public void changeAppointment(Timestamp start, Timestamp end) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		validateTimes(this, start, end, this.getDoctor());
		try {
			DatabaseToolbox.begin();
			this.reference.setStartTime(start);
			this.reference.setEndTime(end);
			DatabaseToolbox.commit();
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Attempts to modify the description (set the result) of a consultation.
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_DOCTOR);
		this.reference.setDescription(description);
		try {
			DatabaseToolbox.begin();
			DatabaseToolbox.commit();
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	public String getUuid() {
		return this.reference.getUuid();
	}

	public Timestamp getStartTime() {
		return this.reference.getStartTime();
	}

	public Timestamp getEndTime() {
		return this.reference.getEndTime();
	}

	public String getDescription() {
		return this.reference.getDescription();
	}

	public UserBO getDoctor() {
		return new UserBO(this.reference.getDoctor());
	}

	public PatientBO getPatient() {
		return new PatientBO(this.reference.getPatient());
	}

	protected ConsultationBO(Consultation reference) {
		super(Consultation.class, reference, false);
		// this.getPatient().addObserver(this.getDoctor());
	}

	protected ConsultationBO(String uuid) {
		super(Consultation.class, uuid);
		// this.getPatient().addObserver(this.getDoctor());
	}

	protected ConsultationBO(String uuid, Timestamp startTime,
			Timestamp endTime, String description, UserBO doctor,
			PatientBO patient) {
		super(Consultation.class, new Consultation(uuid, startTime, endTime,
				description, doctor.reference, patient.reference));
	}

	@Override
	protected void _delete() {
		try {
			DatabaseToolbox.remove(reference);
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Validates the proposed times for a new consultation.
	 *
	 * @param start
	 * @param end
	 * @param doctor
	 */
	private static void validateTimes(Timestamp start, Timestamp end,
			UserBO doctor) {
		if (start.after(end)
				|| end.before(new Timestamp(System.currentTimeMillis()))) {
			throw new BusinessException(BusinessError.TIME_VF);
		}
		Collection<ConsultationBO> consultations;
		consultations = ConsultationBO.getNextByDoctor(doctor);
		for (ConsultationBO c : consultations) {
			if (isConflict(start, end, c.getStartTime(), c.getEndTime())) {
				throw new BusinessException(BusinessError.DOCTOR_BUSY_VF,
						doctor.getName());
			}
		}
	}

	/**
	 * Checks if two periods overlap.
	 *
	 * @param sa
	 *            Start time for period A.
	 * @param ea
	 *            End time for period A.
	 * @param sb
	 *            Start time for period B.
	 * @param eb
	 *            End time for period B.
	 * @return True if the two periods overlap (are in conflict).
	 */
	private static boolean isConflict(Timestamp sa, Timestamp ea, Timestamp sb,
			Timestamp eb) {

		if ((sa.after(sb) && sa.before(eb)) || (ea.after(sb) && ea.before(eb))
				|| (sb.after(sa) && sb.before(ea))
				|| (eb.after(sa) && eb.before(ea))) {
			return true;
		}
		return false;

	}

	/**
	 * Validates the proposed times for an existing consultation (in case of a
	 * reschedule).
	 *
	 * @param that
	 * @param start
	 * @param end
	 * @param doctor
	 */
	private static void validateTimes(ConsultationBO that, Timestamp start,
			Timestamp end, UserBO doctor) {
		if (start.after(end)
				|| end.before(new Timestamp(System.currentTimeMillis()))) {
			throw new BusinessException(BusinessError.TIME_VF);
		}
		Collection<ConsultationBO> consultations;
		consultations = ConsultationBO.getNextByDoctor(doctor);
		for (ConsultationBO c : consultations) {
			if (!c.getUuid().equals(that.getUuid())
					&& isConflict(start, end, c.getStartTime(), c.getEndTime())) {
				throw new BusinessException(BusinessError.DOCTOR_BUSY_VF,
						doctor.getName());
			}
		}
	}
}
