sap.ui.controller("sd.p.ui.build.view.Main", {

	onInit: function(){

		this.groupDialog = sap.ui.xmlfragment(this.getView().getId(), "sd.p.ui.build.view.GroupDialog", this);
		this.getView().addDependent(this.groupDialog);
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.groupDialog);

		this.buildDialog = sap.ui.xmlfragment(this.getView().getId(), "sd.p.ui.build.view.BuildDialog", this);
		this.getView().addDependent(this.buildDialog);
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.buildDialog);
	},
	
	data: {},
	
    onLogoff: function() {
        logoff();
    },

	onHome: function() {
		refreshHash();
	},
	
    onAfterRendering: function() {
    	window.location.hash = "build";
    },
    
    formatDateTime: function(d) {
        if (d) {
    	    var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            var date = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: i18n.getText("dateTimeFormat")});
            return date.format(d);
        }
    },
    
    validateField: function(oInput) {
    	if (oInput) {
    		var func = oInput.data("func");
    		if (vIs[func]) {
    			if (vIs[func](oInput.getValue(), JSON.parse(oInput.data("param")))) {
    				oInput.setValueState("Success");
    			}
    			else {
    				oInput.setValueState("Error");
    			}
    		}
    	}
    },
    
    
    onBuild: function() {
    	var items = this.byId("fieldList").getItems();
    	var result = {};
    	var ok = true;
    	for (var i = 0 ; i < items.length; ++i) {
    		var input = items[i].getContent()[0];
    		if (input) {
    			if (input.getValueState() === "None" || !input.getValueState()) {
    				this.validateField(input);
    			}
    			if (input.getValueState() != "Error") {
    				result[input.data("name")] = input.getValue();
    			}
    			else {
    				ok = false;
    			}
    		}
    	}
    	if (ok) {
    		this.data = result;
    		this.groupDialog.open();
    	}
    	else {
	        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
	        sap.m.MessageToast.show(i18n.getText("validationFailed"));
    	}
    },
    
    onChange: function(oEvent) {
    	this.validateField(oEvent.getSource());
    },

    handleDialogSearch: function(oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new sap.ui.model.Filter("Name", sap.ui.model.FilterOperator.Contains, sValue);
        var oBinding = oEvent.getSource().getBinding("items");
        oBinding.filter([oFilter]);
    },
    
    handleAcceptGroupDialog: function(oEvent) {
    	var aContexts = oEvent.getParameter("selectedContexts");
        if (aContexts.length) {
        	var groups = [];
        	for (var i = 0 ; i < aContexts.length; ++i) {
        		groups.push(aContexts[i].getProperty("Name"));
        	}

    		var xmlhttp;
    		if (window.XMLHttpRequest) {
    			xmlhttp = new XMLHttpRequest();
    		} else {
    			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    		}
    		xmlhttp.onreadystatechange = function() {
    			if (xmlhttp.readyState == 4) {
    				if (xmlhttp.status == 200) {
    					downloadFile(xmlhttp.response, 200, xmlhttp);
    				}
    				else {
    					sap.m.MessageToast.show(i18n.getText("unabletoDownload"));
    				}
    			}
    		};
    		var s = JSON.stringify({groups: groups, templates: [], fields: this.data});
    		xmlhttp.responseType = "blob";
    		xmlhttp.open("POST","build", true);
    		xmlhttp.setRequestHeader("Content-type", "application/json");
    		xmlhttp.send(s);
        	
    		this.data = {};
	        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
	        sap.m.MessageToast.show(i18n.getText("downloadStart"));
        }
        else {
	        var i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
	        sap.m.MessageToast.show(i18n.getText("validationFailed"));
        }
    },
    
    handleCloseGroupDialog: function() {
    	this.data = {};
    },
    
    onBuildReject: function() {
    	this.buildDialog.close();
    },
    
    onBuildAccept: function(oEvent) {
    	var binding = oEvent.getSource().getBindingContext();
    	jQuery.ajax("build?document=" + binding.getProperty("Id"), {
    		method: "GET",
    		dataType: "json",
    		success: jQuery.proxy(this.restoreBuild, this),
    		error: showError
    	});
    },
    
    restoreBuild: function(oData) {
    	if (oData && oData.fields) {
    		var items = this.byId("fieldList").getItems();
        	for (var i = 0 ; i < items.length; ++i) {
        		var input = items[i].getContent()[0];
        		if (input) {
        			if (oData.fields[input.data("name")]) {
        				input.setValue(oData.fields[input.data("name")]);
        				this.validateField(input);
        			}
        		}
        	}
    	}
    	this.buildDialog.close();
    },
    
    onRestore: function() {
    	this.buildDialog.open();
    }
    
});
