package sd.a3.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The DB entity class for users.
 * @author Serban Petrescu
 *
 */
@Entity
@Table(name="T_USERS")
@NamedQueries({
    @NamedQuery(name="User.findByUsername",
                query="SELECT u FROM User u WHERE LOWER( u.username ) LIKE :value"),
    @NamedQuery(name="User.findByName",
    			query="SELECT u FROM User u WHERE LOWER( u.name ) LIKE :value"),
    @NamedQuery(name="User.findByNameAndRole",
    			query="SELECT u FROM User u WHERE LOWER( u.name ) LIKE :value AND u.role = :role"),
})
public class User implements Serializable, PersistentEntity<String> {
	private static final long serialVersionUID = 1L;

	@Id
	private String username;
	private String password;
	private String name;
	private Integer role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String param) {
		this.password = param;
	}

	public String getName() {
		return name;
	}

	public void setName(String param) {
		this.name = param;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer param) {
		this.role = param;
	}

	@Override
	public String getKey() {
		return this.username;
	}

	public User() {

	}

	public User(String username, String password, String name, Integer role) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.role = role;
	}



}