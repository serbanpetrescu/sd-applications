package sd.p.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.jaxrs.servlet.CXFNonSpringJaxrsServlet;

import sd.p.bl.UserBO;
import sd.p.db.Cleanup;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

@WebServlet(name = "ODataService", displayName = "ODataService", urlPatterns={"/data.svc/*"}, loadOnStartup = 1, initParams = {
		@WebInitParam(name = "javax.ws.rs.Application", value = "org.apache.olingo.odata2.core.rest.app.ODataApplication"),
		@WebInitParam(name = "org.apache.olingo.odata2.service.factory", value = "sd.p.com.ODataFactory") })
public class ODataServlet extends CXFNonSpringJaxrsServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			getAuthenticatedUser(request);
			super.doGet(request, response);
		} catch (GeneralException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			getAuthenticatedUser(request);
			super.doPost(request, response);
		} catch (GeneralException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			super.doDelete(request, response);
		} catch (GeneralException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			getAuthenticatedUser(request);
			super.doPut(request, response);
		} catch (GeneralException e) {
			sendErrorResponse(e, response);
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	private void sendErrorResponse(GeneralException e,
			HttpServletResponse response) {
		try {
			response.setStatus(403);
			response.setContentType("application/json");
			response.getWriter().print(e.getMessage());
		} catch (IOException ex) {
			// nothing left to do...
		}
		finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}

	private void getAuthenticatedUser(HttpServletRequest request) {
		String username = (String) request.getSession().getAttribute(
				SessionServlet.SESSION_USER);
		if (username != null) {
			UserBO.setLoggedInUser(username);
		} else {
			throw new GeneralException(GeneralError.USER_NOT_AUTH);
		}
	}

}
