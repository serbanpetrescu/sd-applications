package sd.p.db;

import java.io.Serializable;
import java.lang.String;
import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty.Multiplicity;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: Log
 *
 */
@Entity
@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=Log.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=Log.EDM_ESN)
public class Log implements Serializable, PersistentEntity<String> {

	public static final String EDM_EN = "Log";
	public static final String EDM_ESN = "Logs";
	public static final String EDM_PN_GUID = "Guid";
	public static final String EDM_PN_DATE = "Date";
	public static final String EDM_PN_DESC = "Description";
	public static final String EDM_NP_AUTHOR = "Author";
	
	@Id
	@EdmKey
	@EdmProperty(name=EDM_PN_GUID, type=EdmType.STRING)
	private String guid;
	
	@EdmProperty(name=EDM_PN_DATE, type=EdmType.DATE_TIME)
	private Timestamp date;

	@EdmProperty(name=EDM_PN_DESC, type=EdmType.STRING)
	private String description;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="authorUsername")
	@EdmNavigationProperty(toType=User.class, name=EDM_NP_AUTHOR, toMultiplicity=Multiplicity.ONE)
	private User author;
	
	private static final long serialVersionUID = 1L;

	public Log() {
		super();
	}   
	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}   
	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}   
	/*public String getAuthorUsername() {
		return this.authorUsername;
	}

	public void setAuthorUsername(String authorUsername) {
		this.authorUsername = authorUsername;
	}   */
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public User getAuthor() {
		return author;
	}
	public void setAuthor(User author) {
		this.author = author;
	}
	@Override
	public String getKey() {
		return this.guid;
	}
	
	public Log(String guid, Timestamp date, String description, User author) {
		this.guid = guid;
		this.date = date;
		this.description = description;
		this.author = author;
		if (author != null) {
			author.getLogs().add(this);
		}
	}
   
	
	
}
