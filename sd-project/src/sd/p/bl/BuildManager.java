package sd.p.bl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import sd.p.doc.BuildDocument;
import sd.p.doc.DocumentManager;
import sd.p.doc.DocumentManager.DocumentNotFoundException;
import sd.p.doc.OODocumentBuilder;
import sd.p.doc.OOProxy;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;
import sd.p.util.Zip;

public enum BuildManager {
	INSTANCE;

	private static final String TEMP_DIR_PREFIX = "SD_PROJECT";

	public void build(InputStream requestIn, OutputStream responseOut,
			boolean save) {
		BuildConfiguration config = new BuildConfiguration(requestIn);
		Path dir = null;
		try {
			dir = Files.createTempDirectory(TEMP_DIR_PREFIX);
		} catch (IOException e) {
			throw new GeneralException(GeneralError.TEMPORARY_FILE_ERROR);
		}

		OODocumentBuilder builder = OOProxy.INSTANCE.createDocumentBuilder();
		builder.setFields(config.getFields());
		for (TemplateBO t : config.getTemplatesDeep()) {
			try {
				builder.build(DocumentManager.INSTANCE.getDocumentBytes(t
						.getDocument()), dir.toString(), t.getName() + ".odt");
			} catch (IOException | DocumentNotFoundException e) {
				throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
			}
		}

		Zip zip = new Zip(responseOut);
		File[] files = dir.toFile().listFiles();
		for (File file : files) {
			zip.addToZip(file);
			file.delete();
		}
		zip.close();

		if (save) {
			List<String> tags = new ArrayList<String>(config.getGroups().size());
			for (GroupBO g : config.getGroups()) {
				tags.add(g.getName());
			}
			DocumentManager.INSTANCE.saveBuild(UserBO.getLoggedInUser()
					.getDirectory(), UserBO.getLoggedInUser().getUsername(),
					tags, config.toBytes());
		}
		
		dir.toFile().delete();
	}
	
	public Collection<BuildDocument> readAll() {
		return DocumentManager.INSTANCE.getUserBuilds(UserBO.getLoggedInUser().getUsername(),  
				UserBO.getLoggedInUser().getDirectory());
	}
	
	public BuildDocument get(String id) {
		DocumentManager.INSTANCE.isAuthor(id, UserBO.getLoggedInUser().getUsername());
		return DocumentManager.INSTANCE.getBuildDocument(id);
	}
	
	public void restore(String buildId, OutputStream out) {
		DocumentManager.INSTANCE.isAuthor(buildId, UserBO.getLoggedInUser().getUsername());
		try {
			byte[] data = DocumentManager.INSTANCE.getDocumentBytes(buildId);
			out.write(data);
		} catch (DocumentNotFoundException | IOException e) {
			throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
		}
		
	}
}
