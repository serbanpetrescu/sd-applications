package sd.a3.bl;

import javax.naming.NamingException;

import sd.a3.db.DatabaseToolbox;
import sd.a3.db.PersistentEntity;

/**
 * Base class for all business objects.
 *
 * @author Serban Petrescu
 *
 * @param <K>
 *            The database key type.
 * @param <DB>
 *            The database entity type.
 */
public abstract class BusinessObject<K, DB extends PersistentEntity<K>> {

	protected DB reference;
	protected Class<DB> clazz;

	/**
	 * Builds a new BusinessObject and persists it into the database. This
	 * constructor should only be used for NEW business objects.
	 *
	 * @param clazz
	 *            The class of the database entity type.
	 * @param reference
	 *            The database entity object.
	 */
	protected BusinessObject(Class<DB> clazz, DB reference) {
		try {
			if (DatabaseToolbox.checkExistence(clazz, reference.getKey())) {
				throw new BusinessException(BusinessError.ALREADY_EXISTS, clazz
						.getSimpleName().toLowerCase());
			}
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}

		this.reference = reference;
		this.clazz = clazz;
		try {
			DatabaseToolbox.persist(this.reference);
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
	}

	/**
	 * Builds a new BO based on an already existing DB entity which was also
	 * already read from the DB.
	 *
	 * @param clazz
	 *            The class of the database entity type.
	 * @param reference
	 *            The database entity object.
	 * @param forced
	 *            Dummy parameter (can be set to any value).
	 */
	protected BusinessObject(Class<DB> clazz, DB reference, boolean forced) {
		this.reference = reference;
		this.clazz = clazz;
	}

	/**
	 * Builds a new BO based on an already existing DB entity. The entity will
	 * be read from the DB by the constructor.
	 *
	 * @param clazz
	 *            The class of the database entity type.
	 * @param key
	 *            The key of the BO.
	 */
	protected BusinessObject(Class<DB> clazz, K key) {
		try {
			this.reference = DatabaseToolbox.find(clazz, key);
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		}
		if (this.reference == null) {
			throw new BusinessException(BusinessError.ENTITY_DOESNT_EXIST,
					clazz.getSimpleName().toLowerCase());
		}
	}

	/**
	 * Helper method for deleting the current BO from the persistence service.
	 */
	protected void _delete() {
		try {
			DatabaseToolbox.remove(this.reference);
		} catch (NamingException e) {
			throw new BusinessException(BusinessError.PERSIST_UNABLE);
		} catch (IllegalArgumentException e) {
			throw new BusinessException(BusinessError.ENTITY_DOESNT_EXIST,
					this.clazz.getSimpleName().toLowerCase());
		}
	}

}
