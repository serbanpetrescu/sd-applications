package sd.a3.db;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;

/**
 * Singleton class for handling the {@link EntityManager} and
 * {@link EntityManagerFactory} instances. A {@link ThreadLocal} member is used,
 * so a cleanup helper is created and added to the {@link Cleanup} singleton.
 *
 * @author Serban Petrescu
 *
 */
public enum EmfFactory {

	INSTANCE;

	public static final String DS_NAME = "java:comp/env/jdbc/DefaultDB";
	public static final String PU_NAME = "sd-assignment-3";

	private EmfFactory() {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(DS_NAME);
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
			this.emf = Persistence.createEntityManagerFactory(PU_NAME,
					properties);
			this.em = new ThreadLocal<EntityManager>();

			Cleanup.INSTANCE.registerCleaner(new Cleanup.Wrapper() {
				public void doCleanup() {
					em.remove();
				}
			});

		} catch (NamingException e) {
			this.e = e;
		}
	}

	private EntityManagerFactory emf;
	private ThreadLocal<EntityManager> em;
	private NamingException e;

	public EntityManagerFactory getEmf() throws NamingException {
		if (emf == null) {
			throw e;
		}
		return emf;
	}

	public EntityManager getEm() throws NamingException {
		if (em == null) {
			throw e;
		}
		if (em.get() == null) {
			em.set(emf.createEntityManager());
		}
		return em.get();
	}
}
