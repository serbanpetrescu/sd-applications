package sd.p.db;

import java.io.Serializable;
import java.lang.String;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty.Multiplicity;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: Role
 *
 */
@Entity
@Table(name="T_ROLE")
@NamedQueries({
    @NamedQuery(name="Role.readAll",
    			query="SELECT f FROM Role f"),
    @NamedQuery(name="Role.searchByName",
    			query="SELECT f FROM Role f WHERE LOWER( f.name ) LIKE :value"),
}) 
@EdmEntityType(name=Role.EDM_EN, namespace=EdmConstants.NAMESPACE)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=Role.EDM_ESN)
public class Role implements Serializable, PersistentEntity<String> {
	private static final long serialVersionUID = 1L;
	
	public static final String EDM_EN = "Role";
	public static final String EDM_ESN = "Roles";
	public static final String EDM_PN_NAME = "Name";
	public static final String EDM_PN_DESC = "Description";
	public static final String EDM_NP_GROUPS = "Groups";
	
	@Id
	@EdmKey
	@EdmProperty(name=EDM_PN_NAME, type=EdmType.STRING)
	private String name;
	
	@EdmProperty(name=EDM_PN_DESC, type=EdmType.STRING)
	private String description;

	@OneToMany(mappedBy="role")
	@EdmNavigationProperty(toType=Group.class, name=EDM_NP_GROUPS, toMultiplicity=Multiplicity.MANY)
	private Collection<Group> groups;

	public Role() {
		super();
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String getKey() {
		return this.name;
	}
	public Collection<Group> getGroups() {
		return groups;
	}
	public void setGroups(Collection<Group> groups) {
		this.groups = groups;
	}
	
	public Role(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.groups = new LinkedList<Group>();
	}
	
	
   
}
