package sd.a3.tst;

import java.sql.Date;
import java.util.Collection;
import java.util.Scanner;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;

import sd.a3.db.DatabaseToolbox;
import sd.a3.db.Patient;
import sd.a3.db.PersistentEntity;
import sd.a3.db.User;

public class DBTest extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public DBTest() throws NamingException {
		loadUsers();
		loadPatients();
		
		System.err.println(DatabaseToolbox.existsPatient("929-51-0813"));
		System.err.println(DatabaseToolbox.existsPatient("91231213"));
		System.err.println(DatabaseToolbox.existsUser("jscottc"));
		System.err.println(DatabaseToolbox.existsUser("dasdasdsa"));

		System.err.println(persistToString(DatabaseToolbox.findPatient("929-51-0813")));
		System.err.println(persistToString(DatabaseToolbox.findPatient("91231213")));
		System.err.println(persistToString(DatabaseToolbox.findUser("jscottc")));
		System.err.println(persistToString(DatabaseToolbox.findUser("dasdasdsa")));
		
		System.err.println(collToString(DatabaseToolbox.searchUsersByName("ral")));
		System.err.println(collToString(DatabaseToolbox.searchUsersByName("dasdsadadsads")));
		System.err.println(collToString(DatabaseToolbox.searchUsersByUsername("asdasdadadada")));
		System.err.println(collToString(DatabaseToolbox.searchUsersByUsername("scott")));

		System.err.println(collToString(DatabaseToolbox.searchPatientsByName("lisa")));
		System.err.println(collToString(DatabaseToolbox.searchPatientsByName("doasdkaokd")));
		System.err.flush();
	}
	
	private static String persistToString(PersistentEntity<String> e) {
		if (e == null) {
			return "null";
		}
		else {
			return "{" + e.getKey() + "}";
		}
	}
	
	private static String collToString(Collection<? extends PersistentEntity<String>> c) {
		if (c == null) {
			return "null";
		}
		else {
			StringBuilder str = new StringBuilder();
			str.append("[");
			for (PersistentEntity<String> p : c) {
				str.append(persistToString(p));
				str.append(',');
			}
			str.append("]");
			return str.toString();
		}
	}
	
	private static void loadUsers() throws NamingException {
		Scanner in = new Scanner(DBTest.class.getResourceAsStream("db_users.txt"));
		while (in.hasNextLine()) {
			String[] s = in.nextLine().split("\t");
			if (s.length > 3) {
				User u = new User(s[0], s[1], s[2], Integer.parseInt(s[3]));
				DatabaseToolbox.persist(u);
			}
		}
		in.close();
	}
	
	private static void loadPatients() throws NamingException {
		Scanner in = new Scanner(DBTest.class.getResourceAsStream("db_patients.txt"));
		while (in.hasNextLine()) {
			String[] s = in.nextLine().split("\t");
			if (s.length > 5) {
				Patient p = new Patient(s[0], s[1], s[2], s[3], Date.valueOf(s[4]));
				DatabaseToolbox.persist(p);
			}
		}
		in.close();
	}
}
