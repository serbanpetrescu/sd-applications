package sd.p.err;

import sd.p.util.JSONToolbox;

public class GeneralException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public static final String JSON_ERROR = "error";
	public static final String JSON_CODE = "code";
	public static final String JSON_PARAMS = "params";

	public GeneralException(GeneralError e, String... args) {
		super(buildMessage(e, args));
	}

	private static String buildMessage(GeneralError e, String[] args) {
		String str;
		if (e.hasParams()) {
			str = JSONToolbox.serializeObject(
					JSONToolbox.serializePair(JSON_CODE,
							Integer.toString(e.getCode()), false),
					JSONToolbox.serializePair(JSON_PARAMS,
							JSONToolbox.serializeArray(args, true), false));
		} else {
			str = JSONToolbox.serializeObject(
					JSONToolbox.serializePair(JSON_CODE,
							Integer.toString(e.getCode()), false));
		}
		return JSONToolbox.serializeObject(JSONToolbox.serializePair(JSON_ERROR, str, false));
	}

}
