package sd.a3.db;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Database entity class for consultations.
 * @author Serban Petrescu
 *
 */
@Entity
@Table(name="T_CONSULTATIONS")
@NamedQueries({
	@NamedQuery(name="Consultation.findOpenByPatient",
            query="SELECT c FROM Consultation c WHERE c.patient = :patient AND LENGTH( c.description ) = 0"
            		+ " ORDER BY c.startTime ASC"),
    @NamedQuery(name="Consultation.findFutureByDoctor",
                query="SELECT c FROM Consultation c WHERE c.doctor = :doctor ORDER BY c.startTime DESC"),
    @NamedQuery(name="Consultation.findPastByPatient",
                query="SELECT c FROM Consultation c WHERE c.startTime < :now AND c.patient = :patient "
                		+ "ORDER BY c.startTime DESC"),
    @NamedQuery(name="Consultation.findNextByPatient",
                query="SELECT c FROM Consultation c WHERE c.patient = :patient AND"
                		+ " c.endTime > :now ORDER BY c.endTime ASC"),
})
public class Consultation implements Serializable, PersistentEntity<String> {
	private static final long serialVersionUID = 1L;

	@Id
	private String uuid;

	private Timestamp startTime;
	private Timestamp endTime;
	private String description;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn
	private User doctor;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn
	private Patient patient;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getDoctor() {
		return doctor;
	}

	public void setDoctor(User doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	@Override
	public String getKey() {
		return this.uuid;
	}

	public Consultation(String uuid, Timestamp startTime, Timestamp endTime,
			String description, User doctor, Patient patient) {
		super();
		this.uuid = uuid;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.doctor = doctor;
		this.patient = patient;
		this.patient.getConsultations().add(this);
	}

	public Consultation() {

	}


}
