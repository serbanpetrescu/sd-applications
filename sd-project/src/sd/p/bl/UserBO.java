package sd.p.bl;

import java.util.Collection;
import java.util.LinkedList;

import sd.p.db.Cleanup;
import sd.p.db.DatabaseToolbox;
import sd.p.db.Log;
import sd.p.db.Role;
import sd.p.db.User;
import sd.p.doc.DocumentManager;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

public class UserBO extends BusinessObject<User, String> {

	public static Collection<UserBO> search(String username) {
		UserBO.checkLoggedInUserAuthorization();
		return createFromSet(DatabaseToolbox.searchUsers(username));
	}

	public static void checkLoggedInUserAuthorization() {
		if (!getLoggedInUser().getIsOwner()) {
			throw new GeneralException(GeneralError.USER_NOT_OWNER);
		}
	}

	public static UserBO create(String username, boolean isOwner) {
		UserBO.checkLoggedInUserAuthorization();
		validateUsername(username);
		UserBO user = new UserBO(new User(username, INIT_PWD, null, isOwner));
		DatabaseToolbox.begin();
		user.dbObject.setDirectory(DocumentManager.INSTANCE.createUserFolder(username));
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.USER_CREATED, username);
		return user;
	}

	protected static Collection<UserBO> createFromSet(Collection<User> resultSet) {
		Collection<UserBO> users = new LinkedList<UserBO>();
		for (User u : resultSet) {
			users.add(new UserBO(u, false));
		}
		return users;
	}

	public static UserBO createInitialUser() {
		UserBO admin = new UserBO(new User(ADMIN_UNAME, INIT_PWD, null, true));
		//DatabaseToolbox.begin();
		//admin.dbObject.setDirectory(DocumentManager.INSTANCE.createUserFolder(ADMIN_UNAME));
		//DatabaseToolbox.commit();
		return admin;
	}

	public static UserBO get(String username) {
		return new UserBO(username);
	}

	public static UserBO getLoggedInUser() {
		UserBO u = authUser.get();
		if (u != null) {
			return u;
		} else {
			throw new GeneralException(GeneralError.USER_NOT_AUTH);
		}
	}

	public static void setLoggedInUser(String username) {
		if (authUser.get() == null) {
			authUser.set(new UserBO(username));
		} else if (!authUser.get().getUsername().equals(username)) {
			throw new GeneralException(GeneralError.USER_ALREADY_AUTH, authUser
					.get().getUsername());
		}
	}

	public static void setLoggedInUser(String username, String password) {
		if (authUser.get() == null) {
			UserBO u = new UserBO(username);
			if (u.dbObject.getPassword().equals(password)) {
				authUser.set(u);
			} else {
				throw new GeneralException(GeneralError.USER_PWD_FAIL);
			}
		} else {
			throw new GeneralException(GeneralError.USER_ALREADY_AUTH, authUser
					.get().getUsername());
		}
	}

	public static final String ADMIN_UNAME = "Admin";

	public static final String INIT_PWD = "init";

	protected static ThreadLocal<UserBO> authUser = new ThreadLocal<UserBO>();

	static {
		Cleanup.INSTANCE.registerCleaner(new Cleanup.Wrapper() {
			public void doCleanup() {
				authUser.remove();
			}
		});
	}

	protected UserBO(String dbKey) {
		super(User.class, dbKey);
	}

	protected UserBO(User dbObject) {
		this(dbObject, true);
	}

	protected UserBO(User dbObject, boolean createDB) {
		super(User.class, dbObject, createDB);
	}

	public void addRole(Role role) {
		UserBO.checkLoggedInUserAuthorization();
		if (!this.dbObject.getRoles().contains(role)) {
			DatabaseToolbox.begin();
			this.dbObject.getRoles().add(role);
			DatabaseToolbox.commit();
			LogTM.getInstance().create(LogTM.Description.USER_ADD_ROLE, this.getUsername(), role.getName());
		} else {
			throw new GeneralException(GeneralError.USER_ALREADY_HAS_ROLE,
					role.getName());
		}
	}

	public void removeRole(Role role) {
		UserBO.checkLoggedInUserAuthorization();
		DatabaseToolbox.begin();
		this.dbObject.getRoles().remove(role);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.USER_DEL_ROLE, this.getUsername(), role.getName());
	}

	public void authenticate(String password) {
		if (!this.dbObject.getPassword().equals(password)) {
			throw new GeneralException(GeneralError.USER_PWD_FAIL);
		}
	}

	public void changePassword(String password) {
		this.checkLoggedInUserAuthorizationSelf();
		DatabaseToolbox.begin();
		this.dbObject.setPassword(password);
		DatabaseToolbox.commit();
	}

	public void checkLoggedInUserAuthorizationSelf() {
		if (!getLoggedInUser().getIsOwner() && !getLoggedInUser().equals(this)) {
			throw new GeneralException(GeneralError.USER_NOT_OWNER);
		}
	}

	public void delete() {
		UserBO.checkLoggedInUserAuthorization();
		String uname = this.getUsername();
		String folder = this.getDirectory();
		this.deleteBase();
		DocumentManager.INSTANCE.deleteUserFolder(folder);
		LogTM.getInstance().create(LogTM.Description.USER_DELETED, uname);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UserBO && obj != null) {
			return this.getUsername().equals(((UserBO) obj).getUsername());
		} else {
			return super.equals(obj);
		}
	}

	public Collection<TemplateBO> getAccessibleTemplates() {
		return TemplateBO.createFromSet(DatabaseToolbox
				.getTemplatesByUser(this.dbObject));
	}

	public Collection<GroupBO> getAccessibleGroups() {
		return GroupBO.createFromSet(DatabaseToolbox
				.getGroupsByUser(this.dbObject));
	}

	public String getDirectory() {
		return this.dbObject.getDirectory();
	}

	public Boolean getIsOwner() {
		return this.dbObject.getIsOwner();
	}

	public Collection<Log> getLogs() {
		//LogTM.getInstance().create(LogTM.Description.USER_ACTIVITY, this.getUsername());
		return this.dbObject.getLogs();
	}

	public Collection<Role> getRoles() {
		return this.dbObject.getRoles();
	}

	public String getUsername() {
		return this.dbObject.getUsername();
	}

	public void setIsOwner(boolean value) {
		UserBO.checkLoggedInUserAuthorization();
		DatabaseToolbox.begin();
		this.dbObject.setIsOwner(value);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.USER_UPDATED, this.getUsername());
	}
	
	private static void validateUsername(String name) {
		if (!name.matches("^[a-zA-Z][a-zA-Z0-9_.]*$")) {
			throw new GeneralException(GeneralError.INVALID_USERNAME, name);
		}
	}
	
}
