package sd.p.bl;

import java.util.Collection;

import sd.p.db.DatabaseToolbox;
import sd.p.db.Role;

public class RoleTM extends TableModule<Role, String> {
	
	private static volatile RoleTM instance = null;
	public static RoleTM getInstance() {
		if (instance == null) {
			synchronized (RoleTM.class) {
				if (instance == null) {
					instance = new RoleTM();
				}
			}
		}
		return instance;
	}
	
	private RoleTM() {
		super(Role.class);
	}
	
	public Collection<Role> search(String name) {
		UserBO.checkLoggedInUserAuthorization();
		return DatabaseToolbox.searchRoles(name);
	}
	
	public Collection<GroupBO> getGroups(String name) {
		UserBO.checkLoggedInUserAuthorization();
		return GroupBO.createFromSet(this.get(name).getGroups());
		
	}
	
	public void update(String name, String description){
		UserBO.checkLoggedInUserAuthorization();
		Role role = this.get(name);
		DatabaseToolbox.begin();
		role.setDescription(description);
		DatabaseToolbox.commit();
		LogTM.getInstance().create(LogTM.Description.ROLE_UPDATED, name);
	}

	public Role create(String name, String description){
		UserBO.checkLoggedInUserAuthorization();
		validateName(name);
		checkUnique(name);
		Role r = persistBase(new Role(name, description));
		LogTM.getInstance().create(LogTM.Description.ROLE_CREATED, name);
		return r;
	}
}
