package sd.p.db;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.String;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.*;

import org.apache.olingo.odata2.api.annotation.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.annotation.edm.EdmEntityType;
import org.apache.olingo.odata2.api.annotation.edm.EdmKey;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmNavigationProperty.Multiplicity;
import org.apache.olingo.odata2.api.annotation.edm.EdmProperty;
import org.apache.olingo.odata2.api.annotation.edm.EdmType;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Table(name="T_USER")
@NamedQueries({
    @NamedQuery(name="User.readAll",
    			query="SELECT f FROM User f"),
    @NamedQuery(name="User.searchByUsername",
    			query="SELECT f FROM User f WHERE LOWER( f.username ) LIKE :value"),
    @NamedQuery(name="User.getTemplates",
    			query="SELECT t FROM User u JOIN u.roles r JOIN r.groups g JOIN g.templates t "
    					+ "WHERE u.username = :username"),
    @NamedQuery(name="User.getGroups",
				query="SELECT g FROM User u JOIN u.roles r JOIN r.groups g WHERE u.username = :username"),
}) 
@EdmEntityType(namespace=EdmConstants.NAMESPACE, name=User.EDM_EN)
@EdmEntitySet(container=EdmConstants.ENTITY_CONTAINER, name=User.EDM_ESN)
public class User implements Serializable, PersistentEntity<String> {

	public static final String EDM_EN = "User";
	public static final String EDM_ESN = "Users";
	public static final String EDM_PN_USERNAME = "Username";
	public static final String EDM_PN_IS_OWNER = "IsOwner";
	public static final String EDM_NP_LOGS = "Logs";
	public static final String EDM_NP_ROLES = "Roles";
	
	   
	@Id
	@EdmKey
	@EdmProperty(name=User.EDM_PN_USERNAME, type=EdmType.STRING)
	private String username;
	
	private String password;
	
	@Column(nullable = true)
	private String directory;

	@EdmProperty(name=User.EDM_PN_IS_OWNER, type=EdmType.BOOLEAN)
	private Boolean isOwner;
	
	@OneToMany(mappedBy="author", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@EdmNavigationProperty(toType=Log.class, name=User.EDM_NP_LOGS, toMultiplicity=Multiplicity.MANY)
	private Collection<Log> logs;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="T_USER_ROLE",
    	joinColumns = @JoinColumn(name="username", referencedColumnName="username"),
    	inverseJoinColumns = @JoinColumn(name="rolename", referencedColumnName="name"))
	@EdmNavigationProperty(toType=Role.class, name=User.EDM_NP_ROLES, toMultiplicity=Multiplicity.MANY)
	private Collection<Role> roles;
	
	private static final long serialVersionUID = 1L;

	public User() {
		super();
	} 
	
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getDirectory() {
		return this.directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}   
	public Boolean getIsOwner() {
		return this.isOwner;
	}

	public void setIsOwner(Boolean isOwner) {
		this.isOwner = isOwner;
	}
	
	@Override
	public String getKey() {
		return this.username;
	}

	public User(String username, String password, String directory,
			Boolean isOwner) {
		super();
		this.username = username;
		this.password = password;
		this.directory = directory;
		this.isOwner = isOwner;
		this.logs = new LinkedList<Log>();
		this.roles = new LinkedList<Role>();
	}

	public Collection<Log> getLogs() {
		return logs;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setLogs(Collection<Log> logs) {
		this.logs = logs;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
   
}
