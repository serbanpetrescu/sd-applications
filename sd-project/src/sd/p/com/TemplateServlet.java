package sd.p.com;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import sd.p.bl.TemplateBO;
import sd.p.bl.UserBO;
import sd.p.db.Cleanup;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

/**
 * Servlet implementation class TemplateUploadServlet
 */
@WebServlet("/template")
@MultipartConfig
public class TemplateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PARAM_FILE = "file";
	private static final String PARAM_TEMPLATE = "name";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			getAuthenticatedUser(req);
			final String template = req.getParameter(PARAM_TEMPLATE);
			byte[] file = TemplateBO.get(template).getDocumentFile();
			resp.setStatus(200);
			resp.setContentType("application/vnd.oasis.opendocument.text");
			resp.setHeader("Content-Disposition", "attachment; filename = " + template + ".odt");
			resp.getOutputStream().write(file);
		} catch (GeneralException e) {
			sendErrorResponse(e, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			getAuthenticatedUser(req);
			final String template = req.getParameter(PARAM_TEMPLATE);
			final Part filePart = req.getPart(PARAM_FILE);
			TemplateBO.get(template).updateDocument(filePart.getInputStream(),
					filePart.getSize());
			resp.setStatus(200);
			resp.getOutputStream().write("DONE".getBytes());
		} catch (GeneralException e) {
			sendErrorResponse(e, resp);
		}
	}

	private void getAuthenticatedUser(HttpServletRequest request) {
		String username = (String) request.getSession().getAttribute(
				SessionServlet.SESSION_USER);
		if (username != null) {
			UserBO.setLoggedInUser(username);
		} else {
			throw new GeneralException(GeneralError.USER_NOT_AUTH);
		}
	}

	private void sendErrorResponse(GeneralException e,
			HttpServletResponse response) {
		try {
			response.setStatus(403);
			response.setContentType("application/json");
			response.getWriter().print(e.getMessage());
		} catch (IOException ex) {
			// nothing left to do...
		} finally {
			Cleanup.INSTANCE.doCleanup();
		}
	}
}
