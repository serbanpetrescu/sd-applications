package sd.p.util;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.meterware.httpunit.javascript.JavaScript.Document;

import sd.p.bl.UserBO;
import sd.p.db.DatabaseToolbox;
import sd.p.db.Group;
import sd.p.db.Role;
import sd.p.db.SimpleField;
import sd.p.db.Template;
import sd.p.db.User;
import sd.p.doc.DocumentManager;
import sd.p.doc.OOProxy;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

@WebListener
public class InitializationListener implements ServletContextListener {

	public static final String OO_LOCATION = "OpenOfficeLocation";
	private Process ooProcess = null;

	
	private void makeMockData() {
		
		SimpleField cnp = new SimpleField("CNP", "1002", "{\"2003\": \"(1|2)[0-9]{2}(0[0-9]|1[0-2])([0-2][0-9]|3(0|1))[0-9]{6}\"}", "Personal Number");
		DatabaseToolbox.persist(cnp);
		SimpleField ser = new SimpleField("IDCard", "1002", "{\"2003\": \"[A-Za-z]{2} ?[0-9]{6}\"}", "ID Card");
		DatabaseToolbox.persist(ser);
		SimpleField addr = new SimpleField("Address", "1000", "{\"2000\": \"\"}", "Person Address");
		DatabaseToolbox.persist(addr);
		SimpleField name = new SimpleField("Name", "1000", "{\"2000\": \"\"}", "Full Name");
		DatabaseToolbox.persist(name); 
		
		Template request = new Template("Request", "ehc3tX-iFgMIKFEC1ylyEe6RbiLB4jxiIFT2CPFfrXs", "Demo request");
		DatabaseToolbox.persist(request);
		Template inquery = new Template("Inquery", "V8VdsYsKCj55S5ncS5w6pINptlZcgvu6qhydTO6gs7Y", "Demo inquery");
		DatabaseToolbox.persist(inquery);
		
		Role r = new Role("Main", "Demo main role");
		DatabaseToolbox.persist(r);
		
		Group g = new Group("Demo", "Demo group");
		g.getTemplates().add(request);
		g.getTemplates().add(inquery);
		g.setRole(r);
		DatabaseToolbox.persist(g);
		
		User u = new User("petrescs", "init", "", false);
		u.getRoles().add(r);
		DatabaseToolbox.persist(u);
		
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		ooProcess.destroy();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		if (DatabaseToolbox.find(User.class, UserBO.ADMIN_UNAME) == null) {
			UserBO.createInitialUser();
			makeMockData();
		}
		String location = arg0.getServletContext()
				.getInitParameter(OO_LOCATION);
		try {
			ooProcess = Runtime.getRuntime().exec(
					new String[] {
							location,
							"-accept=\"socket,host=" + OOProxy.OO_HOST
									+ ",port=" + OOProxy.OO_PORT
									+ ",tcpNoDelay=1;urp;\"", "-nologo",
							"-headless" });
		} catch (IOException e) {
			throw new GeneralException(GeneralError.UNABLE_TO_CONNECT_OO);
		}
	}
	
	
}
