package sd.p.db;

import java.util.LinkedList;

public enum Cleanup {
	INSTANCE;
	
	
	private Cleanup() {
		cleaners = new LinkedList<Wrapper>();
	}
	
	public void registerCleaner(Wrapper cleaner) {
		cleaners.add(cleaner);
	}
	
	
	public void doCleanup() {
		for (Wrapper c : cleaners) {
			c.doCleanup();
		}
	}
	
	private LinkedList<Wrapper> cleaners;
	
	public static interface Wrapper {
		void doCleanup();
	}
	
}
