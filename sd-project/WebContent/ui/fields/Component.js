/* global showError, vfi18n, vFs, vPs */
jQuery.sap.declare("sd.p.ui.fields.Component");
jQuery.sap.require("sd.p.ui.fields.MyRouter");

sap.ui.core.UIComponent.extend("sd.p.ui.fields.Component", {
	metadata: {
		name: "fields",
		version: "1.0",
		includes: [],
		dependencies: {
			libs: ["sap.m", "sap.ui.layout"],
			components: []
		},

		rootView: "sd.p.ui.fields.view.App",

		config: {
			resourceBundle: "i18n/messageBundle.properties",
			serviceConfig: {
				name: "",
				serviceUrl: "./data.svc/"
			}
		},

		routing: {
			config: {
				routerClass: sd.p.ui.fields.MyRouter,
				viewType: "XML",
				viewPath: "sd.p.ui.fields.view",
				targetAggregation: "detailPages",
				clearTarget: false
			},
			routes: [
				{
					pattern: "",
					name: "main",
					view: "Master",
					targetAggregation: "masterPages",
					targetControl: "idAppControl",
					subroutes: [
						{
							pattern: "{entity}/:tab:",
							name: "detail",
							view: "Detail"
						}
					]
				},
				{
					name: "catchallMaster",
					view: "Master",
					targetAggregation: "masterPages",
					targetControl: "idAppControl",
					subroutes: [
						{
							pattern: ":all*:",
							name: "catchallDetail",
							view: "NotFound",
							transition: "show"
						}
					]
				}
			]
		}
	},

	init: function() {
		sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

		var mConfig = this.getMetadata().getConfig();

		// Always use absolute paths relative to our own component
		// (relative paths will fail if running in the Fiori Launchpad)
		var oRootPath = jQuery.sap.getModulePath("sd.p.ui.fields");

		// Set i18n model
		var i18nModel = new sap.ui.model.resource.ResourceModel({
			bundleUrl: [oRootPath, mConfig.resourceBundle].join("/")
		});
		this.setModel(i18nModel, "i18n");

		var sServiceUrl = mConfig.serviceConfig.serviceUrl;

		//This code is only needed for testing the application when there is no local proxy available
		var bIsMocked = jQuery.sap.getUriParameters().get("responderOn") === "true";
		// Start the mock server for the domain model
		if (bIsMocked) {
			this._startMockServer(sServiceUrl);
		}

		// Create and set domain model to the component
		var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, {
			json: true,
			loadMetadataAsync: true
		});
		oModel.attachMetadataFailed(function() {
			this.getEventBus().publish("Component", "MetadataFailed");
		}, this);
		oModel.setDefaultCountMode(sap.ui.model.odata.CountMode.Inline);
		oModel.attachRequestFailed(showError);
		this.setModel(oModel);

		var result = [];
		for (var vf in vFs) {
			if (vFs.hasOwnProperty(vf)) {
				result.push({
					key: vf,
					text: vfi18n.getText("FUNC_" + vf)
				});
			}
		}
		this.setModel(new sap.ui.model.json.JSONModel(result), "vfs");
		this.setModel(new sap.ui.model.json.JSONModel({}), "vps");

		// Set device model
		var oDeviceModel = new sap.ui.model.json.JSONModel({
			isTouch: sap.ui.Device.support.touch,
			isNoTouch: !sap.ui.Device.support.touch,
			isPhone: sap.ui.Device.system.phone,
			isNoPhone: !sap.ui.Device.system.phone,
			listMode: sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
			listItemType: sap.ui.Device.system.phone ? "Active" : "Inactive"
		});
		oDeviceModel.setDefaultBindingMode("OneWay");
		this.setModel(oDeviceModel, "device");

		this.getRouter().initialize();
		sap.ui.getCore().attachValidationError(function(evt) {
			var control = evt.getParameter("element");
			if (control && control.setValueState) {
				control.setValueState("Error");
			}
		});
		sap.ui.getCore().attachValidationSuccess(function(evt) {
			var control = evt.getParameter("element");
			if (control && control.setValueState) {
				control.setValueState("None");
			}
		});
	},

    extractVps: function() {
        var data = this.getModel("vps").getData();
        var result = {};
        for(var i = 0; i < data.length; ++i) {
            if (data[i].id) {
                result[data[i].id] = data[i].value;
            }
        }
        return JSON.stringify(result);
    },

	refreshVps: function(vf, values) {
		var params = vFs[vf] || vFs["1000"];
		var result = [];
		if (!values) {
			values = {};
		}
		for (var i = 0; i < params.length; ++i) {
			result.push({
				label: vfi18n.getText("PARAM_" + params[i])
			});
			result.push({
				id: params[i],
				value: values[params[i]],
				type: vPs[params[i]]
			});
		}
		this.getModel("vps").setData(result);
	},

	buildVpsItem: function(sId, oContext) {
	    if (oContext.getProperty("label")) {
	        return new sap.m.Label(sId, {text: oContext.getProperty("label")});
	    }
	    else {
    		return new sap.m.Input({
				value: {
					path: "vps>value",
					type: oContext.getProperty("type")
				}
    		});
	    }
	},

	_startMockServer: function(sServiceUrl) {
		jQuery.sap.require("sap.ui.core.util.MockServer");
		var oMockServer = new sap.ui.core.util.MockServer({
			rootUri: sServiceUrl
		});

		var iDelay = +(jQuery.sap.getUriParameters().get("responderDelay") || 0);
		sap.ui.core.util.MockServer.config({
			autoRespondAfter: iDelay
		});

		oMockServer.simulate("model/metadata.xml", "model/");
		oMockServer.start();

		sap.m.MessageToast.show("Running in demo mode with mock data.", {
			duration: 4000
		});
	},

	getEventBus: function() {
		return sap.ui.getCore().getEventBus();
	}
});