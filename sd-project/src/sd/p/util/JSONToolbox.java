package sd.p.util;

public class JSONToolbox {

	
	public static String serializePair(String key, String value, boolean escape) {
		return escapeJSON(key) + ":" + (escape ? escapeJSON(value) : value);
	}
	
	public static String serializeObject(String... pairs) {
		StringBuilder str = new StringBuilder();
		str.append("{");
		serializeArrayInner(str, pairs, false);
		str.append("}");
		return str.toString();
	}
	
	public static String serializeArray(String[] arr, boolean escape) {
		StringBuilder str = new StringBuilder();
		str.append("[");
		serializeArrayInner(str, arr, escape);
		str.append("]");
		return str.toString();
	}
	
	private static void serializeArrayInner(StringBuilder str, String[] arr, boolean escape) {
		String separator = "";
		for (String s : arr) {
			str.append(separator);
			separator = ",";
			if (escape) {
				str.append(JSONToolbox.escapeJSON(s));
			}
			else {
				str.append(s);
			}
		}
	}
	
	/**
	 * Escape a JSON-string for literals.
	 * Source: Jettison open source JSON library.
	 */
	public static String escapeJSON(String string) {
		if (string == null || string.length() == 0) {
			return "\"\"";
		}

		char c = 0;
		int i;
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		String t;

		sb.append('"');
		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
			case '"':
				sb.append('\\');
				sb.append(c);
				break;
			case '/':
				// if (b == '<') {
				sb.append('\\');
				// }
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else {
					sb.append(c);
				}
			}
		}
		sb.append('"');
		return sb.toString();
	}
}
