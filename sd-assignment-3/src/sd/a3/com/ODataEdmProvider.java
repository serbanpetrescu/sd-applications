package sd.a3.com;

import java.util.ArrayList;
import java.util.List;

import org.apache.olingo.odata2.api.edm.EdmMultiplicity;
import org.apache.olingo.odata2.api.edm.EdmSimpleTypeKind;
import org.apache.olingo.odata2.api.edm.FullQualifiedName;
import org.apache.olingo.odata2.api.edm.provider.Association;
import org.apache.olingo.odata2.api.edm.provider.AssociationEnd;
import org.apache.olingo.odata2.api.edm.provider.AssociationSet;
import org.apache.olingo.odata2.api.edm.provider.AssociationSetEnd;
import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.edm.provider.EntityContainer;
import org.apache.olingo.odata2.api.edm.provider.EntityContainerInfo;
import org.apache.olingo.odata2.api.edm.provider.EntitySet;
import org.apache.olingo.odata2.api.edm.provider.EntityType;
import org.apache.olingo.odata2.api.edm.provider.Facets;
import org.apache.olingo.odata2.api.edm.provider.FunctionImport;
import org.apache.olingo.odata2.api.edm.provider.FunctionImportParameter;
import org.apache.olingo.odata2.api.edm.provider.Key;
import org.apache.olingo.odata2.api.edm.provider.NavigationProperty;
import org.apache.olingo.odata2.api.edm.provider.Property;
import org.apache.olingo.odata2.api.edm.provider.PropertyRef;
import org.apache.olingo.odata2.api.edm.provider.ReturnType;
import org.apache.olingo.odata2.api.edm.provider.Schema;
import org.apache.olingo.odata2.api.edm.provider.SimpleProperty;
import org.apache.olingo.odata2.api.exception.ODataException;

/**
 * Edm Provider for the Odata Service. This provider was built by hand, although
 * an alternative method exists using annotations.
 *
 * @author Serban Petrescu
 *
 */
public class ODataEdmProvider extends EdmProvider {

	public static final String ESN_USERS = "Users";
	public static final String ESN_PATIENTS = "Patients";
	public static final String ESN_CONSULTATIONS = "Consultations";
	public static final String EN_USER = "User";
	public static final String EN_PATIENT = "Patient";
	public static final String EN_CONSULTATION = "Consultation";

	public static final String ASN_UC = "UserConsultations";
	public static final String ASN_PC = "PatientConsultations";
	public static final String AN_UC = "User_Consultation";
	public static final String AN_PC = "Patient_Consultation";

	public static final String NAMESPACE = "sd.a3.model";

	public static final FullQualifiedName ET_USER = new FullQualifiedName(
			NAMESPACE, EN_USER);
	public static final FullQualifiedName ET_PATIENT = new FullQualifiedName(
			NAMESPACE, EN_PATIENT);
	public static final FullQualifiedName ET_CONSULTATION = new FullQualifiedName(
			NAMESPACE, EN_CONSULTATION);

	public static final FullQualifiedName A_UC = new FullQualifiedName(
			NAMESPACE, AN_UC);
	public static final FullQualifiedName A_PC = new FullQualifiedName(
			NAMESPACE, AN_PC);

	public static final String R_UC = "UserToConsultation";
	public static final String R_CU = "ConsultationToUser";
	public static final String R_PC = "PatientToConsultation";
	public static final String R_CP = "ConsultationToPatient";

	public static final String ENTITY_CONTAINER = "EntityContainer";

	public static final String FI_NOTIFY = "NotifyDoctor";

	@Override
	public AssociationSet getAssociationSet(String entityContainer,
			FullQualifiedName association, String sourceEntitySetName,
			String sourceEntitySetRole) throws ODataException {
		if (ENTITY_CONTAINER.equals(entityContainer)) {
			if (A_UC.equals(association)) {
				return new AssociationSet()
						.setName(ASN_UC)
						.setAssociation(A_UC)
						.setEnd1(
								new AssociationSetEnd().setRole(R_CU)
										.setEntitySet(ESN_CONSULTATIONS))
						.setEnd2(
								new AssociationSetEnd().setRole(R_UC)
										.setEntitySet(ESN_USERS));
			} else if (A_PC.equals(association)) {
				return new AssociationSet()
						.setName(ASN_PC)
						.setAssociation(A_PC)
						.setEnd1(
								new AssociationSetEnd().setRole(R_CP)
										.setEntitySet(ESN_CONSULTATIONS))
						.setEnd2(
								new AssociationSetEnd().setRole(R_PC)
										.setEntitySet(ESN_PATIENTS));
			}
		}
		return null;
	}

	@Override
	public Association getAssociation(FullQualifiedName edmFQName)
			throws ODataException {
		if (NAMESPACE.equals(edmFQName.getNamespace())) {
			if (A_UC.getName().equals(edmFQName.getName())) {
				return new Association()
						.setName(A_UC.getName())
						.setEnd1(
								new AssociationEnd().setType(ET_USER)
										.setRole(R_UC)
										.setMultiplicity(EdmMultiplicity.ONE))
						.setEnd2(
								new AssociationEnd().setType(ET_CONSULTATION)
										.setRole(R_CU)
										.setMultiplicity(EdmMultiplicity.MANY));
			} else if (A_PC.getName().equals(edmFQName.getName())) {
				return new Association()
						.setName(A_PC.getName())
						.setEnd1(
								new AssociationEnd().setType(ET_PATIENT)
										.setRole(R_PC)
										.setMultiplicity(EdmMultiplicity.ONE))
						.setEnd2(
								new AssociationEnd().setType(ET_CONSULTATION)
										.setRole(R_CP)
										.setMultiplicity(EdmMultiplicity.MANY));
			}
		}
		return null;
	}

	@Override
	public EntitySet getEntitySet(String entityContainer, String name)
			throws ODataException {
		if (ENTITY_CONTAINER.equals(entityContainer)) {
			if (ESN_USERS.equals(name)) {
				return new EntitySet().setName(name).setEntityType(ET_USER);
			} else if (ESN_CONSULTATIONS.equals(name)) {
				return new EntitySet().setName(name).setEntityType(
						ET_CONSULTATION);
			} else if (ESN_PATIENTS.equals(name)) {
				return new EntitySet().setName(name).setEntityType(ET_PATIENT);
			}
		}
		return null;
	}

	@Override
	public EntityContainerInfo getEntityContainerInfo(String name)
			throws ODataException {
		if (name == null || ENTITY_CONTAINER.equals(name)) {
			return new EntityContainerInfo().setName(ENTITY_CONTAINER)
					.setDefaultEntityContainer(true);
		}

		return null;
	}

	@Override
	public List<Schema> getSchemas() throws ODataException {
		List<Schema> schemas = new ArrayList<Schema>();

		Schema schema = new Schema();
		schema.setNamespace(NAMESPACE);

		List<EntityType> entityTypes = new ArrayList<EntityType>();
		entityTypes.add(getEntityType(ET_USER));
		entityTypes.add(getEntityType(ET_PATIENT));
		entityTypes.add(getEntityType(ET_CONSULTATION));
		schema.setEntityTypes(entityTypes);

		List<Association> associations = new ArrayList<Association>();
		associations.add(getAssociation(A_UC));
		associations.add(getAssociation(A_PC));
		schema.setAssociations(associations);

		List<EntityContainer> entityContainers = new ArrayList<EntityContainer>();
		EntityContainer entityContainer = new EntityContainer();
		entityContainer.setName(ENTITY_CONTAINER).setDefaultEntityContainer(
				true);

		List<EntitySet> entitySets = new ArrayList<EntitySet>();
		entitySets.add(getEntitySet(ENTITY_CONTAINER, ESN_USERS));
		entitySets.add(getEntitySet(ENTITY_CONTAINER, ESN_PATIENTS));
		entitySets.add(getEntitySet(ENTITY_CONTAINER, ESN_CONSULTATIONS));
		entityContainer.setEntitySets(entitySets);

		List<AssociationSet> associationSets = new ArrayList<AssociationSet>();
		associationSets.add(getAssociationSet(ENTITY_CONTAINER, A_PC,
				ESN_USERS, R_PC));
		associationSets.add(getAssociationSet(ENTITY_CONTAINER, A_UC,
				ESN_PATIENTS, R_UC));
		entityContainer.setAssociationSets(associationSets);

		List<FunctionImport> functionImports = new ArrayList<FunctionImport>();
		functionImports.add(getFunctionImport(ENTITY_CONTAINER, FI_NOTIFY));
		entityContainer.setFunctionImports(functionImports);

		entityContainers.add(entityContainer);
		schema.setEntityContainers(entityContainers);

		schemas.add(schema);

		return schemas;
	}

	@Override
	public FunctionImport getFunctionImport(String entityContainer, String name)
			throws ODataException {
		if (FI_NOTIFY.equals(name) && ENTITY_CONTAINER.equals(entityContainer)) {
			FunctionImport function = new FunctionImport();
			function.setName(FI_NOTIFY);
			function.setHttpMethod("GET");
			List<FunctionImportParameter> params = new ArrayList<FunctionImportParameter>();
			params.add(new FunctionImportParameter().setName("personalNo")
					.setType(EdmSimpleTypeKind.String).setMode("In"));
			function.setParameters(params);
			function.setReturnType(new ReturnType().setMultiplicity(
					EdmMultiplicity.ZERO_TO_ONE).setTypeName(
					EdmSimpleTypeKind.Single.getFullQualifiedName()));
			return function;
		}
		return null;
	}

	@Override
	public EntityType getEntityType(FullQualifiedName edmFQName)
			throws ODataException {
		if (NAMESPACE.equals(edmFQName.getNamespace())) {

			if (ET_USER.getName().equals(edmFQName.getName())) {

				List<Property> properties = new ArrayList<Property>();
				properties.add(new SimpleProperty().setName("Username")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Name")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Role").setType(
						EdmSimpleTypeKind.Int32));

				List<NavigationProperty> navigationProperties = new ArrayList<NavigationProperty>();
				navigationProperties.add(new NavigationProperty()
						.setName("Consultations").setRelationship(A_UC)
						.setFromRole(R_UC).setToRole(R_CU));

				List<PropertyRef> keyProperties = new ArrayList<PropertyRef>();
				keyProperties.add(new PropertyRef().setName("Username"));
				Key key = new Key().setKeys(keyProperties);

				return new EntityType().setName(ET_USER.getName())
						.setProperties(properties).setKey(key)
						.setNavigationProperties(navigationProperties);

			} else if (ET_PATIENT.getName().equals(edmFQName.getName())) {

				List<Property> properties = new ArrayList<Property>();
				properties.add(new SimpleProperty().setName("PersonalNo")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Name")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("IdCardNo")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Address")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Birthday")
						.setType(EdmSimpleTypeKind.DateTime));

				List<NavigationProperty> navigationProperties = new ArrayList<NavigationProperty>();
				navigationProperties.add(new NavigationProperty()
						.setName("Consultations").setRelationship(A_PC)
						.setFromRole(R_PC).setToRole(R_CP));

				List<PropertyRef> keyProperties = new ArrayList<PropertyRef>();
				keyProperties.add(new PropertyRef().setName("PersonalNo"));
				Key key = new Key().setKeys(keyProperties);

				return new EntityType().setName(ET_PATIENT.getName())
						.setProperties(properties).setKey(key)
						.setNavigationProperties(navigationProperties);

			} else if (ET_CONSULTATION.getName().equals(edmFQName.getName())) {

				List<Property> properties = new ArrayList<Property>();
				properties.add(new SimpleProperty().setName("UUID")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("StartTime")
						.setType(EdmSimpleTypeKind.DateTime)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("EndTime")
						.setType(EdmSimpleTypeKind.DateTime)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Description")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Doctor")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));
				properties.add(new SimpleProperty().setName("Patient")
						.setType(EdmSimpleTypeKind.String)
						.setFacets(new Facets().setNullable(false)));

				List<NavigationProperty> navigationProperties = new ArrayList<NavigationProperty>();

				List<PropertyRef> keyProperties = new ArrayList<PropertyRef>();
				keyProperties.add(new PropertyRef().setName("UUID"));
				Key key = new Key().setKeys(keyProperties);

				return new EntityType().setName(ET_CONSULTATION.getName())
						.setProperties(properties).setKey(key)
						.setNavigationProperties(navigationProperties);

			}
		}

		return null;
	}

}
