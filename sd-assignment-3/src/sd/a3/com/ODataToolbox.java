package sd.a3.com;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmLiteral;
import org.apache.olingo.odata2.api.edm.EdmTyped;
import org.apache.olingo.odata2.api.uri.expression.BinaryExpression;
import org.apache.olingo.odata2.api.uri.expression.BinaryOperator;
import org.apache.olingo.odata2.api.uri.expression.ExpressionVisitor;
import org.apache.olingo.odata2.api.uri.expression.FilterExpression;
import org.apache.olingo.odata2.api.uri.expression.LiteralExpression;
import org.apache.olingo.odata2.api.uri.expression.MemberExpression;
import org.apache.olingo.odata2.api.uri.expression.MethodExpression;
import org.apache.olingo.odata2.api.uri.expression.MethodOperator;
import org.apache.olingo.odata2.api.uri.expression.OrderByExpression;
import org.apache.olingo.odata2.api.uri.expression.OrderExpression;
import org.apache.olingo.odata2.api.uri.expression.PropertyExpression;
import org.apache.olingo.odata2.api.uri.expression.SortOrder;
import org.apache.olingo.odata2.api.uri.expression.UnaryExpression;
import org.apache.olingo.odata2.api.uri.expression.UnaryOperator;

import sd.a3.bl.BusinessError;
import sd.a3.bl.BusinessException;
import sd.a3.bl.ConsultationBO;
import sd.a3.bl.PatientBO;
import sd.a3.bl.UserBO;

/**
 * Helper class with onyl static method for OData operations (simplifies access
 * to the BL layer).
 *
 * @author Serban Petrescu
 *
 */
public class ODataToolbox {

	/**
	 * Notify a doctor that the given patient has arrived.
	 *
	 * @param patient
	 */
	public static void notifyDoctor(String patient) {
		UserBO.checkLoggedInUserAuthorization(UserBO.ROLE_SECRETARY);
		PatientBO.get(patient).notifyObservers();
	}

	public static void deleteConsultation(String uuid) {
		ConsultationBO consult = ConsultationBO.get(uuid);
		String username = consult.getDoctor().getUsername();
		consult.delete();
		WSEndpoint.sendDoctorRefresh(username);

	}

	public static void deletePatient(String personalNo) {
		PatientBO patient = PatientBO.get(personalNo);
		Collection<ConsultationBO> consults = ConsultationBO
				.getAllByPatient(patient);
		patient.delete();
		triggerConsultationsUpdate(consults);
	}

	public static void deleteUser(String username) {
		UserBO.get(username).delete();
	}

	/**
	 * Update a given consultation with data obtained from a OData PUT / MERGE
	 * request.
	 *
	 * @param data
	 *            A map with the data.
	 * @param uuid
	 *            The key of the consultation.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> updateConsultation(
			Map<String, Object> data, String uuid) {
		String description = (String) data.get("Description");
		Calendar start = (Calendar) data.get("StartTime");
		Calendar end = (Calendar) data.get("EndTime");
		ConsultationBO consult = ConsultationBO.get(uuid);
		if (description != null
				&& !consult.getDescription().equals(description)) {
			consult.setDescription(description);
		}
		if (start != null || end != null) {
			Timestamp startTime = (start == null) ? consult.getStartTime()
					: new Timestamp(start.getTimeInMillis());
			Timestamp endTime = (end == null) ? consult.getEndTime()
					: new Timestamp(end.getTimeInMillis());
			if (!consult.getStartTime().equals(startTime)
					|| !consult.getEndTime().equals(endTime)) {
				consult.changeAppointment(startTime, endTime);
				WSEndpoint.sendDoctorRefresh(consult.getDoctor().getUsername());
			}
		}
		return convertConsultation(consult);
	}

	/**
	 * Update a given patient with data obtained from a OData PUT / MERGE
	 * request.
	 *
	 * @param data
	 *            A map with the data.
	 * @param personalNo
	 *            The key of the patient.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> updatePatient(Map<String, Object> data,
			String personalNo) {
		String name = (String) data.get("Name");
		String idCardNo = (String) data.get("IdCardNo");
		String address = (String) data.get("Address");
		Calendar birthday = (Calendar) data.get("Birthday");
		PatientBO patient = PatientBO.get(personalNo);
		Collection<ConsultationBO> consults = ConsultationBO
				.getAllByPatient(patient);
		patient.update(name, idCardNo, address,
				new Date(birthday.getTimeInMillis()));
		triggerConsultationsUpdate(consults);
		return convertPatient(patient);
	}

	/**
	 * Update a given user with data obtained from a OData PUT / MERGE request.
	 *
	 * @param data
	 *            A map with the data.
	 * @param username
	 *            The key of the user.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> updateUser(Map<String, Object> data,
			String username) {
		String name = (String) data.get("Name");
		Integer role = (Integer) data.get("Role");
		UserBO user = UserBO.get(username);
		user.update(name, role);
		return convertUser(user);
	}

	/**
	 * Creates a new consultation using data obtained from a POST request.
	 *
	 * @param data
	 *            A map with the data.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> createConsultation(
			Map<String, Object> data) {
		String personalNo = (String) data.get("Patient");
		String username = (String) data.get("Doctor");
		String description = (String) data.get("Description");
		if (description == null) {
			description = "";
		}
		Calendar start = (Calendar) data.get("StartTime");
		Calendar end = (Calendar) data.get("EndTime");
		if (personalNo != null && username != null && start != null
				&& end != null) {
			UserBO user = UserBO.get(username);
			ConsultationBO result = ConsultationBO.create(
					new Timestamp(start.getTimeInMillis()),
					new Timestamp(end.getTimeInMillis()), description, user,
					PatientBO.get(personalNo));
			WSEndpoint.sendDoctorRefresh(user.getUsername());
			return convertConsultation(result);
		} else {
			throw new BusinessException(BusinessError.INCOMPLETE_REQUEST);
		}
	}

	/**
	 * Creates a new patient using data obtained from a POST request.
	 *
	 * @param data
	 *            A map with the data.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> createPatient(Map<String, Object> data) {
		String personalNo = (String) data.get("PersonalNo");
		String name = (String) data.get("Name");
		String idCardNo = (String) data.get("IdCardNo");
		String address = (String) data.get("Address");
		Calendar birthday = (Calendar) data.get("Birthday");
		if (personalNo != null && name != null && idCardNo != null
				&& address != null && birthday != null) {
			return convertPatient(PatientBO.create(personalNo, name, idCardNo,
					address, new Date(birthday.getTimeInMillis())));
		} else {
			throw new BusinessException(BusinessError.INCOMPLETE_REQUEST);
		}
	}

	/**
	 * Creates a new user using data obtained from a POST request.
	 *
	 * @param data
	 *            A map with the data.
	 * @return The map of the resulting data.
	 */
	public static Map<String, Object> createUser(Map<String, Object> data) {
		String username = (String) data.get("Username");
		String name = (String) data.get("Name");
		Integer role = (Integer) data.get("Role");
		if (username != null && name != null && role != null) {
			return convertUser(UserBO.create(username, name, role));
		} else {
			throw new BusinessException(BusinessError.INCOMPLETE_REQUEST);
		}
	}

	/**
	 * Extracts the name substring from an OData filter.
	 *
	 * @param e
	 *            The OData filter expression.
	 * @return A string with the name substring (or "" if none found).
	 */
	public static String extractNameFilter(FilterExpression e) {
		String value;
		try {
			value = (String) e.accept(new SingleMethodVisitor("Name",
					MethodOperator.SUBSTRINGOF));
		} catch (Exception e1) {
			value = null;
		}
		if (value == null) {
			return "";
		} else {
			return value;
		}
	}

	public static List<Map<String, Object>> searchPatients(String name) {
		return convertPatients(PatientBO.searchByName(name));
	}

	public static List<Map<String, Object>> searchUsers(String name) {
		if (UserBO.getLoggedInUser().hasRole(UserBO.ROLE_ADMIN)) {
			return convertUsers(UserBO.searchByName(name));
		} else {
			return convertUsers(UserBO.searchDoctorsByName(name));
		}
	}

	public static List<Map<String, Object>> getConsultations() {
		return convertConsultations(ConsultationBO.getNextByDoctor(UserBO
				.getLoggedInUser()));
	}

	public static List<Map<String, Object>> getDoctorConsultations(
			String username) {
		return convertConsultations(ConsultationBO.getNextByDoctor(UserBO
				.get(username)));
	}

	public static List<Map<String, Object>> getPatientConsultations(
			String personalNo) {
		if (UserBO.getLoggedInUser().hasRole(UserBO.ROLE_DOCTOR)) {
			return convertConsultations(ConsultationBO
					.getPastByPatient(PatientBO.get(personalNo)));
		} else {
			return convertConsultations(ConsultationBO
					.getOpenByPatient(PatientBO.get(personalNo)));
		}
	}

	public static Map<String, Object> getConsultationPatient(String uuid) {
		return convertPatient(ConsultationBO.get(uuid).getPatient());
	}

	public static Map<String, Object> getConsultationUser(String uuid) {
		return convertUser(ConsultationBO.get(uuid).getDoctor());
	}

	public static Map<String, Object> getConsultation(String uuid) {
		return convertConsultation(ConsultationBO.get(uuid));
	}

	public static Map<String, Object> getPatient(String personalNo) {
		return convertPatient(PatientBO.get(personalNo));
	}

	public static Map<String, Object> getUser(String username) {
		return convertUser(UserBO.get(username));
	}

	/**
	 * Convert a BO to a data map.
	 *
	 * @return The data map.
	 */
	public static Map<String, Object> convertUser(UserBO user) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("Username", user.getUsername());
		data.put("Name", user.getName());
		data.put("Role", user.getRole());
		return data;
	}

	/**
	 * Convert a BO to a data map.
	 *
	 * @return The data map.
	 */
	public static Map<String, Object> convertPatient(PatientBO p) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("PersonalNo", p.getPersonalNo());
		data.put("Name", p.getName());
		data.put("Address", p.getAddress());
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(p.getBirthday().getTime());
		data.put("Birthday", c);
		data.put("IdCardNo", p.getIdCardNo());
		return data;
	}

	/**
	 * Convert a BO to a data map.
	 *
	 * @return The data map.
	 */
	public static Map<String, Object> convertConsultation(ConsultationBO p) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("UUID", p.getUuid());
		data.put("Description", p.getDescription());
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(p.getStartTime().getTime());
		data.put("StartTime", c);
		c = Calendar.getInstance();
		c.setTimeInMillis(p.getEndTime().getTime());
		data.put("EndTime", c);
		data.put("Doctor", p.getDoctor().getUsername());
		data.put("Patient", p.getPatient().getPersonalNo());
		return data;
	}

	/**
	 * Convert a collection of BOs to a list of data maps.
	 *
	 * @return The list of data maps.
	 */
	public static List<Map<String, Object>> convertUsers(Collection<UserBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(
				c.size());
		for (UserBO e : c) {
			result.add(convertUser(e));
		}
		return result;
	}

	/**
	 * Convert a collection of BOs to a list of data maps.
	 *
	 * @return The list of data maps.
	 */
	public static List<Map<String, Object>> convertPatients(
			Collection<PatientBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(
				c.size());
		for (PatientBO e : c) {
			result.add(convertPatient(e));
		}
		return result;
	}

	/**
	 * Convert a collection of BOs to a list of data maps.
	 *
	 * @return The list of data maps.
	 */
	public static List<Map<String, Object>> convertConsultations(
			Collection<ConsultationBO> c) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(
				c.size());
		for (ConsultationBO e : c) {
			result.add(convertConsultation(e));
		}
		return result;
	}

	/**
	 * Triggers the WS refresh of the consultation data for doctors.
	 *
	 * @param consultations
	 *            The modified consultations.
	 * @return false if an error occurred.
	 */
	public static boolean triggerConsultationsUpdate(
			Collection<ConsultationBO> consultations) {
		try {
			Set<String> users = new HashSet<String>();
			for (ConsultationBO c : consultations) {
				users.add(c.getDoctor().getUsername());
			}

			for (String username : users) {
				WSEndpoint.sendDoctorRefresh(username);
			}

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Helper method for extracting the OData name filter.
	 *
	 * @author Serban Petrescu
	 *
	 */
	private static class SingleMethodVisitor implements ExpressionVisitor {
		private String targetField;
		private MethodOperator targetMethod;

		public SingleMethodVisitor(String targetField,
				MethodOperator targetMethod) {
			super();
			this.targetField = targetField;
			this.targetMethod = targetMethod;
		}

		@Override
		public Object visitUnary(UnaryExpression unaryExpression,
				UnaryOperator operator, Object operand) {
			return null;
		}

		@Override
		public Object visitProperty(PropertyExpression propertyExpression,
				String uriLiteral, EdmTyped edmProperty) {
			try {
				if (edmProperty == null
						|| !targetField.equals(edmProperty.getName())) {
					return null;
				} else {
					return targetField;
				}
			} catch (EdmException e) {
				return null;
			}
		}

		@Override
		public Object visitOrderByExpression(
				OrderByExpression orderByExpression, String expressionString,
				List<Object> orders) {
			return null;
		}

		@Override
		public Object visitOrder(OrderExpression orderExpression,
				Object filterResult, SortOrder sortOrder) {
			return null;
		}

		@Override
		public Object visitMethod(MethodExpression methodExpression,
				MethodOperator method, List<Object> parameters) {
			if (method.equals(targetMethod) && parameters.size() > 1
					&& parameters.get(0) != null && parameters.get(1) != null) {
				if (targetField.equals(parameters.get(0))) {
					return parameters.get(1);
				} else {
					return parameters.get(0);
				}
			} else {
				return null;
			}
		}

		@Override
		public Object visitMember(MemberExpression memberExpression,
				Object path, Object property) {
			return null;
		}

		@Override
		public Object visitLiteral(LiteralExpression literal,
				EdmLiteral edmLiteral) {
			return edmLiteral.getLiteral();
		}

		@Override
		public Object visitFilterExpression(FilterExpression filterExpression,
				String expressionString, Object expression) {
			return expression;
		}

		@Override
		public Object visitBinary(BinaryExpression binaryExpression,
				BinaryOperator operator, Object leftSide, Object rightSide) {
			return null;
		}

	}

}
