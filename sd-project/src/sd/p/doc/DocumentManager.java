package sd.p.doc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisNameConstraintViolationException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

import sd.p.db.DatabaseToolbox;
import sd.p.err.GeneralError;
import sd.p.err.GeneralException;

import com.sap.ecm.api.EcmService;
import com.sap.ecm.api.RepositoryOptions;
import com.sap.ecm.api.RepositoryOptions.Visibility;

/**
 * Manages the communication with the document service.
 * 
 * @author Serban
 */
public enum DocumentManager {
	INSTANCE;

	public static final String REP_NAME = "sdproject";
	public static final String REP_KEY = "fh0hf3f39h3i";

	private static final String TEMPLATE_FOLDER = "Templates";
	private static final String USERS_FOLDER = "Users";

	private Session openCmisSession = null;
	private Folder rootFolder;
	private Folder templates, users;

	/**
	 * When the instance is created, the repository is checked and created as
	 * well.
	 */
	private DocumentManager() {
		EcmService ecmService = null;
		String ecmServiceLookupName = "java:comp/env/EcmService";
		try {
			InitialContext ctx = new InitialContext();
			ecmService = (EcmService) ctx.lookup(ecmServiceLookupName);
		} catch (NamingException e) {
			throw new GeneralException(GeneralError.NAMING_EXCEPTION);
		}

		try {
			openCmisSession = ecmService.connect(REP_NAME, REP_KEY);
		} catch (CmisObjectNotFoundException e) {
			RepositoryOptions options = new RepositoryOptions();
			options.setUniqueName(REP_NAME);
			options.setRepositoryKey(REP_KEY);
			options.setVisibility(Visibility.PROTECTED);
			options.setMultiTenantCapable(true);
			ecmService.createRepository(options);

			openCmisSession = ecmService.connect(REP_NAME, REP_KEY);
		} catch (Exception e) {
			throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
		}
		rootFolder = openCmisSession.getRootFolder();
		Map<String, Object> properties;
		try {
			properties = new HashMap<String, Object>();
			properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			properties.put(PropertyIds.NAME, TEMPLATE_FOLDER);
			templates = rootFolder.createFolder(properties);
		} catch (Exception e) {
			templates = (Folder) openCmisSession.getObjectByPath(rootFolder
					.getPath() + TEMPLATE_FOLDER);
		}
		try {
			properties = new HashMap<String, Object>();
			properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			properties.put(PropertyIds.NAME, USERS_FOLDER);
			users = rootFolder.createFolder(properties);
		} catch (Exception e) {
			users = (Folder) openCmisSession.getObjectByPath(rootFolder
					.getPath() + USERS_FOLDER);
		}
	}

	public String saveBuild(String authorFolderId, String authorUname,
			List<String> tags, byte[] build) {
		String name = UUID.randomUUID().toString();
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, name);
		properties.put(PropertyIds.CREATED_BY, authorUname);
		properties.put("sap:tags", tags);

		Folder folder = getUserFolder(authorUname, authorFolderId);
		ContentStream contentStream = openCmisSession.getObjectFactory()
				.createContentStream(name, build.length,
						"text/plain; charset=UTF-8",
						new ByteArrayInputStream(build));
		try {
			Document d = folder.createDocument(properties, contentStream,
					VersioningState.NONE);
			return d.getId();
		} catch (CmisNameConstraintViolationException e) {
			throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
		}
	}

	public BuildDocument getBuildDocument(String id) {
		try {
			Document doc = (Document) openCmisSession.getObject(id);
			String[] tags;
			try {
				tags = (String[]) doc.getPropertyValue("sap:tags");
			} catch (Exception e) {
				tags = new String[] {};
			}
			return new BuildDocument(doc.getId(), doc.getCreatedBy(),
					doc.getCreationDate(), tags);
		} catch (Exception e) {
			return null;
		}
	}

	public Collection<BuildDocument> getUserBuilds(String username,
			String userFolderId) {
		Folder f = getUserFolder(username, userFolderId);
		ItemIterable<CmisObject> kids = f.getChildren();
		Collection<BuildDocument> result = new ArrayList<BuildDocument>();
		for (CmisObject obj : kids) {
			if (obj instanceof Document) {
				Document doc = (Document) obj;
				String[] tags;
				try {
					tags = (String[]) doc.getPropertyValue("sap:tags");
				} catch (Exception e) {
					tags = new String[] {};
				}
				result.add(new BuildDocument(obj.getId(), obj.getCreatedBy(),
						obj.getCreationDate(), tags));

			}
		}
		return result;
	}

	public Folder getUserFolder(String username, String folderId) {
		try {
			return (Folder) openCmisSession.getObject(folderId);
		} catch (Exception e) {
			return fixUserFolder(username);
		}
	}

	public void deleteTemplateDocument(String id) {
		try {
			Document document = (Document) openCmisSession.getObject(id);
			document.delete();
		} catch (CmisObjectNotFoundException e) { /* we want to delete it anyway */
		}
	}

	public String createTemplateDocument(String name) {
		return createDocument(name, templates);
	}

	public Folder fixUserFolder(String username) {
		Folder result;
		try {
			result = (Folder) openCmisSession.getObjectByPath(users.getPath()
					+ "/" + username);
		} catch (Exception e) {
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			properties.put(PropertyIds.NAME, username);
			result = users.createFolder(properties);
		}
		DatabaseToolbox.fixUserDirectory(username, result.getId());
		return result;
	}

	public String createUserFolder(String username) {
		try {
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			properties.put(PropertyIds.NAME, username);
			return users.createFolder(properties).getId();
		} catch (final Exception e) {
			return ((Folder) openCmisSession.getObjectByPath(users.getPath()
					+ "/" + username)).getId();
		}
	}

	public void deleteUserFolder(String folderId) {
		try {
			Folder userfolder = (Folder) openCmisSession.getObject(folderId);
			userfolder.delete();
		} catch (CmisObjectNotFoundException e) { /* we want to delete it anyway */
		}
	}

	/**
	 * Creates an empty document. If the document already exists, no action will
	 * be performed.
	 * 
	 * @param name
	 *            The file name of the new document.
	 */
	private String createDocument(String name, Folder folder) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, name);

		ContentStream contentStream = openCmisSession.getObjectFactory()
				.createContentStream(name, 0, "text/plain; charset=UTF-8",
						new ByteArrayInputStream(new byte[0]));
		try {
			Document d = folder.createDocument(properties, contentStream,
					VersioningState.NONE);
			return d.getId();
		} catch (CmisNameConstraintViolationException e) {
			try {
				Document d = (Document) openCmisSession
						.getObjectByPath(templates.getPath() + "/" + name);
				return d.getId();
			} catch (Exception ex) {
				throw new GeneralException(GeneralError.DOCUMENT_STORE_ERROR);
			}
		}
	}

	/**
	 * Updates the content of a document. If the document does not exist, the
	 * manager will attempt to create it.
	 * 
	 * @param id
	 *            The document id.
	 * @param content
	 *            The new content of the document.
	 * @param length
	 *            The length of the content.
	 * @throws DocumentNotFoundException
	 */
	public void updateDocument(String id, InputStream in, long size)
			throws DocumentNotFoundException {
		try {
			Document document = (Document) openCmisSession.getObject(id);
			ContentStream contentStream = openCmisSession.getObjectFactory()
					.createContentStream(document.getName(), size,
							"text/plain; charset=UTF-8", in);
			document.setContentStream(contentStream, true);
		} catch (CmisObjectNotFoundException e) {
			throw new DocumentNotFoundException(e.getMessage());
		}
	}

	public boolean isAuthor(String objectId, String uname) {
		try {
			Document document = (Document) openCmisSession.getObject(objectId);
			return uname.equals(document.getProperty(PropertyIds.CREATED_BY));
		} catch (CmisObjectNotFoundException e) {
			return false;
		}
	}

	/**
	 * Reads a document from the root folder.
	 * 
	 * @param objectId
	 *            The document id.
	 * @return A stream to the document's content.
	 * @throws DocumentNotFoundException
	 * @throws IOException
	 */
	public byte[] getDocumentBytes(String objectId)
			throws DocumentNotFoundException, IOException {
		try {
			Document document = (Document) openCmisSession.getObject(objectId);
			return readFully(document.getContentStream().getStream(), true);
		} catch (CmisObjectNotFoundException e) {
			throw new DocumentNotFoundException(e.getMessage());
		}
	}

	/**
	 * @author Serban Thrown if a the document already exists.
	 */
	public static class DocumentAlreadyExistsException extends Exception {
		public DocumentAlreadyExistsException(String message) {
			super(message);
		}

		private static final long serialVersionUID = 1L;
	};

	/**
	 * @author Serban Thrown if the document is not found.
	 */
	public static class DocumentNotFoundException extends Exception {
		public DocumentNotFoundException(String message) {
			super(message);
		}

		private static final long serialVersionUID = 1L;
	};

	private byte[] readFully(InputStream is, boolean readAll)
			throws IOException {
		byte[] output = {};
		int pos = 0;
		while (true) {
			int bytesToRead;
			if (pos >= output.length) {
				bytesToRead = output.length + 4096;
				if (output.length < pos + bytesToRead) {
					output = Arrays.copyOf(output, pos + bytesToRead);
				}
			} else {
				bytesToRead = output.length - pos;
			}
			int cc = is.read(output, pos, bytesToRead);
			if (cc < 0) {
				if (output.length != pos) {
					output = Arrays.copyOf(output, pos);
				}
				break;
			}
			pos += cc;
		}
		return output;
	}

}
