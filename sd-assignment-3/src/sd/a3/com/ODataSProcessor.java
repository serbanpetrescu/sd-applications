package sd.a3.com;

import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.olingo.odata2.api.commons.HttpStatusCodes;
import org.apache.olingo.odata2.api.commons.InlineCount;
import org.apache.olingo.odata2.api.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.edm.EdmLiteral;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties.ODataEntityProviderPropertiesBuilder;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.exception.ODataNotFoundException;
import org.apache.olingo.odata2.api.exception.ODataNotImplementedException;
import org.apache.olingo.odata2.api.processor.ODataResponse;
import org.apache.olingo.odata2.api.processor.ODataSingleProcessor;
import org.apache.olingo.odata2.api.uri.info.DeleteUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetEntitySetUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetEntityUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetFunctionImportUriInfo;
import org.apache.olingo.odata2.api.uri.info.PostUriInfo;
import org.apache.olingo.odata2.api.uri.info.PutMergePatchUriInfo;

/**
 * Performs OData CRUD operations on the underlying data model (as requested by the API).
 * @author Serban Petrescu
 *
 */
public class ODataSProcessor extends ODataSingleProcessor {

	@Override
	public ODataResponse executeFunctionImport(
			GetFunctionImportUriInfo uriInfo, String contentType)
			throws ODataException {
		if (ODataEdmProvider.FI_NOTIFY.equals(uriInfo.getFunctionImport().getName())) {
			Map<String, EdmLiteral> params = uriInfo.getFunctionImportParameters();
			if (params.containsKey("personalNo")) {
				ODataToolbox.notifyDoctor(params.get("personalNo").getLiteral());
			}
		}
		return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
	}

	@Override
	public ODataResponse readEntity(GetEntityUriInfo uriInfo, String contentType)
			throws ODataException {

		int depth = uriInfo.getNavigationSegments().size();

		if (depth == 0) {
			EdmEntitySet entitySet = uriInfo.getStartEntitySet();

			if (ODataEdmProvider.ESN_USERS.equals(entitySet.getName())) {
				String username = uriInfo.getKeyPredicates().get(0)
						.getLiteral();

				URI serviceRoot = getContext().getPathInfo().getServiceRoot();
				ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
						.serviceRoot(serviceRoot);

				return EntityProvider.writeEntry(contentType, entitySet,
						ODataToolbox.getUser(username),
						propertiesBuilder.build());

			} else if (ODataEdmProvider.ESN_PATIENTS
					.equals(entitySet.getName())) {
				String personalNo = uriInfo.getKeyPredicates().get(0)
						.getLiteral();
				URI serviceRoot = getContext().getPathInfo().getServiceRoot();
				ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
						.serviceRoot(serviceRoot);

				return EntityProvider.writeEntry(contentType, entitySet,
						ODataToolbox.getPatient(personalNo),
						propertiesBuilder.build());

			} else if (ODataEdmProvider.ESN_CONSULTATIONS.equals(entitySet
					.getName())) {
				String uuid = uriInfo.getKeyPredicates().get(0).getLiteral();
				URI serviceRoot = getContext().getPathInfo().getServiceRoot();
				ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
						.serviceRoot(serviceRoot);

				return EntityProvider.writeEntry(contentType, entitySet,
						ODataToolbox.getConsultation(uuid),
						propertiesBuilder.build());

			}

			throw new ODataNotFoundException(ODataNotFoundException.ENTITY);

		} else if (depth == 1) {
			EdmEntitySet entitySet = uriInfo.getTargetEntitySet();
			if (ODataEdmProvider.ESN_USERS.equals(entitySet.getName())) {
				String uuid = uriInfo.getKeyPredicates().get(0).getLiteral();
				return EntityProvider.writeEntry(
						contentType,
						uriInfo.getTargetEntitySet(),
						ODataToolbox.getConsultationUser(uuid),
						EntityProviderWriteProperties.serviceRoot(
								getContext().getPathInfo().getServiceRoot())
								.build());
			} else if (ODataEdmProvider.ESN_PATIENTS
					.equals(entitySet.getName())) {
				String uuid = uriInfo.getKeyPredicates().get(0).getLiteral();
				return EntityProvider.writeEntry(
						contentType,
						uriInfo.getTargetEntitySet(),
						ODataToolbox.getConsultationPatient(uuid),
						EntityProviderWriteProperties.serviceRoot(
								getContext().getPathInfo().getServiceRoot())
								.build());
			}
			throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
		}

		throw new ODataNotImplementedException();
	}

	@Override
	public ODataResponse updateEntity(PutMergePatchUriInfo uriInfo,
			InputStream content, String requestContentType, boolean merge,
			String contentType) throws ODataException {
		EntityProviderReadProperties properties = EntityProviderReadProperties
				.init().mergeSemantic(false).build();

		ODataEntry entry = EntityProvider.readEntry(requestContentType,
				uriInfo.getTargetEntitySet(), content, properties);
		Map<String, Object> data = entry.getProperties();
		String key = uriInfo.getKeyPredicates().get(0).getLiteral();
		String entityName = uriInfo.getTargetEntitySet().getName();

		if (ODataEdmProvider.ESN_USERS.equals(entityName)) {
			ODataToolbox.updateUser(data, key);
		} else if (ODataEdmProvider.ESN_PATIENTS.equals(entityName)) {
			ODataToolbox.updatePatient(data, key);
		} else if (ODataEdmProvider.ESN_CONSULTATIONS.equals(entityName)) {
			ODataToolbox.updateConsultation(data, key);
		}

		return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
	}

	@Override
	public ODataResponse createEntity(PostUriInfo uriInfo, InputStream content,
			String requestContentType, String contentType)
			throws ODataException {

		if (uriInfo.getNavigationSegments().size() > 0) {
			throw new ODataNotImplementedException();
		}

		if (uriInfo.getStartEntitySet().getEntityType().hasStream()) {
			throw new ODataNotImplementedException();
		}

		EntityProviderReadProperties properties = EntityProviderReadProperties
				.init().mergeSemantic(false).build();

		ODataEntry entry = EntityProvider.readEntry(requestContentType,
				uriInfo.getStartEntitySet(), content, properties);

		Map<String, Object> data = entry.getProperties();

		EdmEntitySet set = uriInfo.getStartEntitySet();

		if (ODataEdmProvider.ESN_USERS.equals(set.getName())) {
			data = ODataToolbox.createUser(data);
		} else if (ODataEdmProvider.ESN_PATIENTS.equals(set.getName())) {
			data = ODataToolbox.createPatient(data);
		} else if (ODataEdmProvider.ESN_CONSULTATIONS.equals(set.getName())) {
			data = ODataToolbox.createConsultation(data);
		}

		return EntityProvider.writeEntry(
				contentType,
				uriInfo.getStartEntitySet(),
				data,
				EntityProviderWriteProperties.serviceRoot(
						getContext().getPathInfo().getServiceRoot()).build());
	}

	@Override
	public ODataResponse readEntitySet(GetEntitySetUriInfo uriInfo,
			String contentType) throws ODataException {

		EdmEntitySet entitySet, sourceSet;
		ODataEntityProviderPropertiesBuilder propertiesBuilder = EntityProviderWriteProperties
				.serviceRoot(getContext().getPathInfo().getServiceRoot());

		if (uriInfo.getNavigationSegments().size() == 0) {
			entitySet = uriInfo.getStartEntitySet();
			if (ODataEdmProvider.ESN_USERS.equals(entitySet.getName())) {
				String name = ODataToolbox.extractNameFilter(uriInfo
						.getFilter());
				List<Map<String, Object>> result = applySetParameters(
						ODataToolbox.searchUsers(name), propertiesBuilder,
						uriInfo);

				return EntityProvider.writeFeed(contentType, entitySet, result,
						propertiesBuilder.build());
			} else if (ODataEdmProvider.ESN_PATIENTS
					.equals(entitySet.getName())) {
				String name = ODataToolbox.extractNameFilter(uriInfo
						.getFilter());
				List<Map<String, Object>> result = applySetParameters(
						ODataToolbox.searchPatients(name), propertiesBuilder,
						uriInfo);

				return EntityProvider.writeFeed(contentType, entitySet, result,
						propertiesBuilder.build());
			} else if (ODataEdmProvider.ESN_CONSULTATIONS.equals(entitySet
					.getName())) {
				List<Map<String, Object>> result = applySetParameters(
						ODataToolbox.getConsultations(), propertiesBuilder,
						uriInfo);

				return EntityProvider.writeFeed(contentType, entitySet, result,
						propertiesBuilder.build());
			}

			throw new ODataNotFoundException(ODataNotFoundException.ENTITY);

		} else if (uriInfo.getNavigationSegments().size() == 1) {
			entitySet = uriInfo.getTargetEntitySet();

			if (ODataEdmProvider.ESN_CONSULTATIONS.equals(entitySet.getName())) {

				sourceSet = uriInfo.getStartEntitySet();

				if (ODataEdmProvider.ESN_PATIENTS.equals(sourceSet.getName())) {
					String personalNo = uriInfo.getKeyPredicates().get(0)
							.getLiteral();
					return EntityProvider.writeFeed(
							contentType,
							entitySet,
							ODataToolbox.getPatientConsultations(personalNo),
							EntityProviderWriteProperties
									.serviceRoot(
											getContext().getPathInfo()
													.getServiceRoot()).build());
				} else if (ODataEdmProvider.ESN_USERS.equals(sourceSet
						.getName())) {
					String username = uriInfo.getKeyPredicates().get(0)
							.getLiteral();
					return EntityProvider.writeFeed(
							contentType,
							entitySet,
							ODataToolbox.getDoctorConsultations(username),
							EntityProviderWriteProperties
									.serviceRoot(
											getContext().getPathInfo()
													.getServiceRoot()).build());
				}
			}

			throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
		}

		throw new ODataNotImplementedException();
	}

	/** Applies the entity set parameters (skip, top, inline count).
	 * @param result The data set (result of the query).
	 * @param properties The response properties (these are updated by the method).
	 * @param uriInfo The request URI information.
	 * @return The result set (with paging applied).
	 */
	private List<Map<String, Object>> applySetParameters(
			List<Map<String, Object>> result,
			ODataEntityProviderPropertiesBuilder properties,
			GetEntitySetUriInfo uriInfo) {
		if (InlineCount.ALLPAGES.equals(uriInfo.getInlineCount())) {
			properties.inlineCountType(InlineCount.ALLPAGES).inlineCount(
					result.size());
		}
		if (uriInfo.getSkip() == null && uriInfo.getTop() == null) {
			return result;
		} else {
			int top = (uriInfo.getTop() == null) ? result.size() : uriInfo
					.getTop();
			int skip = (uriInfo.getSkip() == null) ? 0 : uriInfo.getSkip();
			if (skip > result.size()) {
				skip = result.size();
			}
			if (skip + top > result.size()) {
				top = result.size() - skip;
			}
			return result.subList(skip, skip + top);
		}

	}

	@Override
	public ODataResponse deleteEntity(DeleteUriInfo uriInfo, String contentType)
			throws ODataException {
		EdmEntitySet set = uriInfo.getStartEntitySet();
		String key = uriInfo.getKeyPredicates().get(0).getLiteral();
		if (ODataEdmProvider.ESN_USERS.equals(set.getName())) {
			ODataToolbox.deleteUser(key);
		} else if (ODataEdmProvider.ESN_PATIENTS.equals(set.getName())) {
			ODataToolbox.deletePatient(key);
		} else if (ODataEdmProvider.ESN_CONSULTATIONS.equals(set.getName())) {
			ODataToolbox.deleteConsultation(key);
		}
		return ODataResponse.status(HttpStatusCodes.NO_CONTENT).build();
	}
}
